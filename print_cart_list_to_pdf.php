﻿<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>カート印刷</title>
</head>
<style type="text/css">
  .fullscreen-image {
     cursor: url("img/plus_cursor.png"), pointer; /* マウスポインタを指定 */
     display: inline-block;   /* 横方向に並べる指定 */
     margin: 0px 5px 5px 0px; /* 周囲の余白量(右と下に5pxずつ) */
  }
  table {
  border-collapse: collapse;
  border: solid 2px orange;/*表全体を線で囲う*/
  }
  table th, table td {
  border: dashed 1px orange;
  /*破線 1px オレンジ*/
  }
  table td.col_price {
    text-align: right;
  }
  .center {
      text-align: center;
  }
</style>
<script>
//変数storageにlocalStorageを格納//sessionstorageでも入れ替え可
var storage = localStorage;
var arJasco_List_Class = {
  JASCO_List_chCode: "null",//商品番号
  JASCO_List_chNumber_string: "null",//品目番号
  JASCO_List_chProc_type: "null",//型式
  JASCO_List_chProc_name: "null",//商品名称
  JASCO_List_iPrice_p: "null",//販売価格
  JASCO_List_iPrice_d1: "null",//卸1
  JASCO_List_iPrice_d2: "null",//卸2
  JASCO_List_x_iPrice_d3: "null",//卸3
  JASCO_List_iPrice_m: "null",//工場卸
  JASCO_List_x_iGroup: "null",//販売ｸﾞﾙｰﾌﾟ
  JASCO_List_chSpecification: "null",//規格
  JASCO_List_chNotes: "null",//仕様・備考→（規格に入りきらない物）
  JASCO_List_x_chSite: "null",//ｻｲﾄ
  JASCO_List_x_iStock_pc: "null",//引当可能在庫数（PC）
  JASCO_List_chDomestic_classification: "null",//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  JASCO_List_chService_classification: "null",//価格表区分2→価格表区分（サービス）
  JASCO_List_chOverseas_classification: "null",//価格表区分3→価格表区分（輸出）
  JASCO_List_chEnd_date: "null",//有効終了日
  JASCO_List_x_chWhile_stocks_laste: "null",//在庫限り
  JASCO_List_chParts_centere: "null",//パーツセンタ対象
  JASCO_List_x_chValid: "null",//有効品目
  JASCO_List_chSubstitutional_goods: "null",//置換商品
  JASCO_List_x_chDiscontinuance: "null",//商品中止理由
  JASCO_List_chPrecaution1: "null",//特記事項1
  JASCO_List_chClass1: "null",//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  JASCO_List_chProc_name_eng: "null",//英名
  JASCO_List_chPicture: "null",//写真
  JASCO_List_chInstrument_type: "null",//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  JASCO_List_chCompatible_models: "null",//対応機種/用途
  JASCO_List_chEmplacement_cost: "null",//搬入料(大型、中型、小型）
  //sales infos
  JASCO_List_iSalesCount:"null",//登録数
  //time stamp
  JASCO_List_rec_timestamp:"0"//登録時刻
}
function addCommma(tmp_value){
  var tmpstring = String(tmp_value).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  return tmpstring;
}
//保存されているデータをリスト表示する
function show_result() {
  var resultTable = "";
  //タイトル部
  resultTable+="<table><thead><tr>";
  resultTable+="<th class=xxx>個数<br></th>";
  resultTable+="<th class=xxx>商品番号<br></th>";
  resultTable+="<th class=xxx>品目番号<br></th>";
  resultTable+="<th class=xxx>型式<br></th>";
  resultTable+="<th class=xxx>商品名称<br></th>";
  resultTable+="<th class=xxx>単価<br></th>";
  resultTable+="<th class=xxx>販売価格(税抜)<br></th>";
  resultTable+="<th class=xxx>規格<br></th>";
  resultTable+="<th class=xxx>仕様・備考<br></th>";
  resultTable+="<th class=xxx>対応機種/用途<br></th>";
  resultTable+="<th class=xxx>画像<br></th>";
  resultTable+="</tr></thead>";
  resultTable+="<tbody>";
  var sum = 0;

  //Key名でソートされているのでtimestampで配列を並べ替えて表示(予定)⇒getSelect,getTextにも影響するので注意

  //保存されているデータの数だけループ
  for(var i=0; i<storage.length; i++){    
    var k = storage.key(i);
    arJasco_Product = JSON.parse(storage.getItem(k));
    resultTable+="<tr>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_iSalesCount +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chCode +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chNumber_string +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chProc_type +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chProc_name +"</td>";
    resultTable+="<td class=\"col_price\">"+ addCommma(arJasco_Product.JASCO_List_iPrice_p)+"</td>";//単価
    resultTable+="<td class=\"col_price\">"+ addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_p)*parseInt(arJasco_Product.JASCO_List_iSalesCount)) +"</td>";//販売価格
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chSpecification  +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chNotes +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_List_chCompatible_models +"</td>";
    resultTable+="<td class=XXX>";
    resultTable+="<div class=\"fullscreen-image\" data-image=\""+arJasco_Product.JASCO_List_chPicture+"\" data-title=\""+arJasco_Product.JASCO_List_chCode+"\" data-caption=\""+arJasco_Product.JASCO_List_chNumber_string+"\">";
    resultTable+="<img src=\""+arJasco_Product.JASCO_List_chPicture+"\" width=\"50\" alt=\" \">";
    resultTable+="</div></td></tr>";
    sum += parseInt(arJasco_Product.JASCO_List_iPrice_p)*parseInt(arJasco_Product.JASCO_List_iSalesCount);
  }
  resultTable+="</tbody>";
  resultTable+="<tfoot><tr>";
  resultTable+="<td colspan=\"12\"align=\"right\">"+"合計(税抜)：" + addCommma(sum) + "円です。</td>";
  resultTable+="</tfoot></tr></table>";
  //上のループで作成されたテキストを表示する
  document.getElementById("show_result_field").innerHTML = resultTable;
}
</script>
<body onLoad="show_result()">
  <div class="bg-success padding-y-20">
    <div class="container text-center">
    <h1>カート一覧</h1>
    <div id="show_result_field"></div><br>
  </div>
</body>
</html>
<?php
require_once("../TCPDF/tcpdf.php");
$tcpdf = new TCPDF("Portrait");
$tcpdf->AddPage();
$tcpdf->SetFont("kozminproregular", "", 10);

//html content

//html test
$html =  <<< EOF
<h1>カートの印刷</h1>
<div id="show_result_field"></div><br>
<img src="image/footer_all.jpg">
EOF;

$tcpdf->writeHTML($html);
ob_end_clean();// 出力用バッファの内容を消去
$tcpdf->Output("test.pdf");
//$tcpdf->Output("test.pdf", 'D');//ソフトウェアで表示ないしダウンロード
?>
