﻿<?php
  header("Content-type: text/html; charset=utf-8");
  //ファイルパスの設定
  require("./config/filePath.php");
  //echo ini_get('upload_max_filesize');
  //echo ini_get('post_max_size');
  //echo ini_get('memory_limit');
  $tempfile = ''; //PHP8.0 初期化必須
  $filename = '';
  $filename1 = '';
  if( isset( $_FILES['fname1']['tmp_name']) )
    $tempfile = $_FILES['fname1']['tmp_name'];
  if( isset( $_FILES['fname1']['name']) )
    $filename = filePath::$importtext_path. $_FILES['fname1']['name'];
  //echo "name:".$_FILES['fname']['name']."<br>";
  //echo "type:".$_FILES['fname']['type']."<br>";
  //echo "tmp_name:".$_FILES['fname']['tmp_name']."<br>";
  //echo "error:".$_FILES['fname']['error']."<br>";
  //echo "size:".$_FILES['fname']['size']."<br>";
  if (is_uploaded_file($tempfile)) {
    if ( move_uploaded_file($tempfile , $filename )) {
      	chmod($filename, 0755);
        echo $filename. "をアップロードしました。<br>";
    } else {
        echo "ファイルをアップロードできません。<br>";
    }
  } else {
    echo "ファイルが選択されていません。<br>";
  } 
 $filename1 = $filename;

  $text1 = '';
  //$filename1 ="C:\\xampp\\_notes\\basefiles\\zenhan.txt";
  //$filename1 =$_REQUEST['name'];
  $dummy_code = '';
  $start = '';
  $end = '';
  $sec = 0;
  $statement = '';
  //データベースへ接続設定
  require("./config/dbConnect.php");
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。".$filename1."<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(前半)の削除前<br>";
    //既存データベース(前半)の削除
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer_fh'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_fh");
      if($statement){
        if($statement->execute())
          echo "データベース(前半)の削除成功です。<br>";
        else
          echo "データベース(前半)の削失敗しました。<br>";
      }else{
        echo "データベース(前半)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //データベース(前半)の作成（枠だけ）
    echo "データベース(前半)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_fh(
    code varchar(32),
    number_string varchar(32),
    proc_type varchar(64),
    proc_name varchar(64),
    price_p varchar(32),
    price_d1 varchar(32),
    price_d2 varchar(32),
    x_price_d3 varchar(32),
    price_m varchar(32),
    x_group varchar(32),
    specification varchar(128),
    notes text,
    x_site varchar(16),
    class1 varchar(32),
    domestic_classification varchar(64),
    service_classification varchar(64),
    overseas_classification varchar(64),
    end_date varchar(64),
    x_while_stocks_last varchar(16),
    parts_center varchar(16),
    x_valid varchar(16),
    substitutional_goods varchar(32),
    x_stock_pc varchar(32),
    x_discontinuance varchar(512),
    x_insttype_dont_use varchar(512),
    proc_name_eng varchar(256),
    eng_annotation text,
    price_e varchar(32),
    price_d_us varchar(32),
    price_d_eu varchar(32)
    )");
    //PHP8.0で厳格化されたため dateにブランクが入れられずエラー→一旦文字列で取り込む
    /*
    //PHP8.0で厳格化されたためLOADするファイルにない列を削除
    precaution1 varchar(128),
    x_precaution2 varchar(128),
    x_annotation varchar(128),
    x_department_in_charge varchar(64),
    x_inventory_section varchar(64),
    x_number_string_mgr_code varchar(32),
    x_number_string_mgr_name varchar(32),
    x_class2 varchar(32),    
    x_start_date varchar(64),
    x_sp_check varchar(32),
    x_class_sp varchar(32),
    x_category varchar(32),
    x_delivery_fee varchar(32),
    x_memo varchar(32)
    */
    if($statement){
      if($statement->execute())
        echo "データベース(前半)の作成成功です。<br>";
      else
        echo "データベース(前半)の作成失敗しました。<br>";
    }else{
      echo "データベース(前半)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(前半)の中身詰める<br>";
    //$filename3 = "C:\\\\xampp\\\\_notes\\\\basefiles\\\\zenhan.txt";
    echo "ファイル名:".$filename1."です。<br>";
    //$filename_import = str_replace("\\", "\\\\", $filename1);//$tempfileであれば必要
    $filename_import = '';
    $filename_import =  $filename1;
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $buffer_string = '';
    $buffer_string = "LOAD DATA INFILE :Load_filename INTO TABLE new_list_set_buffer_fh FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES;";
 
    echo "buffer_string:".$buffer_string."です。<br>";
    $statement = $dbh->prepare($buffer_string);
    if($statement){
      $statement->bindValue(':Load_filename', $filename_import, PDO::PARAM_STR);
      if($statement->execute()){
        echo "データベース(前半)の中身詰める 成功<br>";
      }else{
        echo "データベース(前半)の中身詰める 失敗<br>";
      }
    }else{
      echo "データベース(前半)の中身詰める<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    $e = ''; 
    /**********************************************
      echo "データベース(前半)コピーの削除前<br>";
      //既存データベース(前半)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_fh_cpy");
      if($statement){
        if($statement->execute())
          echo "データベース(前半)コピーの削除成功です。<br>";
        else
          echo "データベース(前半)コピーの削失敗しました。<br>";
      }else{
        echo "データベース(前半)コピーの削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";

      /**********************************************
        echo "データベース(前半)コピー<br>";
        //new_list_set_buffer_fh_cpy
        $start = microtime(true);
        echo "開始:".$start."です。<br>";
        $statement = $dbh->prepare("CREATE TABLE new_list_set_buffer_fh_cpy LIKE new_list_set_buffer_fh");
        if($statement){
          if($statement->execute())
            echo "データベース(前半)のコピー成功です。<br>";
          else
            echo "データベース(前半)のコピー敗しました。<br>";
        }else{
          echo "データベース(前半)のコピー敗しました。<br>";
        }
        $end = microtime(true);
        echo "終了:".$end."です。<br>";
        $sec = ($end - $start);
        echo "処理時間:".$sec."です。<br><br>";
      /**********************************************
        echo "データベース(前半)コピー<br>";
        //既存データベース(前半)のコピー
        $start = microtime(true);
        echo "開始:".$start."です。<br>";
        $statement = $dbh->prepare("INSERT INTO new_list_set_buffer_fh_cpy SELECT * FROM new_list_set_buffer_fh");
        if($statement){
          if($statement->execute())
            echo "データベース(前半)のコピー成功です。<br>";
          else
            echo "データベース(前半)のコピー敗しました。<br>";
        }else{
          echo "データベース(前半)のコピー敗しました。<br>";
        }
        $end = microtime(true);
        echo "終了:".$end."です。<br>";
        $sec = ($end - $start);
        echo "処理時間:".$sec."です。<br><br>";
        /**********************************************
        echo "重複削除<br>";
       //既存データベース(前半)の削除
       $start = microtime(true);
       echo "開始:".$start."です。<br>";
       $statement = $dbh->prepare("CREATE TEMPORARY TABLE new_list_set_buffer_fh_cpy_tmp AS SELECT MIN(code),
       number_string,proc_type, proc_name, price_p, price_d1, price_d2, x_price_d3, price_m, x_group, specification, notes,
       x_site, x_stock_pc, domestic_classification, service_classification, overseas_classification, end_date, x_while_stocks_last,
       parts_center, x_valid, substitutional_goods, class1, class2, x_discontinuance, annotation, precaution1, x_precaution2,
       x_department_in_charge, x_inventory_section, proc_name_eng FROM new_list_set_buffer_fh_cpy GROUP BY number_string");
       if($statement){
         if($statement->execute())
           echo "重複削除成功です。<br>";
         else
           echo "重複削除敗しました。<br>";
       }else{
         echo "重複削除敗しました。<br>";
       }
       $end = microtime(true);
       echo "終了:".$end."です。<br>";
       $sec = ($end - $start);
       echo "処理時間:".$sec."です。<br><br>";
       /**********************************************

       echo "new_list_set_buffer_fh_cpyさくじょ<br>";
       //既存データベース(前半)の削除
       $start = microtime(true);
       echo "開始:".$start."です。<br>";
       $statement = $dbh->prepare("DELETE FROM new_list_set_buffer_fh_cpy");
       if($statement){
         if($statement->execute())
           echo "成功です。<br>";
         else
           echo "敗しました。<br>";
       }else{
         echo "敗しました。<br>";
       }
       $end = microtime(true);
       echo "終了:".$end."です。<br>";
       $sec = ($end - $start);
       echo "処理時間:".$sec."です。<br><br>";


       echo "new_list_set_buffer_fh_cpy_tmpよりコピー<br>";
       //既存データベース(前半)の削除
       $start = microtime(true);
       echo "開始:".$start."です。<br>";
       $statement = $dbh->prepare("INSERT INTO new_list_set_buffer_fh_cpy SELECT * FROM new_list_set_buffer_fh_cpy_tmp");
       if($statement){
         if($statement->execute())
           echo "成功です。<br>";
         else
           echo "敗しました。<br>";
       }else{
         echo "敗しました。<br>";
       }
       $end = microtime(true);
       echo "終了:".$end."です。<br>";
       $sec = ($end - $start);
       echo "処理時間:".$sec."です。<br><br>";

       echo "new_list_set_buffer_fh_cpy_tmpよりコピー<br>";
       //既存データベース(前半)の削除
       $start = microtime(true);
       echo "開始:".$start."です。<br>";
       $statement = $dbh->prepare("DROP TEMPORARY TABLE new_list_set_buffer_fh_cpy_tmp");
       if($statement){
         if($statement->execute())
           echo "成功です。<br>";
         else
           echo "敗しました。<br>";
       }else{
         echo "敗しました。<br>";
       }
       $end = microtime(true);
       echo "終了:".$end."です。<br>";
       $sec = ($end - $start);
       echo "処理時間:".$sec."です。<br><br>";
    /**********************************************
    //インデックスの作成
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_fh ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    /**********************************************
    echo "データベース(前半)の品目番号重複削除<br>";
    $statement = $dbh->prepare("DELETE FROM new_list_set_buffer_fh WHERE code NOT IN(SELECT min_code FROM(SELECT MIN(CHAR_LENGTH(code)),code AS min_code FROM new_list_set_buffer_fh GROUP BY number_string) tmp)");
    if($statement){
      if($statement->execute()){
        echo "データベース(前半)の品目番号重複削除 成功<br>";
      }else{
        echo "データベース(前半)の品目番号重複削除 失敗<br>";
      }
    }else{
      echo "データベース(前半)の品目番号重複削除<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";

    /**********************************************/
     //インデックスの作成
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_fh ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }    
    /**********************************************/
    print('データベース登録完了！<br>');
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

  //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
  //echo "<script type='text/javascript'>history.back();</script>";//力技でウィンドウを閉じる

?>
