<?php
    require("./config/loadEnv.php");
    require("./config/getSessionUserInfo.php");

    $userInfo = getSessionUserInfo();

    // 開発者と営業管理室に編集権限付与
    if($userInfo['idno'] == "0811" || $userInfo['idno'] == "0858" || $userInfo['scode'] == 404 || $userInfo['scode'] == 401)
    {
      $Admin = 1;
    }
    else{
      $Admin = 0;     
    }

    if ($Admin == 0) {
        // ユーザー情報が取得できなかった場合の処理
        header("Location: login.php?alert=" . urlencode("ログインが必要です"));
        exit;
    }
    header("Content-type: text/html; charset=utf-8");
    
    //login**************************************************
    require("./config/section.php");
    $viewtype =  getsectiontype($userInfo['scode']);
    //echo "<br><br><br><br><br><br><br><br>表示：".$userInfo['name'].$viewtype."です。<br>";
    //viewtype**************************************************

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品検索</title>
	<link rel="stylesheet" type="text/css" href="css/style_flex2.css?230721">
	<link rel="stylesheet" type="text/css" href="css/checkbox.css?230224">
	<link rel="stylesheet" type="text/css" href="css/search_items.css?230727">
 	<link rel="stylesheet" href="css/under_bar_menu.css?230722">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <style>
            .flex_box{
                display: flex;
                justify-content: center;  /* 水平方向に中央寄せ */
                align-items: center;      /* 垂直方向に中央寄せ */
                height: 12vh;            /* ビューポート全体の高さを使用 */
                margin: 0;
                background-color: #ffffff; /* 背景色を設定 */
            }

            .box {
                width: 340px;
                padding: 20px;
                background-color: #fff;
                font-size:14px;
                text-align: left; /* ボックス内の文字を左揃え */
            }
  </style>
</head>
<body>  
<header>
    <div class = "grid_contents">
      <div class ="grid_item_01" style="width:120px;background-color:white;"> </div>
      <div class ="grid_item_02" style="background-color:white;">
        <p style="font-size:small;text-align:right;text-decoration:underline;"><?php echo $userInfo['name'] ?> さん
      </div>
   </div>
  </header>
  <h2 style="text-align:center; margin-top">管理メニュー</h1>
  <div class="flex_box">
    <div class="box">
      <a href="file_setting.php" target="_blank" rel="noopener noreferrer">商品検索・価格表用のファイルの登録（DB作成）</a><br>
      <a href="control_photo_req.php" target="_blank" rel="noopener noreferrer">写真リクエストの管理</a><br>
      <a href="https://docs.google.com/forms/d/1OWx8LjpvkTxxsE0nBqLwV_EBRN6q5DaieDXokOWUII8/edit?usp=sharing_eip_m&invite=CNrluLcJ&ts=62e9de15" target="_blank" rel="noopener noreferrer">新商品検索システムのご意見・ご要望（集計結果）</a><br>
    </div>
  </div>
  <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
	    	    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div id="menu_button_top">
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
          <a href="javascript:void(0);" onclick="confirmLogout()">
	    	    <img src="image/logout.png" width="40" height="40" alt="logout" title="ログアウト" style="margin-top:5px;margin-right:5px;float:right;">
          </a>
		    </div>
	   </div>
   </div>
</body>
<script>
    function confirmLogout() {
        if (confirm('ログアウトします。よろしいですか？')) {
            window.location.href = 'login.php';
        }
    }
</script>
</html>