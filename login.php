<?php
    // セッションCookieの取得やクリア関数などの必要な処理やライブラリの読み込みをここに追加してください。
    // 例: require_once 'session.php';
    // 例: require_once 'clearCookie.php';

    $DOMEIN = $_SERVER['HTTP_HOST']; // ドメインURL

    if ($DOMEIN === "www.jasco.co.jp") {
        $path = "/intranet/sales/";
    } else {
        $path = "/";
    }

    // 現在の時刻よりも1時間前の時刻を取得
    $past_time = time() - 3600;

    // クッキーの有効期限を過去の時刻に設定することで、クッキーを削除する
    setcookie('CGISESSID', '', $past_time, $path, $DOMEIN);
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <!-- 省略... 元のmetaタグやlinkタグなどはここに -->
	<link rel="stylesheet" type="text/css" href="css/style_flex2.css?240721">
 	<link rel="stylesheet" type="text/css" href="css/under_bar_menu.css?24052301">
    <link rel="stylesheet" type="text/css" href="css/search_items.css?240727">
    <link rel='stylesheet' href='./css/normalize.css'>
    <link rel='stylesheet' href='./css/skeleton.css'>
    <style>
        p {
                font-size: 14px;
            }
    </style>
    <title>ログイン画面</title>
</head>
<body>
    <div style="text-align:center; background-color:#DC143C; opacity:0.7; color:white;"><?php echo htmlspecialchars($_GET['alert'] ) ?></div>
    <div style="display: flex;justify-content: center;">
        <img style="text-align:center; height:35px;" class="img_jascologo" src="image/jasco_logo_login.png">
	    <img style="height:38px;" id="center_title" class="img_jascologo" src="image/kensaku_login.png">
        <img style="text-align:center; height:35px;" class="img_jascologo" src="image/blank_login.png">
    </div>
    <hr style="size:140px; height:6px;border: none;  color: #11AB4F;background-color: #11AB4F; margin-top:0px;">
    <div style="text-align:center;font-size:16px;">
        <p style="width:630px; margin: 0 auto; background-color:#11AB4F;color:white;font-weight:bold; margin-top:20px;text-align:left;">・下記の項目を入力してログインしてください</p>
        <br><br>
        <form name="form" action="login_rec.php" method="post">
            <!--ユーザー名:&nbsp;<input type="text" name="user_name"style="background-color: #11AB4F;"><br><br>-->
            ユーザー名:&nbsp;<input type="text" name="user_name"><br><br>
            パスワード:&nbsp;<input type="password" name="password"><br><br><br>
            <button type="submit" class="search_button" style="height:60px;" title="ログイン">ログイン</button>
        </form>
        <br>
        <a href="../tokuchu/regist-password.cgi">新規登録の方はこちらをクリック</a>
        <br><br>
        <div style="width:630px; margin: 0 auto;">
            <hr style="width:630px;">
            <p style="text-align:left;">
                    　★お知らせ★<br>
                    　商品検索システムを使用するにあたり、ユーザー認証が必要となります。<br>
                    　特注システムをご使用の方は、同じユーザー名、パスワードでログインできます。<br>
                    　使用が初めての方は新規登録をお願いいたします。<br>
            </p>
            <p style="text-align:right;margin-right:10px;">2024.08.01</sp>
            <hr style="width:630px;">
        </div>
    </div>
</body>
</html>