
$(document).ready(function () {
    var DEP_MAINOFFICE = 1; // 本社
    var DEP_ABROAD = 2; // 海外事業部
    var DEP_DOMESTIC = 3; // SC
    var DEP_OUTSIDECMP_D1 = 4; // 国内卸1代理店
    var DEP_OUTSIDECMP_D2 = 5; // 国内卸2代理店
    var DEP_JE = 6; // ジャスコエンジ
    var DEP_JI = 7; // ジャスコインター
    var DEP_ABROAD_CH = 8; // 海外代理店(China)
    var DEP_ABROAD_US = 9; // 海外代理店(US)
    var DEP_ABROAD_EU = 10; // 海外代理店(EU)
    var def_hide_row;
    var def_adjust_row = 0;

    var viewType = document.getElementById('viewtype').value;
    var internationalItems = document.getElementById('international_items').value;
    var speed = 100;

    // 国内では海外分類を表示しないので1列少ない
    if (viewType == DEP_DOMESTIC) {
        def_hide_row = 3;  //一覧検索の国内
    }
    // 本社で海外チェック有　
    else if (viewType == DEP_MAINOFFICE && internationalItems) {
        def_hide_row = 4;  //本社       
    }
    // 本社で海外チェック無　
    else if (viewType == DEP_MAINOFFICE && !internationalItems) {
        def_hide_row = 5;  //本社       
    }
    else {
        def_hide_row = 4;  //それ以外       
    }

    // 本社で海外チェック有　
    if (viewType == DEP_MAINOFFICE && internationalItems) {

        // 英名、英名注釈、輸出価格、US卸、UE卸を折りたたむ
        setTimeout(function () { $(".international_items").hide(); }, speed);
        var tr = $("table tr");//全行を取得
        //alert(tr.length);
        for (var i = 0, l = tr.length; i < l; i++) {
            var cells = tr.eq(i).children();//1行目から順にth、td問わず列を取得
            //alert(cells.attr("colSpan"));
            if (cells.attr("colSpan") > 1) {
                var tmp = cells.attr("colSpan");
                var tmp2 = Number(tmp) - Number(def_hide_row);
                cells.attr("colSpan", tmp2);
            }
        }
         // colSpanの調整
         def_adjust_row = 4;
    }
    
    // 本社で海外チェック無
    if (viewType == DEP_MAINOFFICE && !internationalItems) {

        // 英名、英名注釈、輸出価格、US卸、UE卸を折りたたむ
        setTimeout(function () { $(".international_items").hide(); }, speed);
        setTimeout(function () { $(".international_must_items").hide(); }, speed);
        var tr = $("table tr");//全行を取得
        //alert(tr.length);
        for (var i = 0, l = tr.length; i < l; i++) {
            var cells = tr.eq(i).children();//1行目から順にth、td問わず列を取得
            //alert(cells.attr("colSpan"));
            if (cells.attr("colSpan") > 1) {
                var tmp = cells.attr("colSpan");
                var tmp2 = Number(tmp) - Number(def_hide_row);
                cells.attr("colSpan", tmp2);
            }
        }
         // colSpanの調整
         def_adjust_row = -1;
    }




    $("#toggle").click(function () {
        let element = document.getElementById('toggle');
        if (true == element.checked) {
            var data = [];
            var tr = $("table tr");//全行を取得
            
            // 本社で海外チェック有、トグルボタンを押して開く
            if (viewType == DEP_MAINOFFICE && internationalItems) {
                setTimeout(function () { $(".international_items").show(); }, speed);
            }            
            
            //alert(tr.length);
            for (var i = 0, l = tr.length; i < l; i++) {
                var cells = tr.eq(i).children();//1行目から順にth、td問わず列を取得
                //alert(cells.attr("colSpan"));
                if (cells.attr("colSpan") > 1) {
                    var tmp = cells.attr("colSpan");
                    var tmp2 = Number(tmp) + Number(def_hide_row) + Number(def_adjust_row);
                    cells.attr("colSpan", tmp2);
                }

                /*for( var j=0,m=cells.length;j<m;j++ ){
                    if( typeof data[i] == "undefined" )
                        data[i] = [];
                    data[i][j] = cells.eq(j).text();//i行目j列の文字列を取得
                    //alert(data[i][j] = cells.eq(j).text());
                    //alert(cells.eq(j).attr("colspan"));
                    //alert(cells.eq(j).attr("rowspan"));
                }*/
            }
            setTimeout(function () { $(".int_hiderow").show(); }, speed);
            setTimeout(function () { $(".fixed_col_int_hide").show(); }, speed);
        } else {  
            //$(".col_listnum").attr("rowspan",14);
            //alert("unchecked");
            var data = [];
            var tr = $("table tr");//全行を取得
            //alert(tr.length);
            for (var i = 0, l = tr.length; i < l; i++) {
                var cells = tr.eq(i).children();//1行目から順にth、td問わず列を取得
                //alert(cells.attr("colSpan"));
                if (cells.attr("colSpan") > 1) {
                    var tmp = cells.attr("colSpan");
                    var tmp2 = Number(tmp) - Number(def_hide_row) - Number(def_adjust_row);
                    cells.attr("colSpan", tmp2);
                }
            }
            
            // 本社で海外チェック有、トグルボタンを押して閉じる
            if (viewType == DEP_MAINOFFICE && internationalItems) {
                setTimeout(function () { $(".international_items").hide(); }, speed);
            }        
       
            setTimeout(function () { $(".int_hiderow").hide(); }, speed);
            setTimeout(function () { $(".fixed_col_int_hide").hide(); }, speed);
        }
    });
});