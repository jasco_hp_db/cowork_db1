
/*
$(document).ready(function() {    
    /*
    //id指定しないといけない
    $("#click_clipboard").click(function() {
        alert('id名「' + this.id + '」のボタンを押しました。');
    });//特定のボタン
    */
    /*
    //クリック全部　ボタンイベントの後
    document.body.onclick = function ( event ) {
        alert('id名「' + this.id + '」のボタンを押しました。');
    }
});*/

document.addEventListener('DOMContentLoaded', function(){
    function menuClick(){
        //alert('id名「' + this.id + '」のボタンを押しました。');
        /*  alert(tmp_code);*/
        var textVal = this.id;
        // テキストエリアを用意する
        var copyFrom = document.createElement("textarea");
        // テキストエリアへ値をセット
        copyFrom.textContent = textVal;
        // bodyタグの要素を取得
        var bodyElm = document.getElementsByTagName("body")[0];
        // 子要素にテキストエリアを配置
        bodyElm.appendChild(copyFrom);
        // テキストエリアの値を選択
        copyFrom.select();
        // コピーコマンド発行
        var retVal = document.execCommand('copy');
        //alert(retVal);
        // 追加テキストエリアを削除
        bodyElm.removeChild(copyFrom);
        alert(this.id + ' をコピーしました。');
    }
    // 引数に指定したclassの値をもつ要素をすべて取得
    const menus = document.getElementsByClassName('btn_click_clipboard');
    // 上記で取得したすべての要素に対してクリックイベントを適用
    for(let i = 0; i < menus.length; i++) {
        menus[i].addEventListener('click', menuClick, false);
    }
    
}, false);