// When the table click is clicked, 
//   set the focus to the text box.
$("#myTable").on("click",function(e){
    $("#tableEventShifter").focus();
});

// When clicked, select a row.
$("#myTable tbody tr").on("click",function(e){
// Deselect other selected rows.
$(this).siblings().removeClass("selected");

$(this).addClass("selected"); 
});

$("#tableEventShifter").on("keydown", function(e) {
    switch (e.keyCode) {
    case 38: // up
        $('#myTable tbody tr:not(:first).selected')
        .removeClass('selected')
        .prev().addClass('selected')
        break;
        
    case 40: // down
        $('#myTable tbody tr:not(:last).selected')
        .removeClass('selected')
        .next().addClass('selected')
        break;
    }
// Disable scroll
e.preventDefault();
});