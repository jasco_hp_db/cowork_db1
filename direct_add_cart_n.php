<?php
  $SearchWord = htmlspecialchars($_GET["List_CodeID"],ENT_QUOTES);
  //echo $SearchWord;
  //データベースへ接続設定
  require("./config/dbConnect.php");
  require("./config/filePath.php");
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM new_list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer WHERE code LIKE (:ProcName) ");
    if($statement){
      $like_SearchWord = "%".$SearchWord."%";
      //プレースホルダへ実際の値を設定する
      $statement->bindValue(':ProcName', $SearchWord, PDO::PARAM_STR);
      //echo "検索ワード:".$like_SearchWord."です。<br>";
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        //echo $row_count;
        //検索結果を配列に詰める
        //$arrSearhedProducts= new ArrayObject();
        
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $rows[] = $record;
        }
        //echo "検索結果:".$row_count."件です。<br>";
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  } 
  //データベースへ接続(セッションへ保存できるようになれば不要)
  //properties
  foreach($rows as $row){ //とりあえず$rowにつめる
  }
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>商品追加</title>
</head>
<script>
//変数storageにlocalStorageを格納//sessionstorageでも入れ替え可
var storage = localStorage;
var arJasco_List_Class = {
  JASCO_List_Rec_ver:"1161",//登録バージョン番号
  JASCO_List_Rec_No:"-1",//登録番号
  JASCO_List_chCode: "null",//商品番号
  JASCO_List_chNumber_string: "null",//品目番号
  JASCO_List_chProc_type: "null",//型式
  JASCO_List_chProc_name: "null",//商品名称
  JASCO_List_iPrice_p: "null",//販売価格
  JASCO_List_iPrice_d1: "null",//卸1
  JASCO_List_iPrice_d2: "null",//卸2
  JASCO_List_x_iPrice_d3: "null",//卸3
  JASCO_List_iPrice_m: "null",//工場卸
  JASCO_List_x_iGroup: "null",//販売ｸﾞﾙｰﾌﾟ
  JASCO_List_chSpecification: "null",//規格
  JASCO_List_chNotes: "null",//仕様・備考→（規格に入りきらない物）
  JASCO_List_x_chSite: "null",//ｻｲﾄ
  JASCO_List_chClass1: "null",//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  JASCO_List_chDomestic_classification: "null",//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  JASCO_List_chService_classification: "null",//価格表区分2→価格表区分（サービス）
  JASCO_List_chOverseas_classification: "null",//価格表区分3→価格表区分（輸出）
  JASCO_List_chEnd_date: "null",//有効終了日
  JASCO_List_x_chWhile_stocks_laste: "null",//在庫限り
  JASCO_List_chParts_centere: "null",//パーツセンタ対象
  JASCO_List_x_chValid: "null",//有効品目
  JASCO_List_chSubstitutional_goods: "null",//置換商品
  JASCO_List_x_chDiscontinuance: "null",//商品中止理由
  JASCO_List_chProc_name_eng: "null",//英名
  JASCO_List_chEng_annotation:"null",//英名注釈
  JASCO_List_iPrice_e:"null",//輸出価格
  JASCO_List_iPrice_us:"null",//US卸
  JASCO_List_iPrice_eu:"null",//EU卸
  JASCO_List_chPrecaution1: "null",//特記事項1
  JASCO_List_x_chPrecaution2: "null",//特記事項2
  JASCO_List_chInstrument_type: "null",//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  JASCO_List_chCompatible_models: "null",//対応機種/用途
  JASCO_List_chCategory: "null",//小見出しカテゴリー
  //写真リンク
  JASCO_List_chPicture: "null",//写真
  //sales infos
  JASCO_List_iSalesCount:"null",//登録数
  //time stamp
  JASCO_List_rec_timestamp:"0"//登録時刻
}
var arJasco_ProductRec = arJasco_List_Class;
//データを保存する
function set(){
  //alert("set()");
  arJasco_List_Class.JASCO_List_Rec_ver = 1161;//releaseバージョンと連動（構造が変わった時点でバージョンを適用）
  arJasco_List_Class.JASCO_List_Rec_No = 0;//0スタート
  arJasco_List_Class.JASCO_List_chCode = "<?php echo $row["code"];?>";
  arJasco_List_Class.JASCO_List_chNumber_string = "<?php echo $row["number_string"];?>";
  arJasco_List_Class.JASCO_List_chProc_type = "<?php echo $row["proc_type"];?>";
  arJasco_List_Class.JASCO_List_chProc_name = "<?php echo $row["proc_name"];?>";
  arJasco_List_Class.JASCO_List_iPrice_p = "<?php echo $row["price_p"];?>";
  arJasco_List_Class.JASCO_List_iPrice_d1 = "<?php echo $row["price_d1"];?>";
  arJasco_List_Class.JASCO_List_iPrice_d2 = "<?php echo $row["price_d2"];?>";
  arJasco_List_Class.JASCO_List_iPrice_m = "<?php echo $row["price_m"];?>";
  arJasco_List_Class.JASCO_List_chSpecification = "<?php echo $row["specification"];?>";
  arJasco_List_Class.JASCO_List_chNotes = "<?php echo $row["notes"];?>";
  arJasco_List_Class.JASCO_List_chClass1 = "<?php echo $row["class1"];?>";
  arJasco_List_Class.JASCO_List_chDomestic_classification = "<?php echo $row["domestic_classification"];?>";
  arJasco_List_Class.JASCO_List_chService_classification = "<?php echo $row["service_classification"];?>";
  arJasco_List_Class.JASCO_List_chOverseas_classification = "<?php echo $row["overseas_classification"];?>";
  arJasco_List_Class.JASCO_List_chEnd_date = "<?php echo $row["end_date"];?>";
  arJasco_List_Class.JASCO_List_chParts_centere = "<?php echo $row["parts_center"];?>";
  arJasco_List_Class.JASCO_List_chSubstitutional_goods = "<?php echo $row["substitutional_goods"];?>";
  arJasco_List_Class.JASCO_List_chProc_name_eng = "<?php echo $row["proc_name_eng"];?>";
  arJasco_List_Class.JASCO_List_chEng_annotation = "<?php echo $row["proc_eng_annotation"];?>";
  //arJasco_List_Class.JASCO_List_iPrice_e = "<?php echo $row["price_e"];?>";
  //arJasco_List_Class.JASCO_List_iPrice_us = "<?php echo $row["price_us"];?>";
  //arJasco_List_Class.JASCO_List_iPrice_eu = "<?php echo $row["price_eu"];?>";
  arJasco_List_Class.JASCO_List_chPrecaution1 = "<?php echo $row["precaution1"];?>";//そのまま渡すとSyntaxError: unexpected token: numeric literalが出る 多分"を使っている
  arJasco_List_Class.JASCO_List_chInstrument_type = "<?php echo $row["instrument_type"];?>";
  arJasco_List_Class.JASCO_List_chCompatible_models = "<?php echo $row["compatible_models"];?>";
  <?php
   $tmpstr_PictureName = filePath::$picture_path.$row["number_string"].".jpg";
   if(file_exists(filePath::$picture_path.$row["number_string"].".jpg")!=true){
     if(file_exists(filePath::$picture_path.$row["code"].".jpg")==true){
       $tmpstr_PictureName = filePath::$picture_path.$row["code"].".jpg";
     }
   }
  ?>
  arJasco_List_Class.JASCO_List_chPicture = "<?php echo $tmpstr_PictureName;?>";
  //arJasco_List_Class.JASCO_List_chPicture = "<?php if(file_exists(filePath::$picture_path.$row["number_string"].".jpg")==true){echo  filePath::$picture_path.$row["number_string"];}else if(file_exists(filePath::$picture_path.$row["code"].".jpg")==true){echo  filePath::$picture_path.$row["code"];}?>"+".jpg";
  arJasco_List_Class.JASCO_List_iSalesCount = 1;
  var date = new Date();
  var nowtimemm = date.getTime();
  arJasco_List_Class.JASCO_List_rec_timestamp = nowtimemm;//x:time();x:date("Y/m/d")
  var iSalesCount = 0;
  var iLogCount = 0;
  //同一の結果チェック

  for(var i=0; i<storage.length; i++){//入れた順番ではなくソートされているので注意    
    var k = storage.key(i);
    if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
    arJasco_ProductRec = JSON.parse(storage.getItem(k));
    iLogCount++;
    if(arJasco_ProductRec.JASCO_List_chCode == arJasco_List_Class.JASCO_List_chCode){
      iLogCount = arJasco_ProductRec.JASCO_List_Rec_No;
      iSalesCount = parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);  
      //alert("arJasco_ProductRec.JASCO_List_iSalesCount");
      iSalesCount++;
      arJasco_List_Class.JASCO_List_iSalesCount = iSalesCount;//個数カウントの上昇
      break;
    }
  }
  arJasco_List_Class.JASCO_List_Rec_No = iLogCount;
  var stockKey = "";
  stockKey = "Jasco_shopping_cart_" + arJasco_List_Class.JASCO_List_Rec_No;
  storage.setItem(stockKey, JSON.stringify(arJasco_List_Class));
  var strMsg = "<?php echo $row["code"];?>を追加しました";
  alert(strMsg);
  window.close();
}
</script>
<body onLoad="set()">
</body>
</html>