function init(){
    alert("TEST init()");
    //ページ表示後このfunctionが実行されます
    //もしもストレージにデータがあればテキストボックスに地名を書き出します
    document.form1.text1.value = localStorage.getItem("JASCO_Product_lNumber");//⇒localStorageへの記録自体は成功
    document.form1.text2.value = localStorage.getItem("JASCO_Product_strNumber");//⇒localStorageへの記録自体は成功
    document.form1.text3.value = localStorage.getItem("JASCO_Product_strType")//⇒localStorageへの記録自体は成功
}

function save(){
    alert("TEST save()");    //オブジェクトを一意のキーで保存する
    //var obj = {
    //  JASCO_Product_lNumber: document.form1.text1.value,
    //  JASCO_Product_strNumber: document.form1.text2.value,
    //  JASCO_Product_strType: document.form1.text3.value
    //};
    //文字列に一旦変換してから保存(JSONに限らず、オブジェクトを保存する場合は文字列に変換する)
    //var str = JSON.stringify(obj);
    //localStorage.setItem("SelectItem", str);      
    localStorage.setItem("JASCO_Product_lNumber", document.form1.text1.value)
    localStorage.setItem("JASCO_Product_strNumber", document.form1.text2.value)
    localStorage.setItem("JASCO_Product_strType", document.form1.text3.value)                                                                                                                                                                                                                                                                                                                                                                                                           
}

function load(){ 
    alert("TEST load()");    //オブジェクトを一意のキーで保存する   
    //JSONデータを取得する
    //var str = localStorage.getItem("SelectItem");
    //var obj = JSON.parse(str);
    //document.form1.text1.value = obj.JASCO_Product_lNumber;//⇒localStorageへの記録自体は成功
    //document.form1.text2.value = obj.JASCO_Product_strNumber;//⇒localStorageへの記録自体は成功
    //document.form1.text3.value = obj.JASCO_Product_strType;//⇒localStorageへの記録自体は成功
    document.form1.text1.value = localStorage.getItem("JASCO_Product_lNumber");//⇒localStorageへの記録自体は成功
    document.form1.text2.value = localStorage.getItem("JASCO_Product_strNumber");//⇒localStorageへの記録自体は成功
    document.form1.text3.value = localStorage.getItem("JASCO_Product_strType")//⇒localStorageへの記録自体は成功
}
