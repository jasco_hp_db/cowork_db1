function init(){
    alert("TEST init()");
    //ページ表示後このfunctionが実行されます
    //もしもストレージにデータがあればテキストボックスに地名を書き出します
    document.form1.text1.value = localStorage.getItem("area");//⇒localStorageへの記録自体は成功
}

function chkLL(){
    alert("TEST chkLL()");
    //アプリケーションID
    var appID = "nsw_test1";
    //JSONPの呼び出しURL主要部
    var URL = "http://geo.search.olp.yahooapis.jp/OpenLocalPlatform/V1/geoCode?appid=";
    //最後のcallback=の値は必ず呼び出すfunctionと合わせてください
    URL = URL+ appID + "&output=json&callback=foo";

    //フォームの入力から住所をクエリーに取り込み
    var ADDR = "&query=" + document.form1.text1.value;
    //<script>タグを生成
    var ADDJS = document.createElement("script");
    //呼び出しURLとquery=にフォームの住所を挿入
    ADDJS.src = URL + ADDR;
    //ADDJS.type = "application/javascript";//application/json
    ADDJS.type ='application/json';
    ADDJS.charset = 'utf-8';
    alert(ADDJS.src);
    //生成したscriptタグを書き出し
    document.body.appendChild(ADDJS);

    //areaというキーにテキストボックスの地名を書き出します
    //localStorage.setItem("area",document.form1.text1.value);//TEST⇒成功

    
    //試しに
    //http://geo.search.olp.yahooapis.jp/OpenLocalPlatform/V1/geoCode?appid=nsw_test1&output=json&callback=foo&query=大阪
    //直打ちすると
    //SyntaxError: JSON.parse: unexpected character at line 1 column 1 of the JSON data
    /*foo( {
        "Error" : {
        "Message" : "The URL You Requested Was Not Found"
        }
        } )
    */                                                                                                                                                                                                                                                                                                                                                                                                                                 
}

//JSONPの呼び出しが終わったら実行されるfunction
function foo(data){
    alert("TEST foo()");
    var view = document.getElementById("view");
    //データは10件返ってきますがその中の0番目のデータから
    //ズバリ直撃で緯度と経度を出します
    //地名の取得
    view.innerHTML = data["Feature"][0]["Name"]+"<br>";

    //読み仮名の取得
    view.innerHTML += "("+data["Feature"][0]["Property"]["Yomi"]+")<br>";

    var lotlon = new Array();
    lotlon = data["Feature"][0]["Geometry"]["Coordinates"].split(",");
    view.innerHTML += "東経:" + lotlon[0];
    view.innerHTML +="/";
    view.innerHTML += "北緯:" + lotlon[1];

    drawMAP(lotlon[0],lotlon[1]);

    //areaというキーにテキストボックスの地名を書き出します
    localStorage.setItem("area",document.form1.text1.value);
}

//地図の描画　引数として緯度と経度をジオコーダから渡します
function drawMAP(lot,lon){
    alert("TEST drawMAP()");
    var latlng = new google.maps.LatLng(lot,lon);
    var myOptions = {
        zoom:14,
        center:latlng,
        mapTypeID:google.maps.MapTypeID.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("vmap"),myOptions);
}