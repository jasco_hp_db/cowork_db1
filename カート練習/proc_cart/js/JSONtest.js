// localStorageのキー
var key = "test";  
 
// JSONデータ（初期値）
var obj = {
    foo: 'aaa',
    bar: 'bbb',
    hoge: 'ccc'
};
 
// localStorageに保存したデータの表示
var showStorage = function() {
    $('#result').empty();
    var obj = getObject();
    for(var key in obj){
        $('#result').append('<p>' + key + ':' + obj[key] + '<p>');
    }
};
 
// localStorageの文字列をJSONで取得
var getObject = function() {
    var str = localStorage.getItem(key);
    return JSON.parse(str);
};
 
// JSONを文字列でlocalStorageに保存
var setObject = function(obj) {
    var str = JSON.stringify(obj);
    localStorage.setItem(key, str);
};
 
$(function(){
 
    // キーと値の追加
    $('#put').click(function() {
        var key = $('#key').val();
        var value = $('#value').val();
        var obj = getObject();
        if (!obj) {
            obj = new Object();
        }
        obj[key] = value;
        setObject(obj);
        showStorage();
    });
 
    // キーで指定した値の削除
    $('#remove').click(function() {
        var key = $('#key').val();
        var obj = getObject();
        if (obj) {
            delete obj[key];
            setObject(obj);
            showStorage();
        }
    });
 
    // データの全削除
    $('#clear').click(function() {
        localStorage.clear();
        showStorage();
    });
 
    // 初期値をlocalStorageに保存（初回ロード時のみ）
    var data = getObject();
    if (!data) {
        setObject(obj);
    }
 
    // データの表示
    showStorage();
});