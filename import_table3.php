﻿<?php
  header("Content-type: text/html; charset=utf-8");
  //echo "検索ワード:".htmlspecialchars($_POST["input_searchWord"],ENT_QUOTES)."です。<br>";
  $text1 = '';
  $dummy_code = '';
  //データベースへ接続設定
  require("./config/dbConnect.php");
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！

    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = 0;
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(計算過程)の削除前<br>";
    
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer_proc'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(計算過程)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_proc");
      if($statement){
        if($statement->execute())
          echo "データベース(計算過程)の削除成功です。<br>";
        else
          echo "データベース(計算過程)の削失敗しました。<br>";
      }else{
        echo "データベース(計算過程)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //データベース(計算過程)の作成（枠だけ）
    echo "データベース(計算過程)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE new_list_set_buffer_proc(
    code varchar(32),
    number_string varchar(32),
    proc_type varchar(64),
    proc_name varchar(64),
    price_p varchar(32),
    price_d1 varchar(32),
    price_d2 varchar(32),
    x_price_d3 varchar(32),
    price_m varchar(32),
    x_group varchar(32),
    specification varchar(128),
    notes text,
    x_site varchar(16),
    class1 varchar(16),
    domestic_classification varchar(64),
    service_classification varchar(64),
    overseas_classification varchar(64),
    end_date varchar(64),
    x_while_stocks_last varchar(16),
    parts_center varchar(16),
    x_valid varchar(16),
    substitutional_goods varchar(32),
    x_stock_pc varchar(32),
    x_discontinuance varchar(512),
    x_insttype_dont_use varchar(512),
    proc_name_eng varchar(512),
    eng_annotation text,
    price_e varchar(32),
    price_d_us varchar(32),
    price_d_eu varchar(32),
    precaution1 varchar(128),
    x_precaution2 varchar(128),
    x_department_in_charge varchar(64),
    x_inventory_section varchar(64),
    x_start_date varchar(64)
    )");
    if($statement){
      if($statement->execute())
        echo "データベース(計算過程)の作成成功です。<br>";
      else
        echo "データベース(計算過程)の作成失敗しました。<br>";
    }else{
      echo "データベース(計算過程)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************
    echo "データベース(計算過程_ソート)の削除前<br>";
    //既存データベース(計算過程_ソート)の削除
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_proc_sort");
    if($statement){
      if($statement->execute())
        echo "データベース(計算過程_ソート)の削除成功です。<br>";
      else
        echo "データベース(計算過程_ソート)の削失敗しました。<br>";
    }else{
      echo "データベース(計算過程_ソート)の削失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /*********************************************
    //データベース(計算過程_ソート)の作成（枠だけ）処理用
    echo "データベース(計算過程_ソート)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_proc_sort LIKE new_list_set_buffer_proc");
    if($statement){
      if($statement->execute())
        echo "データベース(計算過程_ソート)の作成成功です。<br>";
      else
        echo "データベース(計算過程_ソート)の作成失敗しました。<br>";
    }else{
      echo "データベース(計算過程_ソート)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";

    /**********************************************/
    //処理1 new_list_set_buffer_procに処理をした前半配列を詰める
    /**********************************************/
    //新規テーブル準備
    $statement2 = $dbh -> prepare("INSERT INTO new_list_set_buffer_proc (code, number_string, 
    proc_type, proc_name, price_p, price_d1, price_d2, x_price_d3, price_m, x_group, specification, notes,
    x_site, class1, domestic_classification, service_classification, overseas_classification, 
    end_date, x_while_stocks_last, parts_center, x_valid, substitutional_goods, x_stock_pc, x_discontinuance,
    x_insttype_dont_use, proc_name_eng, eng_annotation, price_e,price_d_us,price_d_eu, precaution1, x_precaution2,
    x_department_in_charge, x_inventory_section, x_start_date) 
    VALUES (:code, :number_string, 
    :proc_type, :proc_name, :price_p, :price_d1, :price_d2, :x_price_d3, :price_m, :x_group, :specification, :notes,
    :x_site, :class1, :domestic_classification, :service_classification, :overseas_classification, 
    :end_date, :x_while_stocks_last, :parts_center, :x_valid, :substitutional_goods,:x_stock_pc,:x_discontinuance,
    :x_insttype_dont_use, :proc_name_eng, :eng_annotation, :price_e,:price_d_us,:price_d_eu,:precaution1, :x_precaution2,
    :x_department_in_charge, :x_inventory_section, :x_start_date)");
    $start = microtime(true);
    echo "処理1開始:".$start."です。<br>";
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh WHERE number_string = '' ");
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh WHERE number_string != '' ");
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh GROUP BY number_string");//6万件//GROUP BY number_string //品目番号が重複したものを削除  ブランクも重複と判断される
    //$statement = $dbh->prepare("SELECT DISTINCT number_string FROM new_list_set_buffer_fh");//6万件//GROUP BY number_string //品目番号が重複したものを削除  ブランクも重複と判断される
    //$statement = $dbh->prepare("DELETE FROM new_list_set_buffer_fh WHERE code NOT IN(SELECT min_code FROM (SELECT MIN(code) min_code FROM new_list_set_buffer_fh GROUP BY number_string) tmp)");
      $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh");//import_table1.phpにて重複削除済
      if($statement){

      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){

          //0000H1～0000H7、99961～99968、0000E/0000J/0000K/0000R は必ず追加する　　　→99961～99968商品登録中止の為登録削除
          //9996A1～99968,9996BTは必ず追加する →2022/11/21国内フラグ・価格が登録されたため自動登録。
          if(!preg_match('/^[0]{4}[H]{1}[0-9]{1}$/', $record["code"])&&
          !preg_match('/^[0]{4}[E,J,K,R]{1}$/', $record["code"])){

            //　　!preg_match('/^[9]{3}[6]{1}[1-9]{1}$/', $record["code"])&&　　　→99961～99968商品登録中止の為登録削除 →2022/11/21削除
            //   !preg_match('/^[9]{3}[6]{1}[A-Z]{1}[0-9,A-Z]{1}$/', $record["code"])&&　　 →2022/11/21削除

            //以下、飛ばすための条件を設定/////////

            //価格表区分1が「X」「SP」を削除
            if($record["domestic_classification"] == "X"||$record["domestic_classification"] == "SP")
              continue;
            //価格表区分1～3が空欄を削除
            if($record["domestic_classification"] == "" && $record["service_classification"] == ""&& $record["overseas_classification"] == "")
              continue;
            //価格～工場卸が空欄を削除
            if($record["price_p"] == "" && $record["price_d1"] == ""&& $record["price_d2"] == "" && $record["x_price_d3"] == ""&&  $record["price_m"] == "")
              continue;
            //価格～工場卸が「0」を削除
            if("0"==$record["price_p"]&&"0"==$record["price_d1"]&&"0"==$record["price_d2"]&&"0"==$record["x_price_d3"]&&"0"==$record["price_m"]){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //品目番号が2000から始まるものを削除
            //if("2000" == substr($record["code"], 0, 4))
            //  continue;

            //商品番号が「Sから始まる5桁」を削除 正規表現
            if(preg_match('/^[S]{1}[a-zA-Z0-9]{5}$/', $record["code"])){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //特注見積を削除 「6桁-」「6桁S」「6桁F」
            if(preg_match('/^[0-9]{6}-/',$record["code"])||
            preg_match('/^[0-9]{6}[S]+/',$record["code"])||
            preg_match('/^[0-9]{6}[F]+/',$record["code"])){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //指定商品番号(JE専用品)を削除
            if($record["code"] == "0501H106A"||
                $record["code"] == "10019001"||
                $record["code"] == "10019002"||
                $record["code"] == "10020218A"||
                $record["code"] == "11029003"||
                $record["code"] == "11029004"||
                $record["code"] == "11029005")
            continue;

            //指定商品番号(重点)を削除
            if($record["code"] == "GGJ1"||
                $record["code"] == "GGJ2"||
                $record["code"] == "GGJ3"||
                $record["code"] == "GGJ4"||
                $record["code"] == "GGJ5"||
                $record["code"] == "KK42"||
                $record["code"] == "KK43"||
                $record["code"] == "KK44"||
                $record["code"] == "KK45")
            continue;

            //飛ばすための条件を設定(ここまで)/////////

          }//0000H1～0000H7、99961～99968、0000E/0000J/0000K/0000R は飛ばさない ここまで
         
          //新規テーブルへ挿入
          if($statement2){
            $statement2->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement2->bindValue(':number_string', $record["number_string"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement2->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_price_d3', $record["x_price_d3"], PDO::PARAM_STR);
            $statement2->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
            $statement2->bindValue(':x_group', $record["x_group"], PDO::PARAM_STR);
            $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement2->bindValue(':x_site', $record["x_site"], PDO::PARAM_STR);
            $statement2->bindValue(':class1', $record["class1"], PDO::PARAM_STR);
            $statement2->bindValue(':domestic_classification', $record["domestic_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':service_classification', $record["service_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':overseas_classification', $record["overseas_classification"], PDO::PARAM_STR);
            //if($record["x_while_stocks_last"]=="TRUE"&&$record["parts_center"]=="TRUE")//在庫限りが「TRUE」と、パーツセンター対象に「TRUE」が入っている商品の有効終了日をブランクに
            //if($record["x_while_stocks_last"]=="TRUE")//在庫限りが「TRUE」が入っている商品の有効終了日をブランクに
            //  $statement2->bindValue(':end_date', "", PDO::PARAM_STR);
            //else
            if($record["end_date"] != "")
              $statement2->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR); //在庫限りを中止商品にしないために有効終了日の削除をなくした
            else
              $statement2->bindValue(':end_date', null, PDO::PARAM_STR);
            $statement2->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            $statement2->bindValue(':parts_center', $record["parts_center"], PDO::PARAM_STR);
            $statement2->bindValue(':x_valid', $record["x_valid"], PDO::PARAM_STR);
            $statement2->bindValue(':substitutional_goods', $record["substitutional_goods"], PDO::PARAM_STR);
            $statement2->bindValue(':x_stock_pc', $record["x_stock_pc"], PDO::PARAM_STR);
            $statement2->bindValue(':x_discontinuance', $record["x_discontinuance"], PDO::PARAM_STR);
            $statement2->bindValue(':x_insttype_dont_use', $record["x_insttype_dont_use"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name_eng', $record["proc_name_eng"], PDO::PARAM_STR);
            $statement2->bindValue(':eng_annotation', $record["eng_annotation"], PDO::PARAM_STR);
            $statement2->bindValue(':price_e', $record["price_e"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_us', $record["price_d_us"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_eu', $record["price_d_eu"], PDO::PARAM_STR);
            //$statement2->bindValue(':precaution1', $record["precaution1"], PDO::PARAM_STR);
            //$statement2->bindValue(':x_precaution2', $record["x_precaution2"], PDO::PARAM_STR);
            //$statement2->bindValue(':x_department_in_charge', $record["x_department_in_charge"], PDO::PARAM_STR);
            //$statement2->bindValue(':x_inventory_section', $record["x_inventory_section"], PDO::PARAM_STR);
            //$statement2->bindValue(':x_start_date', $record["x_start_date"], PDO::PARAM_STR);
            
            //PHP8.0で厳格化されたため元データ(IFSデータ）読み込み時に存在しない列は消した
            $statement2->bindValue(':precaution1', "", PDO::PARAM_STR);
            $statement2->bindValue(':x_precaution2', "", PDO::PARAM_STR);
            $statement2->bindValue(':x_department_in_charge', "", PDO::PARAM_STR);
            $statement2->bindValue(':x_inventory_section', "", PDO::PARAM_STR);
            $statement2->bindValue(':x_start_date', "", PDO::PARAM_STR);

            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件処理1成功です。<br>";
      }else{
        echo "処理1失敗です。<br>";
      }//execute
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "処理1失敗しました。";
      echo "処理2失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理1時間:".$sec."です。<br><br>";

    
    /**********************************************/
     //インデックスの作成
     $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_proc ADD INDEX `code_idx` (code)");
     if($statement){
       $statement->execute();
       echo "new_list_set_buffer_proc インデックス作成成功です。<br>";
     }else{
       echo "new_list_set_buffer_proc インデックス作成失敗しました。<br>";
     }    
    /*********************************************/

    /*********************************************/
    //処理2　特定の商品0000Eなどを追加  //GROUP BY number_string"で重複見ると消えてしまう
    /**********************************************
    $start = microtime(true);
    echo "処理2開始:".$start."です。<br>";
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh WHERE number_string = '' OR number_string IS NULL");//0000Eなど重複除外GROUP BY時に　number_stringがないものは飛ばされるので追加
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){

          //0000H1～0000H7、99961～99968、0000E/0000J/0000K/0000R は必ず追加する
          if(!preg_match('/^[0]{4}[H]{1}[0-9]{1}$/', $record["code"])&&
          !preg_match('/^[9]{3}[6]{1}[1-9]{1}$/', $record["code"])&&
          !preg_match('/^[0]{4}[E,J,K,R]{1}$/', $record["code"])){

            //以下、飛ばすための条件を設定/////////

            //価格表区分1が「X」「SP」を削除
            if($record["domestic_classification"] == "X"||$record["domestic_classification"] == "SP")
              continue;
            //価格表区分1～3が空欄を削除
            if($record["domestic_classification"] == "" && $record["service_classification"] == ""&& $record["overseas_classification"] == "")
              continue;
            //価格～工場卸が空欄を削除
            if($record["price_p"] == "" && $record["price_d1"] == ""&& $record["price_d2"] == "" && $record["x_price_d3"] == ""&&  $record["price_m"] == "")
              continue;
            //価格～工場卸が「0」を削除
            if("0"==$record["price_p"]&&"0"==$record["price_d1"]&&"0"==$record["price_d2"]&&"0"==$record["x_price_d3"]&&"0"==$record["price_m"]){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //品目番号が2000から始まるものを削除
            //if(strpos($record["number_string"],'2000-') !== false)
            if("2000" == substr($record["code"], 0, 4))
              continue;

            //商品番号が「Sから始まる5桁」を削除 正規表現
            if(preg_match('/^[S]{1}[a-zA-Z0-9]{5}$/', $record["code"])){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //特注見積を削除 「6桁-」「6桁S」「6桁F」
            if(preg_match('/^[0-9]{6}-/',$record["code"])||
            preg_match('/^[0-9]{6}[S]+/',$record["code"])||
            preg_match('/^[0-9]{6}[F]+/',$record["code"])){
              //echo $record["code"]."を除外しました。<br>";
              continue;
            }

            //指定商品番号(JE専用品)を削除
            if($record["code"] == "0501H106A"||
                $record["code"] == "10019001"||
                $record["code"] == "10019002"||
                $record["code"] == "10020218A"||
                $record["code"] == "11029003"||
                $record["code"] == "11029004"||
                $record["code"] == "11029005")
            continue;

            //指定商品番号(重点)を削除
            if($record["code"] == "GGJ1"||
                $record["code"] == "GGJ2"||
                $record["code"] == "GGJ3"||
                $record["code"] == "GGJ4"||
                $record["code"] == "GGJ5"||
                $record["code"] == "KK42"||
                $record["code"] == "KK43"||
                $record["code"] == "KK44"||
                $record["code"] == "KK45")
            continue;

            //飛ばすための条件を設定(ここまで)/////////

          }//0000H1～0000H7、99961～99968、0000E/0000J/0000K/0000R は飛ばさない ここまで

          //新規テーブルへ挿入
          if($statement2){
            $statement2->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement2->bindValue(':number_string', $record["number_string"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement2->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_price_d3', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
            $statement2->bindValue(':x_group', $record["x_group"], PDO::PARAM_STR);
            $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement2->bindValue(':x_site', $record["x_site"], PDO::PARAM_STR);
            $statement2->bindValue(':class1', $record["class1"], PDO::PARAM_STR);
            $statement2->bindValue(':domestic_classification', $record["domestic_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':service_classification', $record["service_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':overseas_classification', $record["overseas_classification"], PDO::PARAM_STR);
            //if($record["x_while_stocks_last"]=="TRUE"&&$record["parts_center"]=="TRUE")//在庫限りが「TRUE」と、パーツセンター対象に「TRUE」が入っている商品の有効終了日をブランクに
            //if($record["x_while_stocks_last"]=="TRUE")//在庫限りが「TRUE」が入っている商品の有効終了日をブランクに
            //  $statement2->bindValue(':end_date', "", PDO::PARAM_STR);
            //else
              $statement2->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR); //在庫限りを中止商品にしないために有効終了日の削除をなくした
            $statement2->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            $statement2->bindValue(':parts_center', $record["parts_center"], PDO::PARAM_STR);
            $statement2->bindValue(':x_valid', $record["x_valid"], PDO::PARAM_STR);
            $statement2->bindValue(':substitutional_goods', $record["substitutional_goods"], PDO::PARAM_STR);
            $statement2->bindValue(':x_stock_pc', $record["x_stock_pc"], PDO::PARAM_STR);
            $statement2->bindValue(':x_discontinuance', $record["x_discontinuance"], PDO::PARAM_STR);
            $statement2->bindValue(':x_insttype_dont_use', $record["x_insttype_dont_use"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name_eng', $record["proc_name_eng"], PDO::PARAM_STR);
            $statement2->bindValue(':eng_annotation', $record["eng_annotation"], PDO::PARAM_STR);
            $statement2->bindValue(':price_e', $record["price_e"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_us', $record["price_d_us"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_eu', $record["price_d_eu"], PDO::PARAM_STR);
            $statement2->bindValue(':precaution1', $record["precaution1"], PDO::PARAM_STR);
            $statement2->bindValue(':x_precaution2', $record["x_precaution2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_department_in_charge', $record["x_department_in_charge"], PDO::PARAM_STR);
            $statement2->bindValue(':x_inventory_section', $record["x_inventory_section"], PDO::PARAM_STR);
            $statement2->bindValue(':x_start_date', $record["x_start_date"], PDO::PARAM_STR);
            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件処理2成功です。<br>";
      }else{
        echo "処理2失敗です。<br>";
      }
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "処理2失敗しました。";
      echo "処理2失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理2時間:".$sec."です。<br><br>";
    
    /*********************************************/
    ////処理3 new_list_set_buffer_procを $ids = ["本体","付属品", "消耗品", "ソフトウェア", "技術料", "サービスパーツ", ""];順に並べ替える
    /**********************************************
    //新規テーブル準備
    $statement3 = $dbh -> prepare("INSERT INTO new_list_set_buffer_proc_sort (code, number_string, 
    proc_type, proc_name, price_p, price_d1, price_d2, x_price_d3, price_m, x_group, specification, notes,
    x_site, x_stock_pc, domestic_classification, service_classification, overseas_classification, 
    end_date, x_while_stocks_last, parts_center, x_valid, substitutional_goods, class1, class2, x_discontinuance,
    annotation, precaution1, x_precaution2, x_department_in_charge, x_inventory_section, proc_name_eng) 
    VALUES (:code, :number_string, 
    :proc_type, :proc_name, :price_p, :price_d1, :price_d2, :x_price_d3, :price_m, :x_group, :specification, :notes,
    :x_site, :x_stock_pc, :domestic_classification, :service_classification, :overseas_classification, 
    :end_date, :x_while_stocks_last, :parts_center, :x_valid, :substitutional_goods, :class1, :class2, :x_discontinuance,
    :annotation, :precaution1, :x_precaution2, :x_department_in_charge, :x_inventory_section,:proc_name_eng)");
    $start = microtime(true);
    echo "処理3開始:".$start."です。<br>";
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_proc");
    if($statement){
      $row_count = 0;

      $itest = 0;
      $ids = ["本体","付属品", "消耗品", "ソフトウェア", "技術料", "サービスパーツ", ""];
      for( $itest=0; $itest<7; $itest++){//execute と　fetchを　繰り返す為にここにしているが効率は悪い
        $currtclass1 = $ids[$itest];
        echo $currtclass1."<br>";

        if($statement->execute()){
          $row_count = $statement->rowCount();
          $i = 0;
          $dbh->beginTransaction();//必須
          while($record = $statement->fetch(PDO::FETCH_ASSOC)){

            if($currtclass1 != $record["class1"]) continue;

            //新規テーブルへ挿入
            if($statement3){
            $statement3->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement3->bindValue(':number_string', $record["number_string"], PDO::PARAM_STR);
            $statement3->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement3->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement3->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
            $statement3->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
            $statement3->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
            $statement3->bindValue(':x_price_d3', $record["price_d2"], PDO::PARAM_STR);
            $statement3->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
            $statement3->bindValue(':x_group', $record["x_group"], PDO::PARAM_STR);
            $statement3->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement3->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement3->bindValue(':x_site', $record["x_site"], PDO::PARAM_STR);
            $statement3->bindValue(':class1', $record["class1"], PDO::PARAM_STR);
            $statement3->bindValue(':domestic_classification', $record["domestic_classification"], PDO::PARAM_STR);
            $statement3->bindValue(':service_classification', $record["service_classification"], PDO::PARAM_STR);
            $statement3->bindValue(':overseas_classification', $record["overseas_classification"], PDO::PARAM_STR);
            //if($record["x_while_stocks_last"]=="TRUE"&&$record["parts_center"]=="TRUE")//在庫限りが「TRUE」と、パーツセンター対象に「TRUE」が入っている商品の有効終了日をブランクに
            //if($record["x_while_stocks_last"]=="TRUE")//在庫限りが「TRUE」が入っている商品の有効終了日をブランクに
            //  $statement3->bindValue(':end_date', "", PDO::PARAM_STR);
            //else
              $statement3->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR); //在庫限りを中止商品にしないために有効終了日の削除をなくした
            $statement3->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            $statement3->bindValue(':parts_center', $record["parts_center"], PDO::PARAM_STR);
            $statement3->bindValue(':x_valid', $record["x_valid"], PDO::PARAM_STR);
            $statement3->bindValue(':substitutional_goods', $record["substitutional_goods"], PDO::PARAM_STR);
            $statement3->bindValue(':x_stock_pc', $record["x_stock_pc"], PDO::PARAM_STR);
            $statement3->bindValue(':x_discontinuance', $record["x_discontinuance"], PDO::PARAM_STR);
            $statement3->bindValue(':x_insttype_dont_use', $record["x_insttype_dont_use"], PDO::PARAM_STR);
            $statement3->bindValue(':proc_name_eng', $record["proc_name_eng"], PDO::PARAM_STR);
            $statement3->bindValue(':eng_annotation', $record["eng_annotation"], PDO::PARAM_STR);
            $statement3->bindValue(':price_e', $record["price_e"], PDO::PARAM_STR);
            $statement3->bindValue(':price_d_us', $record["price_d_us"], PDO::PARAM_STR);
            $statement3->bindValue(':price_d_eu', $record["price_d_eu"], PDO::PARAM_STR);
            $statement3->bindValue(':precaution1', $record["precaution1"], PDO::PARAM_STR);
            $statement3->bindValue(':x_precaution2', $record["x_precaution2"], PDO::PARAM_STR);
            $statement3->bindValue(':x_department_in_charge', $record["x_department_in_charge"], PDO::PARAM_STR);
            $statement3->bindValue(':x_inventory_section', $record["x_inventory_section"], PDO::PARAM_STR);
            $statement3->bindValue(':x_start_date', $record["x_start_date"], PDO::PARAM_STR);
            if($statement3->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
          }//while
          $dbh->commit();//必須
          echo $i."件処理3成功です。<br>";
        }else{
          echo "処理3失敗です。<br>";
        }//execute
      }//for( $itest=0; $itest<7; $itest++)
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "処理3失敗しました。";
      echo "処理3失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理3時間:".$sec."です。<br><br>";

    /*********************************************/
    print('データベース登録完了！<br>');
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
  //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
  //echo "<script type='text/javascript'>history.back();</script>";//力技でウィンドウを閉じる

?>
