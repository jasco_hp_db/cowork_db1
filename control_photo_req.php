<?php
    require("./config/loadEnv.php");
    require("./config/getSessionUserInfo.php");

    $userInfo = getSessionUserInfo();

    // 開発者と営業管理室に編集権限付与
    if($userInfo['idno'] == "0811" || $userInfo['idno'] == "0858" || $userInfo['scode'] == 404 || $userInfo['scode'] == 401)
    {
      $Admin = 1;
    }
    else{
      $Admin = 0;     
    }

    if ($Admin == 0) {
        // ユーザー情報が取得できなかった場合の処理
        header("Location: login.php?alert=" . urlencode("ログインが必要です"));
        exit;
    }
    header("Content-type: text/html; charset=utf-8");
    
    //login**************************************************
    require("./config/section.php");
    $viewtype =  getsectiontype($userInfo['scode']);
    //echo "<br><br><br><br><br><br><br><br>表示：".$userInfo['name'].$viewtype."です。<br>";
    //viewtype**************************************************

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/photo_req_sheet.css">
  <title>写真リクエストの管理</title>
  <link rel="stylesheet" type="text/css" href="css/style_flex2.css?230721">
	<link rel="stylesheet" type="text/css" href="css/checkbox.css?230224">
	<link rel="stylesheet" type="text/css" href="css/search_items.css?230727">
 	<link rel="stylesheet" href="css/under_bar_menu.css?230722">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    //jQueryでcontentEditableなものの値の変更をイベントとして受け取る
    $(function(){
      $("[contentEditable]").each(function () {
        var $this = $(this);
        var htmlOld = $this.html();
        $this.on('blur keyup paste copy cut mouseup', function() {
            var htmlNew = $this.html();
            if (htmlOld !== htmlNew) {
                $this.trigger('change');
                htmlOld = htmlNew;
            }
        })
      });
    });
    $("[contentEditable]").on("change", function() {
      $('#result').text('通信中...');
      //console.log($(this).text());
      var text1 = $(this).text();
      var id = $(this).attr('id');
      //alert($(this).text());
      //alert(String(id));
      //location.href("add_req_phto.php?List_CodeID="+String(id)+"&Update_ReqCount="+$(this).text()+" target=\"_blank\"");//無限ループ
      $.ajax({
        //*************//残念ながら今回は使わない
        url: 'edit_req_phto.php',
        type: 'POST',
        // フォーム要素の内容をハッシュ形式に変換
        data: {
          List_CodeID: String(id),
          Update_ReqCount: text1,
        },
        //timeout: 5000,//50だと間に合わない
      }).done(function(data){
        $('#result').text('設定成功');
        location.reload();
      /* 通信成功時 */
      }).fail(function(data){
        $('#result').text('設定失敗');
      /* 通信失敗時 */
      }).always(function(data){
      /* 通信成功・失敗問わず */
      });
    });
  </script>
   <style>
            .flex_box{
                display: flex;
                justify-content: center;  /* 水平方向に中央寄せ */
                align-items: center;      /* 垂直方向に中央寄せ */
                height: 122vh;            /* ビューポート全体の高さを使用 */
                margin: 0;
                background-color: #ffffff; /* 背景色を設定 */
            }

            .box {
                width: 1040px;
                padding: 20px;
                background-color: #fff;
                font-size:14px;
                text-align: left; /* ボックス内の文字を左揃え */
            }
  </style>
</head>
<body>  
  <div class="main">
  <div class="flex_box">
	<div class="box">
<?php
  //データベースへ接続設定
  require("./config/dbConnect.php");  
  
  //写真フォルダパスの設定
  require("./config/filePath.php");
  $tmpstr_JASCO_List_chCode = "";//商品番号
  $tmpstr_JASCO_List_chNumber_string = "";//品目番号
  $tmpstr_JASCO_List_chUpdatedata = "";//更新日時
  $tmpstr_JASCO_List_chCount = "";//カウント数
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM new_list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    /*** */
    $statement = $dbh->prepare("SHOW TABLES LIKE 'request_photo_count'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
      }
    }
    if($testval==0){
      /**********************************************/
      //写真リクエスト用テーブルの作成(import_table4で作っているはずだが消してしまった場合も想定)
      $statement = $dbh->prepare("CREATE TABLE request_photo_count(
        code varchar(32),
        number_string varchar(32),
        updatedate varchar(64),
        req_count varchar(32)
        )");
        if($statement)
          $statement->execute();
      /**********************************************/
    }

    // delete=1の場合は最初に削除する
    $isDelete = isset($_GET['delete']) ? $_GET['delete'] : '';
    $itemCode = isset($_GET['code']) ? $_GET['code'] : '';
    if($isDelete){
      $statement3 = $dbh->prepare("UPDATE request_photo_count SET is_active = '0' WHERE code = '$itemCode'");
      $statement3->execute();    
    }

      
    //内訳表示***********************************************************************
    echo "<div style='position: absolute; top: 90px; left: 50%; transform: translateX(-50%);'>";
    echo "<h1>写真リクエストの管理</h1>";
    echo "<table class=\"req_table\" id=\"req_table\">";
    echo "<thead>";
    echo "<tr style='background-color:lightgray;'>";
    echo "<th>商品番号</th>";
    echo "<th>品目番号</th>";
    echo "<th><a href='control_photo_req.php?update=1'>更新日時</a></th>";
    echo "<th><a href='control_photo_req.php'>要求回数</a></th>";
    echo "<th>要求者</th>";
    echo "<th>削除</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    //***********************************************************************
    $num_now = 0;
    $updatetOrder = isset($_GET['update']) ? $_GET['update'] : '';
    
    if($updatetOrder){
    	$buffer ="SELECT * FROM request_photo_count WHERE is_active != 0 ORDER BY updatedate DESC";
    }else{
    	$buffer ="SELECT * FROM request_photo_count WHERE is_active != 0 ORDER BY LPAD(req_count,4,0) DESC";    
    }
    $statement = $dbh->prepare($buffer );
    if($statement){
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          //if($record["req_count"] > 0){
            $tmpstr_JASCO_List_chCode = $record["code"];//商品番号
            $tmpstr_JASCO_List_chNumber_string = $record["number_string"];//品目番号
            $tmpstr_JASCO_List_chUpdatedata = $record["updatedate"];//更新日時
            $tmpstr_JASCO_List_chCount = $record["req_count"];//要求回数
            $reqID = isset($record["req_id"]) ? $record["req_id"] : '';
	     // IDをアンダースコアで分割
	     $idArray = explode("_", $reqID);
	     $name = "";
            
	     // SQLクエリを直接埋め込む
	     if($reqID){	     
	     	try {
		    // 各IDに対して名前を取得して出力
		    foreach ($idArray as $id) {
		        // SQLクエリを直接埋め込む（注意：ここではインジェクション対策は省いていますが、本来は準備済みステートメントを使うべきです）
		        $sql = "SELECT name FROM gtest.UserAccountDB WHERE idno = $id";
		        // クエリを実行して結果を取得
		        $statement2 = $dbh->prepare($sql);
		        $statement2->execute();
		        // 結果をフェッチ
		        $result = $statement2->fetch(PDO::FETCH_ASSOC);
		        // データがあるか確認
		        if ($result) {
		            if($name){
		            	$name = $name . ", " . $result['name'];
		            }else{
		            	$name = $result['name'];
		            }		            
		        }
		    }	     	
		} catch (PDOException $e) {
    			// エラーメッセージを表示
    			echo "エラー: " . $e->getMessage();
		}
	     }
	     else{
	     	$name = "";
	     }
        
            //***********************************************************************
            if($tmpstr_JASCO_List_chCount>0){
              //echo $picture_path.$record["code"]."<br>";
              if(file_exists(filePath::$picture_path.$record["number_string"].".jpg")==false && file_exists(filePath::$picture_path.$record["code"].".jpg")==false){//商品コード＋jpg名出の登録もあり
                echo "<tr><td>".$record["code"]."</td>";
                echo "<td>".$record["number_string"]."</td>";
                echo "<td>".$record["updatedate"]."</td>";
                //echo "<td contentEditable=\"true\" id=\"".$record["code"]."\" style=\"background-color:skyblue;\">".$record["req_count"]."</td></tr>";
                echo "<td id=\"".$record["code"]."\">".$record["req_count"]."</td>";
                echo "<td>".$name."</td>";
                echo "<td><a href='#' onclick=\"confirmDeletion('control_photo_req.php?delete=1&code=" . $record["code"] . "', '" . $record["code"] . "'); return false;\">削除</a></td>";
                echo "</tr>";
              }else{
		    if($updatetOrder){
		    	$buffer ="SELECT * FROM request_photo_count WHERE is_active != 0 ORDER BY updatedate DESC";
		    }else{
		    	$buffer ="SELECT * FROM request_photo_count WHERE is_active != 0 ORDER BY LPAD(req_count,4,0) DESC";    
		    }
                $buffer ="UPDATE request_photo_count SET req_count = \"0\",updatedate = \"".date("Y/m/d H:i:s")."\" WHERE code = \"".$tmpstr_JASCO_List_chCode."\"";
                $statement2 = $dbh->prepare($buffer );
                if($statement2){      
                  if($statement2->execute()){
                  }
                }
              }
            }
            //***********************************************************************
          //}
        }
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
  echo "</tbody>";
  echo "</table>";  
  echo "</div>";
?>
	</div>
  </div>
  <div id="result"style="text-align:center;"></div>
  <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
	    	    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div id="menu_button_top">
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
          <a href="javascript:void(0);" onclick="confirmLogout()">
	    	    <img src="image/logout.png" width="40" height="40" alt="logout" title="ログアウト" style="margin-top:5px;margin-right:5px;float:right;">
          </a>
		    </div>
	   </div>
   </div>
</div>
</body>
<script>
    function confirmLogout() {
        if (confirm('ログアウトします。よろしいですか？')) {
            window.location.href = 'login.php';
        }
    }

    function confirmDeletion(url, number) {
        if (confirm(number + " を本当に削除しますか？")) {
            // ユーザーがOKを押した場合、指定したURLに遷移する
            window.location.href = url;
        }
    }
</script>
</html>