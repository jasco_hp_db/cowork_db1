﻿<?php
  //require_once("jasco_class_definition.php");
  header("Content-type: text/html; charset=utf-8");
  //写真フォルダパスの設定
    require("./config/filePath.php");
  //login***************************************************
  require("./config/loadEnv.php");
  require("./config/getSessionUserInfo.php");

  $userInfo = getSessionUserInfo();

  if (!$userInfo) {
      // ユーザー情報が取得できなかった場合の処理
      header("Location: login.php?alert=" . urlencode("ログインが必要です"));
      exit;
  }
  //login**************************************************
  require("./config/section.php");
  $viewtype =  getsectiontype($userInfo['scode']);
  //viewtype**************************************************
  
  //送信データの取得
  $SearchWord = htmlspecialchars($_GET["List_CodeID"],ENT_QUOTES);//($_POST["List_ResultID"]
  //echo "検索ID:".$SearchWord."です。<br>";  

  //データベースへ接続設定
  require("./config/dbConnect.php");
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM new_list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer WHERE code LIKE (:ProcName) ");
    if($statement){
      $like_SearchWord = "%".$SearchWord."%";
      //プレースホルダへ実際の値を設定する
      $statement->bindValue(':ProcName', $SearchWord, PDO::PARAM_STR);
      //echo "検索ワード:".$like_SearchWord."です。<br>";
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        //検索結果を配列に詰める
        //$arrSearhedProducts= new ArrayObject();
        
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $rows[] = $record;
        }
        //echo "検索結果:".$row_count."件です。<br>";
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  } 
  //データベースへ接続(セッションへ保存できるようになれば不要)
  //properties

  $tmpstr_JASCO_List_chCode = "";//商品番号
  $tmpstr_JASCO_List_chNumber_string = "";//品目番号
  $tmpstr_JASCO_List_chProc_type = "";//型式
  $tmpstr_JASCO_List_chProc_name = "";//商品名称
  $tmpstr_JASCO_List_iPrice_p = "";//販売価格
  $tmpstr_JASCO_List_iPrice_d1 = "";//卸1
  $tmpstr_JASCO_List_iPrice_d2 = "";//卸2
  $tmpstr_JASCO_List_x_iPrice_d3 = "";//卸3
  $tmpstr_JASCO_List_iPrice_m = "";//工場卸
  $tmpstr_JASCO_List_x_iGroup = "";//販売ｸﾞﾙｰﾌﾟ
  $tmpstr_JASCO_List_chSpecification = "";//規格
  $tmpstr_JASCO_List_chNotes = "";//仕様・備考→（規格に入りきらない物）
  $tmpstr_JASCO_List_x_chSite = "";//ｻｲﾄ
  $tmpstr_JASCO_List_x_iStock_pc = "";//引当可能在庫数（PC）
  $tmpstr_JASCO_List_chDomestic_classification = "";//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  $tmpstr_JASCO_List_chService_classification = "";//価格表区分2→価格表区分（サービス）
  $tmpstr_JASCO_List_chOverseas_classification = "";//価格表区分3→価格表区分（輸出）
  $tmpstr_JASCO_List_chEnd_date = "";//有効終了日
  $tmpstr_JASCO_List_x_chWhile_stocks_laste = "";//在庫限り
  $tmpstr_JASCO_List_chParts_centere = "";//パーツセンタ対象
  $tmpstr_JASCO_List_x_chValid = "";//有効品目
  $tmpstr_JASCO_List_chSubstitutional_goods = "";//置換商品
  $tmpstr_JASCO_List_x_chDiscontinuance = "";//商品中止理由
  $tmpstr_JASCO_List_chClass1 = "";//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  $tmpstr_JASCO_List_chProc_name_eng = "";//英名
  $tmpstr_JASCO_List_chEng_annotation = "";//英名注釈
  $tmpstr_JASCO_List_iPrice_e = "";//輸出価格
  $tmpstr_JASCO_List_iPrice_us = "";//US卸
  $tmpstr_JASCO_List_iPrice_eu = "";//EU卸

  $tmpstr_JASCO_List_iPrice_ch = "";//CH定価（販売価格＊1.5）IFSにはないデータ
  
  $tmpstr_JASCO_List_chPrecaution1 = "";//特記事項1
  $tmpstr_JASCO_List_chInstrument_type = "";//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  $tmpstr_JASCO_List_chCompatible_models = "";//対応機種/用途
  //$tmpstr_JASCO_List_chShift1 = "";//価格表区分1 
  //$tmpstr_JASCO_List_chShift2 = "";//価格表区分2
  //$tmpstr_JASCO_List_chCategory = "";//小見出しカテゴリー
  $tmpstr_JASCO_List_chEmplacement_cost = "";//搬入料(大型、中型、小型）
  //$tmpstr_JASCO_List_chMemo = "null";//メモ、要請欄

  foreach($rows as $row){ 
    if(isset($row["code"]))
      $tmpstr_JASCO_List_chCode = $row["code"];//商品番号
    if(isset($row["number_string"]))
      $tmpstr_JASCO_List_chNumber_string = $row["number_string"];//品目番号
    if(isset($row["proc_type"]))
      $tmpstr_JASCO_List_chProc_type = $row["proc_type"];//型式
    if(isset($row["proc_name"]))
      $tmpstr_JASCO_List_chProc_name = $row["proc_name"];//商品名称
    if(isset($row["price_p"]))
      $tmpstr_JASCO_List_iPrice_p = $row["price_p"];//定価
    if(isset($row["price_d1"]))
      $tmpstr_JASCO_List_iPrice_d1 = $row["price_d1"];//卸1
    if(isset($row["price_d2"]))
      $tmpstr_JASCO_List_iPrice_d2 = $row["price_d2"];//卸2
    if(isset($row["price_m"]))
      $tmpstr_JASCO_List_iPrice_m = $row["price_m"];//工場卸
    if(isset($row["specification"]))
      $tmpstr_JASCO_List_chSpecification = $row["specification"];//規格
    if(isset($row["notes"]))
      $tmpstr_JASCO_List_chNotes = $row["notes"];//仕様・備考
    if(isset($row["domestic_classification"]))
      $tmpstr_JASCO_List_chDomestic_classification = $row["domestic_classification"];//価格表区分（国内）    
    if(isset($row["service_classification"]))
      $tmpstr_JASCO_List_chService_classification = $row["service_classification"];//価格表区分（サービス）
    if(isset($row["overseas_classification"]))
      $tmpstr_JASCO_List_chOverseas_classification = $row["overseas_classification"];//価格表区分3→価格表区分（輸出）
    if(isset($row["end_date"]))
      $tmpstr_JASCO_List_chEnd_date = $row["end_date"];//有効終了日
    if(isset($row["x_while_stocks_last"]))
      $tmpstr_JASCO_List_x_chWhile_stocks_laste = $row["x_while_stocks_last"];
    if(isset($row["parts_center"]))
      $tmpstr_JASCO_List_chParts_centere = $row["parts_center"];//パーツセンタ対象   
    if(isset($row["substitutional_goods"]))
      $tmpstr_JASCO_List_chSubstitutional_goods = $row["substitutional_goods"];//置換商品
    if(isset($row["class1"]))
      $tmpstr_JASCO_List_chClass1 = $row["class1"];//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他） 
    if(isset($row["proc_name_eng"]))
      $tmpstr_JASCO_List_chProc_name_eng = $row["proc_name_eng"];//英名
    if(isset($row["eng_annotation"]))
      $tmpstr_JASCO_List_chEng_annotation = $row["eng_annotation"];//英注釈
    if(isset($row["price_e"]))
      $tmpstr_JASCO_List_iPrice_e = $row["price_e"];//輸出価格
    if(isset($row["price_d_us"]))
      $tmpstr_JASCO_List_iPrice_us = $row["price_d_us"];//US卸
    if(isset($row["price_d_eu"]))
      $tmpstr_JASCO_List_iPrice_eu = $row["price_d_eu"];//EU卸
    if(isset($row["precaution1"]))
      $tmpstr_JASCO_List_chPrecaution1 = $row["precaution1"];//特記事項1
    if(isset($row["instrument_type"]))
      $tmpstr_JASCO_List_chInstrument_type = $row["instrument_type"];//機種分類    
    if(isset($row["compatible_models"]))
      $tmpstr_JASCO_List_chCompatible_models = $row["compatible_models"];//対応機種/用途
    //if(isset($row["emplacement_cost"]))
      //$tmpstr_JASCO_List_chEmplacement_cost = $row["emplacement_cost"];/搬入料(大型、中型、小型）

      $tmpstr_JASCO_List_iPrice_ch = $tmpstr_JASCO_List_iPrice_p * 1.5;

  }
  $tmpstr_JASCO_List_rec_timestamp = time();//date("Y/m/d H:i:s");/strtotime("now")
  
  //定価
  if($tmpstr_JASCO_List_iPrice_p!=0)
    $tmpstr_JASCO_List_dPriceP_view = number_format($tmpstr_JASCO_List_iPrice_p);
  else
    $tmpstr_JASCO_List_dPriceP_view = "";
  //卸1
  if($tmpstr_JASCO_List_iPrice_d1!=0)
    $tmpstr_JASCO_List_dPriceD1_view = number_format($tmpstr_JASCO_List_iPrice_d1);
  else
    $tmpstr_JASCO_List_dPriceD1_view = "";
  //卸2
  if($tmpstr_JASCO_List_iPrice_d2!=0)
    $tmpstr_JASCO_List_dPriceD2_view = number_format($tmpstr_JASCO_List_iPrice_d2);
  else
    $tmpstr_JASCO_List_dPriceD2_view = "";
  //工場卸
  $tmpstr_JASCO_List_dPriceM_view = number_format($tmpstr_JASCO_List_iPrice_m);

  //輸出価格
  if($tmpstr_JASCO_List_iPrice_e!=0)
    $tmpstr_JASCO_List_iPrice_e_view = number_format($tmpstr_JASCO_List_iPrice_e);
  else
    $tmpstr_JASCO_List_iPrice_e_view = "";
  //US卸
  if($tmpstr_JASCO_List_iPrice_us!=0)
    $tmpstr_JASCO_List_iPrice_us_view = number_format($tmpstr_JASCO_List_iPrice_us);
  else
    $tmpstr_JASCO_List_iPrice_us_view = "";
  //EU卸
  if($tmpstr_JASCO_List_iPrice_eu!=0)
    $tmpstr_JASCO_List_iPrice_eu_view = number_format($tmpstr_JASCO_List_iPrice_eu);
  else
    $tmpstr_JASCO_List_iPrice_eu_view = "";

  //CH定価
  if($tmpstr_JASCO_List_iPrice_ch!=0)
   $tmpstr_JASCO_List_iPrice_ch_view = number_format($tmpstr_JASCO_List_iPrice_ch);
  else
    $tmpstr_JASCO_List_iPrice_ch_view = "";

  $tmpstr_PictureName = filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg";
  if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg")!=true){
    if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg")==true){
      $tmpstr_PictureName = filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg";
    }
  }
  //echo "<script>alert('$tmpstr_PictureName');</script>";

  $today = date("Y/m/d");
?>

<script>
//変数storageにlocalStorageを格納//sessionstorageでも入れ替え可
var storage = localStorage;
var arJasco_List_Class = {
  JASCO_List_Rec_ver:"1161",//登録バージョン番号
  JASCO_List_Rec_No:"-1",//登録番号
  JASCO_List_chCode: "null",//商品番号
  JASCO_List_chNumber_string: "null",//品目番号
  JASCO_List_chProc_type: "null",//型式
  JASCO_List_chProc_name: "null",//商品名称
  JASCO_List_iPrice_p: "null",//販売価格
  JASCO_List_iPrice_d1: "null",//卸1
  JASCO_List_iPrice_d2: "null",//卸2
  JASCO_List_x_iPrice_d3: "null",//卸3
  JASCO_List_iPrice_m: "null",//工場卸
  JASCO_List_x_iGroup: "null",//販売ｸﾞﾙｰﾌﾟ
  JASCO_List_chSpecification: "null",//規格
  JASCO_List_chNotes: "null",//仕様・備考→（規格に入りきらない物）
  JASCO_List_x_chSite: "null",//ｻｲﾄ
  JASCO_List_chClass1: "null",//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  JASCO_List_chDomestic_classification: "null",//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  JASCO_List_chService_classification: "null",//価格表区分2→価格表区分（サービス）
  JASCO_List_chOverseas_classification: "null",//価格表区分3→価格表区分（輸出）
  JASCO_List_chEnd_date: "null",//有効終了日
  JASCO_List_x_chWhile_stocks_laste: "null",//在庫限り
  JASCO_List_chParts_centere: "null",//パーツセンタ対象
  JASCO_List_x_chValid: "null",//有効品目
  JASCO_List_chSubstitutional_goods: "null",//置換商品
  JASCO_List_x_chDiscontinuance: "null",//商品中止理由
  JASCO_List_chProc_name_eng: "null",//英名
  JASCO_List_chEng_annotation:"null",//英名注釈
  JASCO_List_iPrice_e:"null",//輸出価格
  JASCO_List_iPrice_us:"null",//US卸
  JASCO_List_iPrice_eu:"null",//EU卸
  JASCO_List_chPrecaution1: "null",//特記事項1
  JASCO_List_x_chPrecaution2: "null",//特記事項2
  JASCO_List_chInstrument_type: "null",//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  JASCO_List_chCompatible_models: "null",//対応機種/用途
  JASCO_List_chCategory: "null",//小見出しカテゴリー
  //写真リンク
  JASCO_List_chPicture: "null",//写真
  //sales infos
  JASCO_List_iSalesCount:"null",//登録数
  //time stamp
  JASCO_List_rec_timestamp:"0"//登録時刻
}
var arJasco_ProductRec = arJasco_List_Class;
//データを保存する
function set(){
  //alert("set()");
  arJasco_List_Class.JASCO_List_Rec_ver = 1161;//releaseバージョンと連動（構造が変わった時点でバージョンを適用）
  arJasco_List_Class.JASCO_List_Rec_No = 0;//0スタート
  arJasco_List_Class.JASCO_List_chCode =  "<?php echo  $tmpstr_JASCO_List_chCode;?>";
  arJasco_List_Class.JASCO_List_chNumber_string =  "<?php echo  $tmpstr_JASCO_List_chNumber_string;?>";
  arJasco_List_Class.JASCO_List_chProc_type =  "<?php echo  $tmpstr_JASCO_List_chProc_type;?>";
  arJasco_List_Class.JASCO_List_chProc_name =  "<?php echo  $tmpstr_JASCO_List_chProc_name;?>";
  arJasco_List_Class.JASCO_List_iPrice_p =  "<?php echo  $tmpstr_JASCO_List_iPrice_p;?>";
  arJasco_List_Class.JASCO_List_iPrice_d1 =  "<?php echo  $tmpstr_JASCO_List_iPrice_d1;?>";
  arJasco_List_Class.JASCO_List_iPrice_d2 =  "<?php echo  $tmpstr_JASCO_List_iPrice_d2;?>";
  arJasco_List_Class.JASCO_List_iPrice_m =  "<?php echo  $tmpstr_JASCO_List_iPrice_m;?>";
  arJasco_List_Class.JASCO_List_chSpecification =  "<?php echo  $tmpstr_JASCO_List_chSpecification;?>";
  arJasco_List_Class.JASCO_List_chNotes =  "<?php echo  $tmpstr_JASCO_List_chNotes;?>";
  arJasco_List_Class.JASCO_List_chClass1 =  "<?php echo  $tmpstr_JASCO_List_chClass1;?>";
  arJasco_List_Class.JASCO_List_chDomestic_classification =  "<?php echo  $tmpstr_JASCO_List_chDomestic_classification;?>";
  arJasco_List_Class.JASCO_List_chService_classification =  "<?php echo  $tmpstr_JASCO_List_chService_classification;?>";
  arJasco_List_Class.JASCO_List_chOverseas_classification =  "<?php echo  $tmpstr_JASCO_List_chOverseas_classification;?>";
  arJasco_List_Class.JASCO_List_chEnd_date =  "<?php echo  $tmpstr_JASCO_List_chEnd_date;?>";
  arJasco_List_Class.JASCO_List_chParts_centere =  "<?php echo  $tmpstr_JASCO_List_chParts_centere;?>";
  arJasco_List_Class.JASCO_List_chSubstitutional_goods =  "<?php echo  $tmpstr_JASCO_List_chSubstitutional_goods;?>";
  arJasco_List_Class.JASCO_List_chProc_name_eng =  "<?php echo  $tmpstr_JASCO_List_chProc_name_eng;?>";
  arJasco_List_Class.JASCO_List_chEng_annotation = "<?php echo $tmpstr_JASCO_List_chEng_annotation;?>";
  //arJasco_List_Class.JASCO_List_iPrice_e = "<?php echo $tmpstr_JASCO_List_iPrice_e;?>";
  //arJasco_List_Class.JASCO_List_iPrice_us = "<?php echo $tmpstr_JASCO_List_iPrice_us;?>";
  //arJasco_List_Class.JASCO_List_iPrice_eu = "<?php echo $tmpstr_JASCO_List_iPrice_eu;?>";  
  arJasco_List_Class.JASCO_List_chPrecaution1 =  "<?php echo  $tmpstr_JASCO_List_chPrecaution1;?>";
  arJasco_List_Class.JASCO_List_chInstrument_type =  "<?php echo  $tmpstr_JASCO_List_chInstrument_type;?>";
  arJasco_List_Class.JASCO_List_chCompatible_models =  "<?php echo  $tmpstr_JASCO_List_chCompatible_models;?>";
  <?php
   $tmpstr_PictureName = filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg";
   if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg")!=true){
     if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg")==true){
       $tmpstr_PictureName = filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg";
     }
   }
  ?>
  arJasco_List_Class.JASCO_List_chPicture = "<?php echo $tmpstr_PictureName;?>";
  arJasco_List_Class.JASCO_List_iSalesCount = 1;
  arJasco_List_Class.JASCO_List_rec_timestamp =  "<?php echo  $tmpstr_JASCO_List_rec_timestamp;?>";
  var iSalesCount = 0;
  var iLogCount = 0;
  //同一の結果チェック
  for(var i=0; i<storage.length; i++){//入れた順番ではなくソートされているので注意    
    var k = storage.key(i);
    if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
    arJasco_ProductRec = JSON.parse(storage.getItem(k));
    iLogCount++;
    if(arJasco_ProductRec.JASCO_List_chCode == arJasco_List_Class.JASCO_List_chCode){
      iSalesCount = parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);  
      //alert("arJasco_ProductRec.JASCO_List_iSalesCount");
      iSalesCount++;
      arJasco_List_Class.JASCO_List_iSalesCount = iSalesCount;//個数カウントの上昇
      break;
    }
  }
  arJasco_List_Class.JASCO_List_Rec_No = iLogCount;
  var stockKey = "";
  stockKey = "Jasco_shopping_cart_" + arJasco_List_Class.JASCO_List_Rec_No;
  storage.setItem(stockKey, JSON.stringify(arJasco_List_Class));
}
function cle() {
  storage.clear();
}
//クリップボードへコピー
function copyToClipboard() {
  var textVal ="null";
  textVal = "商品番号: "+"<?php echo  $tmpstr_JASCO_List_chCode;?>"+"\r\n";
   //textVal += "品目番号: "+"<?php echo  $tmpstr_JASCO_List_chNumber_string;?>"+"\r\n";
  textVal += "型式: "+"<?php echo  $tmpstr_JASCO_List_chProc_type;?>"+"\r\n";
  textVal += "商品名称: "+"<?php echo  $tmpstr_JASCO_List_chProc_name;?>"+"\r\n";
  textVal += "販売価格(税抜き): "+"<?php echo $tmpstr_JASCO_List_dPriceP_view;?>"+"\r\n";
  textVal += "規格: "+"<?php echo  $tmpstr_JASCO_List_chSpecification;?>"+"\r\n";
  //textVal += "仕様・備考: "+"<?php echo  $tmpstr_JASCO_List_chNotes;?>"+"\r\n";
   //textVal += "対応機種/用途: "+"<?php echo  $tmpstr_JASCO_List_chCompatible_models;?>";

  // テキストエリアを用意する
  var copyFrom = document.createElement("textarea");
  // テキストエリアへ値をセット
  copyFrom.textContent = textVal;
 
  // bodyタグの要素を取得
  var bodyElm = document.getElementsByTagName("body")[0];
  // 子要素にテキストエリアを配置
  bodyElm.appendChild(copyFrom);
 
  // テキストエリアの値を選択
  copyFrom.select();
  // コピーコマンド発行
  var retVal = document.execCommand('copy');
  // 追加テキストエリアを削除
  bodyElm.removeChild(copyFrom);

  // コピー結果をデバッグ
  alert("クリップボードへコピーしました。");
}

function copyToClipboard_d1() {
  var textVal ="null";
  textVal = "商品番号: "+"<?php echo  $tmpstr_JASCO_List_chCode;?>"+"\r\n";
  textVal += "型式: "+"<?php echo  $tmpstr_JASCO_List_chProc_type;?>"+"\r\n";
  textVal += "商品名称: "+"<?php echo  $tmpstr_JASCO_List_chProc_name;?>"+"\r\n";
  textVal += "販売価格(税抜き): "+"<?php echo $tmpstr_JASCO_List_dPriceP_view;?>"+"\r\n";
  textVal += "貴社卸(税抜き): "+"<?php echo $tmpstr_JASCO_List_dPriceD1_view;?>"+"\r\n";
  textVal += "規格: "+"<?php echo  $tmpstr_JASCO_List_chSpecification;?>"+"\r\n";

  // テキストエリアを用意する
  var copyFrom = document.createElement("textarea");
  // テキストエリアへ値をセット
  copyFrom.textContent = textVal;
 
  // bodyタグの要素を取得
  var bodyElm = document.getElementsByTagName("body")[0];
  // 子要素にテキストエリアを配置
  bodyElm.appendChild(copyFrom);
 
  // テキストエリアの値を選択
  copyFrom.select();
  // コピーコマンド発行
  var retVal = document.execCommand('copy');
  // 追加テキストエリアを削除
  bodyElm.removeChild(copyFrom);

  // コピー結果をデバッグ
  alert("卸1 をクリップボードへコピーしました。");
}

function copyToClipboard_d2() {
  var textVal ="null";
  textVal = "商品番号: "+"<?php echo  $tmpstr_JASCO_List_chCode;?>"+"\r\n";
  textVal += "型式: "+"<?php echo  $tmpstr_JASCO_List_chProc_type;?>"+"\r\n";
  textVal += "商品名称: "+"<?php echo  $tmpstr_JASCO_List_chProc_name;?>"+"\r\n";
  textVal += "販売価格(税抜き): "+"<?php echo $tmpstr_JASCO_List_dPriceP_view;?>"+"\r\n";
  textVal += "貴社卸(税抜き): "+"<?php echo $tmpstr_JASCO_List_dPriceD2_view;?>"+"\r\n";
  textVal += "規格: "+"<?php echo  $tmpstr_JASCO_List_chSpecification;?>"+"\r\n";

  // テキストエリアを用意する
  var copyFrom = document.createElement("textarea");
  // テキストエリアへ値をセット
  copyFrom.textContent = textVal;
 
  // bodyタグの要素を取得
  var bodyElm = document.getElementsByTagName("body")[0];
  // 子要素にテキストエリアを配置
  bodyElm.appendChild(copyFrom);
 
  // テキストエリアの値を選択
  copyFrom.select();
  // コピーコマンド発行
  var retVal = document.execCommand('copy');
  // 追加テキストエリアを削除
  bodyElm.removeChild(copyFrom);

  // コピー結果をデバッグ
  alert("卸2 をクリップボードへコピーしました。");
}

//写真追加請求
function request_camera() {
  //alert("request_camera");
  var Ret = window.confirm("商品番号："+"<?php echo  $tmpstr_JASCO_List_chCode;?>"+" の写真リクエストで間違いありませんか？");
  if(Ret){
    const url = "add_req_phto.php?List_CodeID="+"<?php echo $tmpstr_JASCO_List_chCode;?>";
    window.open(url, '_blank')
  }
}
</script>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>商品詳細: <?php echo $tmpstr_JASCO_List_chCode;?></title>
	<link rel="stylesheet" type="text/css" href="css/details.css?20230614">
  <link rel="stylesheet" href="css/under_bar_menu.css?20230309">
  <link rel="stylesheet" href="css/flat_dropdown_menu.css?20230309">
  <link rel="stylesheet" href="../lightbox2-2.11.2/dist/css/lightbox.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css">
  <link rel="stylesheet" type="text/css" href="css/style_flex2.css?230322">
	<!--link rel="stylesheet" type="text/css" href="css/disable_focus_outline.css"-->
  <!--<script type="text/javascript" src="js/intense.js"></script>-->
</head>
<style type="text/css">
  .fullscreen-image {
     cursor: url("img/plus_cursor.png"), pointer; /* マウスポインタを指定 */
     display: inline-block;   /* 横方向に並べる指定 */
     margin: 0px 5px 5px 0px; /* 周囲の余白量(右と下に5pxずつ) */
  }
</style>
<body>
  <h1></h1><br><br><br><br>
  <?php require("./component/return.php"); ?>
     <!--中止商品警告-->
    <?php 
    $tmpstr_Caution = "";
    if($tmpstr_JASCO_List_chEnd_date != 0 && $tmpstr_JASCO_List_chEnd_date != "" && strtotime($today) > strtotime($tmpstr_JASCO_List_chEnd_date))
      if($tmpstr_JASCO_List_x_chWhile_stocks_laste != "TRUE"){
        $tmpstr_Caution = "この商品は中止商品です。";
      }else{
        $tmpstr_Caution = "この商品は在庫限りで販売終了となります。";
      }
    ?>
    
    <?php 
    if($tmpstr_JASCO_List_chEnd_date != 0 && $tmpstr_JASCO_List_chEnd_date != "" && strtotime($today) > strtotime($tmpstr_JASCO_List_chEnd_date)){
    ?>
    <table>
    <!--phpをちりばめて動かしています。-->
    <tbody>
          <tr valign="bottom">
            <th class="caution0" colspan="2"><?php if($tmpstr_Caution != ""){echo $tmpstr_Caution;}?></th>
          </tr>
        <!--商品番号-->
          <tr class="unabletobuy_cell">
            <th class="unabletobuy_cell" style="width:170px;">有効終了日</th>
            <!--　<td class="unabletobuy_cell"><?php echo $tmpstr_JASCO_List_chEnd_date;?></td>　-->
            <td class="unabletobuy_cell"><?php echo str_replace("-", "/", $tmpstr_JASCO_List_chEnd_date);?></td><!--表記修正　2022-01-01　→　2022/01/01 -->
          </tr>
          <tr class="unabletobuy_cell">
            <th class="unabletobuy_cell" style="width:170px;">置換商品</th>
            <td class="unabletobuy_cell"><a href="detail_list_window_n3.php?List_CodeID=<?php echo $tmpstr_JASCO_List_chSubstitutional_goods;?>" target="_blank"><?php echo $tmpstr_JASCO_List_chSubstitutional_goods;?></a></td>
          </tr>
    </tbody>
    </table>
    <br>
    <?php } ?>
 <!--IF国内表示ここから-->
  <?php 
    if($viewtype == DEP_MAINOFFICE ||$viewtype == DEP_DOMESTIC ||$viewtype == DEP_OUTSIDECMP_D1 ||$viewtype == DEP_OUTSIDECMP_D2 || $viewtype == DEP_JE){
  ?>
    <table>
    <!--phpをちりばめて動かしています。-->
    <tbody>
          <tr class=XXX>
            <th class="caution1" colspan="2">*帳票類(納品請求書、出荷ラベルなど)記載項目</th>
          </tr>
        <!--商品番号-->
          <tr>
            <th class=XXX style="width:170px;">商品番号<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chCode;?>
            <div style='display:inline;'> </div>
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chCode;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--品目番号-->
          <tr>
            <th class=XXX>品目番号<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chNumber_string;?>
            <div style='display:inline;'> </div>
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chNumber_string;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--型式-->
          <tr>
            <th class=XXX>型　式<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_type;?>
            <div style='display:inline;'> </div>  
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chProc_type;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--商品名称-->
          <tr>
            <th class=XXX>商品名称<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_name;?>
            <div style='display:inline;'> </div>
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chProc_name;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--規格-->
          <tr>
            <th class=XXX>規　格<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chSpecification;?></td>
          </tr>
        <!--仕様・備考-->
          <tr>
            <th class=XXX>仕様 備考</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chNotes;?></td>
          </tr>
        <!--対応機種/用途-->
          <tr>
            <th class=XXX>対応機種/用途</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chCompatible_models;?></td>
          </tr>
        <!--英名
          <tr>
            <th class=XXX>英　名<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_name_eng;?></td>
          </tr> -->
   <!-- </tbody>
    </table>
    <table>
    <tbody> -->
          <tr>
            <th class="caution1">税抜き価格</th>
          </tr>
        <!--販売価格-->
          <tr>
            <th class=XXX style="width:170px;">販売価格(定価)</th>
            <td class=sale_price><?php echo $tmpstr_JASCO_List_dPriceP_view;?></td>
          </tr>
        <!--卸1-->
          <tr>
            <th class=XXX>卸　1</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceD1_view;?></td>
          </tr>
        <!--卸2-->
          <tr>
            <th class=XXX>卸　2</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceD2_view;?></td>
          </tr>
        <!--工場卸<-->
          <tr>
            <th class=XXX>工場卸</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceM_view;?></td>
          </tr>

    <?php if($viewtype == DEP_MAINOFFICE){?><!--海外関係の項目（本社用-->
      
        <tr>
            <th class="caution1">　</th>
         </tr>
        <!--英名-->
        <tr class=XXX>
          <th class=XXX>英　名<sup>*</sup></th>
          <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_name_eng;?></td>
        </tr>
        <!--英名注釈-->
        <tr>
          <th class=XXX>英名注釈<sup>*</sup></th>
          <td class=XXX><?php echo $tmpstr_JASCO_List_chEng_annotation;?></td>
        </tr>
        <!--輸出価格-->
        <tr>
          <th class=XXX>輸出価格</th>
          <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_e_view;?></td>
        </tr>
        <!--US卸-->
        <tr>
          <th class=XXX>US卸</th>
          <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_us_view;?></td>
        </tr>
        <!--EU卸-->
        <tr>
          <th class=XXX>EU卸</th>
          <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_eu_view;?></td>
        </tr>
    <?php } ?>

    </tbody>
    </table>
  <!--IF国内表示ここまで--><!--海外事業部他ここから-->
  <?php }else if($viewtype == DEP_ABROAD || $viewtype == DEP_JI || $viewtype == DEP_ABROAD_CH || $viewtype == DEP_ABROAD_US || $viewtype == DEP_ABROAD_EU ){ ?>
    <table>
    <!--phpをちりばめて動かしています。-->
    <tbody>
          <tr class=XXX>
            <th class="caution1" colspan="2">*帳票類(納品請求書、出荷ラベルなど)記載項目</th>
          </tr>
        <!--商品番号-->
          <tr>
            <th class=XXX style="width:170px;">商品番号<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chCode;?>
            <div style='display:inline;'> </div>
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chCode;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--品目番号-->
          <tr>
            <th class=XXX>品目番号<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chNumber_string;?>
            <div style='display:inline;'> </div>
            <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chNumber_string;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
            </td>
          </tr>
        <!--英名-->
          <tr>
            <th class=XXX>英　名<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_name_eng;?></td>
          </tr>
        <!--英名注釈-->
          <tr>
            <th class=XXX>英名注釈<sup>*</sup></th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chEng_annotation;?></td>
          </tr>
        <!--対応機種/用途-->
          <tr>
            <th class=XXX>対応機種/用途</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_chCompatible_models;?></td>
          </tr>
        <?php if($viewtype != DEP_ABROAD_US || $viewtype != DEP_ABROAD_EU){?>
          <!--規格-->
            <tr>
              <th class=XXX>規　格<sup>*</sup></th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_chSpecification;?></td>
            </tr>
          <!--仕様・備考-->
            <tr>
              <th class=XXX>仕様 備考</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_chNotes;?></td>
            </tr>
          <!--型式-->
            <tr>
              <th class=XXX>型　式<sup>*</sup></th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_type;?>
              <div style='display:inline;'> </div>  
              <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chProc_type;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
              </td>
            </tr>
          <!--商品名称-->
            <tr>
              <th class=XXX>商品名称<sup>*</sup></th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_chProc_name;?>
              <div style='display:inline;'> </div>
              <div class="btn_click_clipboard" id="<?php echo $tmpstr_JASCO_List_chProc_name;?>" style='display:inline;cursor:pointer;'><img src="image/copy.png" width="10"></div>
              </td>
            </tr>
        <?php } ?>
    </tbody>
    </table>
    <table>
    <!--phpをちりばめて動かしています。-->
    <tbody>
        <tr>
          <th class="caution1">税抜き価格</th>
        </tr>  
        <?php if($viewtype == DEP_ABROAD){?>
          <!--販売価格-->
          <tr>
              <th class=XXX style="width:170px;">販売価格(中国用)</th>
              <td class=sale_price><?php echo $tmpstr_JASCO_List_dPriceP_view;?></td>
            </tr>
          <!--卸2-->
            <tr>
              <th class=XXX>卸2(中国用)</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceD2_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_ABROAD_CH){?>
          <!--販売価格-->
          <tr>
              <th class=XXX style="width:170px;">販売価格</th>
              <td class=sale_price><?php echo $tmpstr_JASCO_List_iPrice_ch_view;?></td>
            </tr>
          <!--卸2-->
            <tr>
              <th class=XXX>卸　2</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceD2_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_ABROAD || $viewtype == DEP_ABROAD_US){?>
          <!--US卸-->
            <tr>
              <th class=XXX>US卸</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_us_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_ABROAD || $viewtype == DEP_ABROAD_EU){?>
          <!--EU卸-->
            <tr>
              <th class=XXX>EU卸</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_eu_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_ABROAD){?>
          <!--輸出価格-->
            <tr>
              <th class=XXX>輸出価格(インタ用)</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_e_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_JI){?>
          <!--輸出価格-->
            <tr>
              <th class=XXX>輸出価格</th>
              <td class=XXX><?php echo $tmpstr_JASCO_List_iPrice_e_view;?></td>
            </tr>
        <?php } ?>
        <?php if($viewtype == DEP_ABROAD){?>
        <!--工場卸<-->
          <tr>
            <th class=XXX>工場卸</th>
            <td class=XXX><?php echo $tmpstr_JASCO_List_dPriceM_view;?></td>
          </tr>
        <?php } ?>
    </tbody>
    </table>
  <?php } ?><!--海外事業部他ここまで-->
    <br>
    <table>
    <tbody>
        <!--機種分類-->
          <tr>
            <th class=XXX style="width:170px;">機種分類</th>
            <td class=XXX><?php
                //キーワードを空白で分割する
              $array = explode(" ",$tmpstr_JASCO_List_chInstrument_type);
              $freeword_size=count($array);
              for($i = 0; $i <$freeword_size;$i++){
                if($i>0)echo "　";
                switch($array[$i]){
                  case "01": 
                  echo "FT/IR"; 
                  break;
                  case "02": echo "NRS"; break;
                  case "03": echo "UV"; break;
                  case "04": echo "FP"; break;
                  case "05": echo "DT"; break;
                  case "06": echo "UVetc"; break;
                  case "07": echo "J/P"; break;
                  case "08": echo "NFS"; break;
                  case "09": echo "LC"; break;
                  case "10": echo "SCF"; break;
                  case "11": echo "COM"; break;
                  case "12": echo "BKK"; break;
                  case "13": echo "LAMP"; break;
                  case "14": echo "other"; break;
                }
              } 
            /*echo $tmpstr_JASCO_List_chInstrument_type;*/
            ?>
            </td>
          </tr>
          
        <?php if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD || $viewtype == DEP_DOMESTIC || $viewtype == DEP_JE || $viewtype == DEP_JI){?>
        <!--パーツセンタ対象-->
          <tr>
            <th class=XXX>パーツセンタ対象</th>
            <td class=XXX><?php if($tmpstr_JASCO_List_chParts_centere =="TRUE")echo"○";?></td>
          </tr>
        <?php } ?>
        <?php if($viewtype != DEP_ABROAD_US || $viewtype != DEP_ABROAD_EU){?>
        <!--登録区分-->
          <tr>
            <th class=XXX>登録区分</th>
            <td class=XXX>
              <?php 
              //キーワードを空白で分割する
              $array = explode(" ",$tmpstr_JASCO_List_chClass1);
              $freeword_size=count($array);
              for($i = 0; $i <$freeword_size;$i++){
                if($i>0)echo "　";
                echo $array[$i];
              }
              /*echo $tmpstr_JASCO_List_chClass1;*/
              ?>
            </td>
          </tr>
        <?php } ?>
        <?php if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD || $viewtype == DEP_DOMESTIC || $viewtype == DEP_JE || $viewtype == DEP_JI){?>
        <!--価格表区分-->
          <tr>
            <th class=XXX>価格表区分</th>
            <td class=XXX>
              <?php 
              $tmp = "";
              if($tmpstr_JASCO_List_chDomestic_classification !=""){echo "国内向け商品";  $tmp =$tmpstr_JASCO_List_chDomestic_classification;}
              if($tmpstr_JASCO_List_chService_classification !=""){
                if($tmp !="")echo "　";
                echo "サービスパーツ";  $tmp =$tmpstr_JASCO_List_chDomestic_classification;
              }
              if($tmpstr_JASCO_List_chOverseas_classification !=""){
                if($tmp || $tmpstr_JASCO_List_chService_classification)echo "　";
                echo "海外向け商品";
              }
              ?>
              </td>
          </tr>
        <?php } ?>
    </tbody>
    </table>
   <?php 
    $tempstr_filename_image = "image/nophoto.jpg";
    if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg")==true){
      $tempstr_filename_image = filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg";
    }
    else if(file_exists(filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg")==true){
      $tempstr_filename_image = filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg";
    }    
    ?>
    <?php if($tempstr_filename_image == "image/nophoto.jpg"){?>
      <img class = "jasco_productImage" src=<?php echo $tempstr_filename_image;?>>

    <?php }else{?>
      <!--
      <div class="fullscreen-image" data-image="<?php echo $tempstr_filename_image;?>">
        <img src="<?php echo $tempstr_filename_image;?>" width="300px" alt="<?php echo $tempstr_filename_image;?>">
      </div>
      -->
      <br>
      <div>
        <a class="example-image-link" href="<?php echo $tempstr_filename_image;?>" data-lightbox="example-2" data-title="<?php echo $tmpstr_JASCO_List_chCode."<br>".$tmpstr_JASCO_List_chProc_name;?>">
        <img class="example-image" src="<?php echo $tempstr_filename_image;?>" width="300px" alt="<?php echo $tempstr_filename_image;?>">
      </a><br>
        <?php
        for($i = 1; $i <50;$i++){//同名のファイル数をカウントするよりもとりあえず50ファイル名まで見る
          $tmpstr = $tmpstr_JASCO_List_chNumber_string."_".$i.".jpg";
          if(file_exists(filePath::$picture_path.$tmpstr)==true){?>
            <a class="example-image-link" href="<?php echo filePath::$picture_path.$tmpstr;?>" data-lightbox="example-2" data-title="<?php echo $tmpstr_JASCO_List_chCode."<br>".$tmpstr_JASCO_List_chProc_name;?>">
            <img style="margin-top:10px;" class="example-image" src="<?php echo filePath::$picture_path.$tmpstr;?>" width="300px" alt="<?php echo filePath::$picture_path.$tmpstr;?>">
            </a><br>
          <?php
          }
        }
        ?>
      </div>
    <?php }?>

<?php
try{
	// SQLクエリを直接埋め込む（注意：ここではインジェクション対策は省いていますが、本来は準備済みステートメントを使うべきです）
	$dbh = new PDO($dsn,$user,$password);//成功！
	$sql = "SELECT is_active FROM request_photo_count WHERE code = '$tmpstr_JASCO_List_chCode'";
	// クエリを実行して結果を取得
	$statement2 = $dbh->prepare($sql);
	$statement2->execute();
	// 結果をフェッチ
	$result = $statement2->fetch(PDO::FETCH_ASSOC);
	// データがあるか確認
	if ($result) {
		$isActive = $result['is_active'];
	}
	else{
		$isActive = "1";	
	}	
} catch (PDOException $e) {}
?>

<?php 
          if(!$isActive){
            print "<p style='margin-top:0px;'>写真リクエスト対象外</p>";
          }
    ?>
<br>

  <ul class="gnav"><!--class="gnav-->
  <!--カートに追加(中止商品でないとき) -->
  <?php if($tmpstr_JASCO_List_chEnd_date == 0 || $tmpstr_JASCO_List_chEnd_date == "" || strtotime($today) <= strtotime($tmpstr_JASCO_List_chEnd_date) || $tmpstr_JASCO_List_x_chWhile_stocks_laste == "TRUE"){?>
    <li>
    <a href = "client_cart_list_n.php?direct=2"class="btn-flat-simple" style="font-size:small;font-weight:normal;" onClick="set()">カートに追加</a>
    </li>
  <?php }?>
  <!--印刷--> 

    <li>
      <div class="btn-flat-simple"style="font-size:small;font-weight:normal;">印刷&nbsp;<i class="fas fa-angle-down"></i></div>
      <ul>
          <li><a href = "print_detail_list_to_pdf_n.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_p"  style="font-size:small;font-weight:normal;" target="_blank">定価</a></li>
          <li><a href = "print_detail_list_to_pdf_d1.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_d1"  style="font-size:small;font-weight:normal;" target="_blank">卸1</a></li>
          <li><a href = "print_detail_list_to_pdf_d2.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_d2"  style="font-size:small;font-weight:normal;" target="_blank">卸2</a></li>
      </ul>
    </li>
    <li>
      <div class="btn-flat-simple"style="font-size:small;font-weight:normal;">コピー&nbsp;<i class="fas fa-angle-down"></i></div>
      <ul>
        <li><div class="btn-flat-simple_p" onClick="copyToClipboard()" style="font-size:small;font-weight:normal;">定価</div></li>
        <li><div class="btn-flat-simple_d1" onClick="copyToClipboard_d1()" style="font-size:small;font-weight:normal;">卸1</div></li>
        <li><div class="btn-flat-simple_d2" onClick="copyToClipboard_d2()" style="font-size:small;font-weight:normal;">卸2</div></li>
      </ul>
    </li>

  <!--
  <a href = "print_detail_list_to_pdf_n.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_p"  style="font-size:small;font-weight:normal;" target="_blank"><i class="fa fa-caret-right"></i> 印刷</a>
  <a href = "print_detail_list_to_pdf_d1.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_d1"  style="font-size:small;font-weight:normal;" target="_blank"><i class="fa fa-caret-right"></i> 印刷(卸1)</a>
  <a href = "print_detail_list_to_pdf_d2.php?List_ResultID=<?php echo $row["code"];?>"class="btn-flat-simple_d2"  style="font-size:small;font-weight:normal;" target="_blank"><i class="fa fa-caret-right"></i> 印刷(卸2)</a>
  -->
  <!--クリップボードへコピー--> 
  <!--
  <div class="btn-flat-simple_p" onClick="copyToClipboard()" style="font-size:small;font-weight:normal;">コピー</div>
  <div class="btn-flat-simple_d1" onClick="copyToClipboard_d1()" style="font-size:small;font-weight:normal;">コピー(卸1)</div>
  <div class="btn-flat-simple_d2" onClick="copyToClipboard_d2()" style="font-size:small;font-weight:normal;">コピー(卸2)</div>
  -->
  <!--印刷 
  <a href="javascript:history.back()" class="btn-flat-simple">結果一覧に戻る</a>-->
  
  <?php 
    if($tempstr_filename_image == "image/nophoto.jpg"){
      //<!--写真リクエスト(中止商品はリクエストしない)-->
      if($tmpstr_JASCO_List_chEnd_date == 0 || $tmpstr_JASCO_List_chEnd_date == "" || strtotime($today) <= strtotime($tmpstr_JASCO_List_chEnd_date)){
        if($isActive){ ?>
        <!--<div class="btn-flat-simple" onclick="request_photo()">写真リクエスト</div> 関数で飛ばすとページ再描画時に必ずカウントアップという症状が出た-->
        <!--
        <a href = "add_req_phto.php?List_CodeID=<?php echo $row["code"];?>"class="btn-flat-simple" target="_blank" style="font-size:small;font-weight:normal;"><i class="fa fa-caret-right"></i> 写真リクエスト</a>
        -->
        <!-- 写真リクエストが削除されたときは表示しない -->
	        <li>       
	        <div class="btn-flat-simple" onClick="request_camera()" style="font-size:small;font-weight:normal;">写真リクエスト</div>
	        <!--<div class="good">
	        <?php $getval = isset($_COOKIE["jascoproc_photo_req_$tmpstr_JASCO_List_chCode"]) ? $_COOKIE["jascoproc_photo_req_$tmpstr_JASCO_List_chCode"]:"0";?>
	        <?php if($getval > 0){?> 
	          <button type="submit" title="写真リクエスト済"><img src="image/camera_requested.jpg" width="40" alt="Photo" class="cart_button"></button>
	        <?php }else{?>
	          <button type="submit" title="写真をリクエスト"><img src="image/camera_plus.jpg" width="40" alt="Photo" class="cart_button"onClick="request_camera()"></button>
	        <?php }?>
	        </div>-->
	        </li>
      <?php } ?>
      <?php }?>
  <?php }?>
  
  </ul><!--class="gnav-->

  <div id="show_result"></div>
  <!--<input type="button" value="ストレージに保存" onClick="set()">-->
  <!--<input type="button" value="ストレージをクリア" onClick="cle()">-->
  <!--<a href="javascript:history.back()">結果一覧に戻る</a>-->
  <!--<a href="#" onClick="window.close(); return false;">閉じる</a>-->
  <!--<div id="headerfixFloatingMenu">
    <img  class="img_jascologo" src="image/jasco_logo2.png">
  </div>-->
   <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php" class="class_white_text">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
		    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div>
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
		    </div>
	   </div>
    </div>
  <!--アンダーバーメニュー表示と重複した部分をスクロールして表示するため-->
  <br><br><br>
  <br><br>
    <?php require("./component/return.php"); ?>
  <br>
  <br><br><br>
<!--</body>の直前-->  
<!--script type="text/javascript">
  var elements = document.querySelectorAll( '.fullscreen-image' );
  Intense( elements );
</script-->
<script src="../lightbox2-2.11.2/dist/js/lightbox-plus-jquery.min.js"></script>
<script src="./js/copy_to_clipboard.js"></script>
<script>
    lightbox.option({
      'resizeDuration': 10,
      'maxWidth': 700,
      'maxHeight': 500, 
      'positionFromTop': 15
    })
</script>
</body>
</html>

