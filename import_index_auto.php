﻿<?php
  //データベースへ接続設定
  require("./config/dbConnect.php");
  header("Content-type: text/html; charset=utf-8");
  //ファイルパスの設定
  require("./config/filePath.php");
  $tempfile = $_FILES['fname']['tmp_name'];
  $filename = filePath::$importtext_path.$_FILES['fname']['name'];
  //$filename = '../../htdocs/pricelist/'.$_FILES['fname']['name'];
  $table_name1 =$_GET['index_type'];
  if (is_uploaded_file($tempfile)) {
      if ( move_uploaded_file($tempfile , $filename )) {
      	chmod($filename, 0755);
    echo $filename. "をアップロードしました。<br>";
    } else {
        echo "ファイルをアップロードできません。<br>";
    }
  } else {
    echo "ファイルが選択されていません。<br>";
  } 
 $filename1 = $filename;

  $text1 = '';
  $dummy_code = '';
  //データベースへ接続
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "h_list_procの削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'h_list_proc'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(前半)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE h_list_proc");
      if($statement){
        if($statement->execute())
          echo "h_list_procの削除成功です。<br>";
        else
          echo "h_list_procの削失敗しました。<br>";
      }else{
        echo "h_list_procの削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    echo "データベース(前半)の削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'h_list'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(前半)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE h_list");
      if($statement){
        if($statement->execute())
          echo "データベース(前半)の削除成功です。<br>";
        else
          echo "データベース(前半)の削失敗しました。<br>";
      }else{
        echo "データベース(前半)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    echo "データベース(一時処理)の削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'h_list_tmp'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
        $testval =$statement->rowCount();
        echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(結果)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE h_list_tmp");
      if($statement){
        if($statement->execute())
          echo "データベース(一時処理)の削除成功です。<br>";
        else
          echo "データベース(一時処理)の削失敗しました。<br>";
      }else{
        echo "データベース(一時処理)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    echo "データベース(結果)の削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'h_list_result'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
        $testval =$statement->rowCount();
        echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(結果)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE h_list_result");
      if($statement){
        if($statement->execute())
          echo "データベース(結果)の削除成功です。<br>";
        else
          echo "データベース(結果)の削失敗しました。<br>";
      }else{
        echo "データベース(結果)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //h_list_procの作成（枠だけ）
    echo "データベース(そのまま)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.h_list_proc(
    h_list_instrument_type_name varchar(256),
    column_2 varchar(32),
    column_3 varchar(32),
    column_4 varchar(1024),
    h_list_page_index varchar(32),
    h_list_row varchar(32),
    column_7 varchar(32),
    column_8 varchar(32),
    column_9 varchar(32),
    column_10 varchar(32),
    h_list_title_or_code varchar(64),
    h_list_number_string varchar(128),
    h_list_sp_proc_type varchar(32),
    h_list_sp_proc_name varchar(256),
    h_list_sp_price_p varchar(32),
    h_list_sp_price_d1 varchar(32),
    h_list_sp_price_d2 varchar(32),
    h_list_sp_price_d3 varchar(32),
    h_list_sp_price_m varchar(32),
    h_list_sp_specification text,
    h_list_sp_notes text,
    column_22 varchar(128),
    column_23 varchar(32),
    column_24 varchar(32),
    column_25 varchar(32),
    column_26 varchar(32),
    column_27 varchar(32),
    column_28 varchar(32),
    column_29 varchar(32),
    column_30 varchar(32),
    column_31 varchar(32),
    column_32 varchar(32),
    column_33 varchar(32),
    column_34 varchar(32),
    column_35 varchar(32)
    )");
    if($statement){
      if($statement->execute())
        echo "データベースの作成成功です。<br>";
      else
        echo "データベースの作成失敗しました。<br>";
    }else{
      echo "データベースの作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベースの中身詰める<br>";
    echo "ファイル名:".$filename1."です。<br>";
    $filename_import =  $filename1;
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    
 //1-2行目に不要な行が入るようになったため飛ばす  
    $buffer_string = "LOAD DATA INFILE :Load_filename INTO TABLE h_list_proc FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\r\\n' IGNORE 2 LINES";
    echo "buffer_string:".$buffer_string."です。<br>";
    $statement = $dbh->prepare($buffer_string);
    if($statement){
      $statement->bindValue(':Load_filename', $filename_import, PDO::PARAM_STR);
      if($statement->execute()){
        echo "データベース(前半)の中身詰める 成功<br>";
      }else{
        echo "データベース(前半)の中身詰める 失敗<br>";
      }
    }else{
      echo "データベース(前半)の中身詰める<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    //データベースの作成（枠だけ）
    echo "データベースの作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("CREATE TABLE pricelist.h_list(
      h_list_no varchar(32),
      h_list_instrument_type_name varchar(256),
      h_list_page_index varchar(32),
      h_list_row varchar(32),
      h_list_title_or_code varchar(64),
      h_list_number_string varchar(128),
      h_list_sp_proc_type varchar(32),
      h_list_sp_proc_name varchar(256),
      h_list_sp_price_p varchar(32),
      h_list_sp_price_d1 varchar(32),
      h_list_sp_price_d2 varchar(32),
      h_list_sp_price_d3 varchar(32),
      h_list_sp_price_m varchar(32),
      h_list_sp_specification text,
      h_list_sp_notes text
      )");
    if($statement){
      if($statement->execute())
        echo "データベースの作成成功です。<br>";
      else
        echo "データベースの作成失敗しました。<br>";
    }else{
      echo "データベースの作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "h_list_procから必要な中身を詰める<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    
    $statement2 = $dbh -> prepare("INSERT INTO h_list(
      h_list_no,h_list_instrument_type_name,h_list_page_index,h_list_row,
      h_list_title_or_code,h_list_number_string,
      h_list_sp_proc_type,h_list_sp_proc_name,h_list_sp_price_p,h_list_sp_price_d1,h_list_sp_price_d2,
      h_list_sp_price_d3,h_list_sp_price_m,h_list_sp_specification,h_list_sp_notes) 
      VALUES (:h_list_no,:h_list_instrument_type_name,:h_list_page_index,:h_list_row,:h_list_title_or_code, :h_list_number_string,
      :h_list_sp_proc_type,:h_list_sp_proc_name,:h_list_sp_price_p,:h_list_sp_price_d1,
      :h_list_sp_price_d2,:h_list_sp_price_d3,:h_list_sp_price_m,:h_list_sp_specification,:h_list_sp_notes)");
 
    $statement = $dbh->prepare("SELECT h_list_instrument_type_name, h_list_page_index,h_list_row,
    h_list_title_or_code,h_list_number_string,
    h_list_sp_proc_type,h_list_sp_proc_name,h_list_sp_price_p,h_list_sp_price_d1,h_list_sp_price_d2,
    h_list_sp_price_d3,h_list_sp_price_m,h_list_sp_specification,h_list_sp_notes  FROM h_list_proc");
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          //新規テーブルへ挿入
          if($statement2){
            //$statement2->bindValue(':h_list_no', $row_count-$i, PDO::PARAM_STR);//逆の順番に詰まってしまう
            $statement2->bindValue(':h_list_no', $i, PDO::PARAM_STR);
            $statement2->bindValue(':h_list_instrument_type_name', $record["h_list_instrument_type_name"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_page_index', $record["h_list_page_index"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_row', $record["h_list_row"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_title_or_code', $record["h_list_title_or_code"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_number_string', $record["h_list_number_string"], PDO::PARAM_STR);
            //通常は使用しないが、"特注/特注""未登録/未登録"時のみ使用
            if(("特注"==$record["h_list_title_or_code"]&&"特注"==$record["h_list_number_string"]) || 
            ("未登録"==$record["h_list_title_or_code"]&&"未登録"==$record["h_list_number_string"]) ){
              $statement2->bindValue(':h_list_sp_proc_type', $record["h_list_sp_proc_type"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_proc_name', $record["h_list_sp_proc_name"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_price_p', $record["h_list_sp_price_p"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_price_d1', $record["h_list_sp_price_d1"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_price_d2', $record["h_list_sp_price_d2"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_price_d3', $record["h_list_sp_price_d3"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_price_m', $record["h_list_sp_price_m"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_specification', $record["h_list_sp_specification"], PDO::PARAM_STR);
              $statement2->bindValue(':h_list_sp_notes', $record["h_list_sp_notes"], PDO::PARAM_STR);
            }else{//IFSから引いてくるので敢えてブランク
              $statement2->bindValue(':h_list_sp_proc_type', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_proc_name', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_price_p',null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_price_d1', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_price_d2', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_price_d3', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_price_m', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_specification', null, PDO::PARAM_NULL);
              $statement2->bindValue(':h_list_sp_notes', null, PDO::PARAM_NULL);
            }

            //echo "code:".$record["code"]."です。<br>";
            if($statement2->execute()){
              //echo "後半への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件データベース（中身を詰める）成功です。<br>";
      }else{
        echo "後データベース（中身を詰める）失敗です。<br>";
      }
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "失敗しました。";
      echo "失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";

    /**********************************************/
    //縦軸不要になり、以降"h_list"のみ対応    
    /**********************************************/
    //並べ替え用テーブルの作成の作成（枠だけ）
    echo "テーブル(価格表)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.h_list_tmp(
    h_list_no varchar(32),
    h_list_instrument_type_name varchar(256),
    h_list_page_index varchar(32),
    h_list_row varchar(32),
    h_list_title_or_code varchar(64),
    h_list_number_string varchar(128),
    proc_type varchar(64),
    proc_name varchar(64),
    price_p varchar(32),
    specification varchar(128),
    notes text,
    price_d1 varchar(32),
    price_d2 varchar(32),
    x_price_d3 varchar(32),
    price_m varchar(32),
    end_date varchar(64),
    x_while_stocks_last varchar(16)
    )");
    if($statement){
      $statement->execute();
      //if($statement->execute())
      //  echo "データベース(全体)の作成成功です。<br>";
      //else
      //  echo "データベース(全体)の作成失敗しました。<br>";
    }else{
      echo "並べ替え用テーブルの作成の作成失敗しました。<br>";
    }
    /**********************************************/
    //データベース(全体)テーブルの作成
    echo "データベース(全体)の作成の作成前<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.h_list_result LIKE h_list_tmp");
    if($statement){
      $statement->execute();
    }else{
      echo "データベース(全体)の作成失敗しました。<br>";
    }
    /**********************************************/
    //新規テーブル準備

    /*//インデックスの作成
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }*/
    //インデックスの作成
    $statement = $dbh->prepare("ALTER TABLE h_list ADD INDEX `code_idx` (h_list_title_or_code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    } 
    /*//インデックスの作成（特注/品目番号）時に必要
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer ADD INDEX `number_string_idx` (number_string)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }*/
    $statement2 = $dbh -> prepare("INSERT INTO h_list_tmp(
    h_list_no,h_list_instrument_type_name,h_list_page_index,h_list_row,
    h_list_title_or_code,h_list_number_string,
    proc_type, proc_name, price_p,specification, notes, price_d1, price_d2, x_price_d3, price_m,end_date,x_while_stocks_last) 
    VALUES (:h_list_no,:h_list_instrument_type_name,:h_list_page_index,:h_list_row,:h_list_title_or_code, :h_list_number_string, 
    :proc_type, :proc_name, :price_p, :specification, :notes, :price_d1, :price_d2, :x_price_d3, :price_m,:end_date,:x_while_stocks_last)");

    $statement = $dbh->prepare("SELECT h_list.h_list_no,h_list.h_list_instrument_type_name, h_list.h_list_page_index,
    h_list.h_list_row,h_list.h_list_title_or_code,h_list.h_list_number_string, 

    h_list.h_list_sp_proc_type,h_list.h_list_sp_proc_name,h_list.h_list_sp_price_p,h_list.h_list_sp_price_d1,h_list.h_list_sp_price_d2, 
    h_list. h_list_sp_price_d3,h_list.h_list_sp_price_m, 
    h_list.h_list_sp_specification,h_list.h_list_sp_notes,

    new_list_set_buffer.proc_type, new_list_set_buffer.proc_name, new_list_set_buffer.price_p, 
    new_list_set_buffer.specification,new_list_set_buffer.notes,
    new_list_set_buffer.price_d1, new_list_set_buffer.price_d2, new_list_set_buffer.x_price_d3,
    new_list_set_buffer.price_m ,new_list_set_buffer.end_date,new_list_set_buffer.x_while_stocks_last
    FROM h_list LEFT OUTER JOIN new_list_set_buffer 
    ON h_list.h_list_title_or_code = new_list_set_buffer.code ");
    //ORDER BY h_list.h_list_no ASC");
    //ON h_list.h_list_number_string = new_list_set_buffer.number_string "); 
    
    $exist_enddateproduct = 0;
    $today = date("Y/m/d");
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        echo  $row_count."件です。<br>";
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          //新規テーブルへ挿入
          if($statement2){
            $statement2->bindValue(':h_list_no', $record["h_list_no"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_instrument_type_name', $record["h_list_instrument_type_name"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_page_index', $record["h_list_page_index"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_row', $record["h_list_row"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_title_or_code', $record["h_list_title_or_code"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_number_string', $record["h_list_number_string"], PDO::PARAM_STR);
            //価格表から読む.品目番号からIFSと紐づける、商品番号からIFSと紐づける(商品番号からがメイン)
            if(("特注"==$record["h_list_title_or_code"]&&"特注"==$record["h_list_number_string"]) || 
            ("未登録"==$record["h_list_title_or_code"]&&"未登録"==$record["h_list_number_string"]) ){
              /*価格表から読む*/
              $statement2->bindValue(':proc_type', $record["h_list_sp_proc_type"], PDO::PARAM_STR);
              $statement2->bindValue(':proc_name', $record["h_list_sp_proc_name"], PDO::PARAM_STR);
              $statement2->bindValue(':price_p', $record["h_list_sp_price_p"], PDO::PARAM_STR);
              $statement2->bindValue(':specification', $record["h_list_sp_specification"], PDO::PARAM_STR);
              $statement2->bindValue(':notes', $record["h_list_sp_notes"], PDO::PARAM_STR);
              $statement2->bindValue(':price_d1', $record["h_list_sp_price_d1"], PDO::PARAM_STR);
              $statement2->bindValue(':price_d2', $record["h_list_sp_price_d2"], PDO::PARAM_STR);
              $statement2->bindValue(':x_price_d3', $record["h_list_sp_price_d3"], PDO::PARAM_STR);
              $statement2->bindValue(':price_m', $record["h_list_sp_price_m"], PDO::PARAM_STR);
              $statement2->bindValue(':end_date', "", PDO::PARAM_STR);
              $statement2->bindValue(':x_while_stocks_last', "", PDO::PARAM_STR);              
            }elseif("特注"==$record["h_list_title_or_code"]&&"特注"!=$record["h_list_number_string"]){
              //echo $record["h_list_number_string"]."<br>";
              /*品目番号からIFSと改めて紐づける */
              $statement3 = $dbh->prepare("SELECT proc_type, proc_name, price_p,specification,notes,
              price_d1, price_d2, x_price_d3,price_m,end_date,x_while_stocks_last FROM new_list_set_buffer WHERE number_string LIKE (:ProcName)");
              if($statement3){
                $statement3->bindValue(':ProcName', $record["h_list_number_string"], PDO::PARAM_STR);
                //echo "検索ワード:".$record["h_list_number_string"]."です。<br>";
                if($statement3->execute()){
                  //echo "検索結果:".$statement3->rowCount()."件です。<br>";
                  while($record_temp = $statement3->fetch(PDO::FETCH_ASSOC)){
                    $rows_temp[] = $record_temp;
                  }
                  //echo $record["proc_name"]."<br>";
                  foreach($rows_temp as $row_temp){

                    //中止商品を登録しようとすると警告メッセージを残す
                    //if($record["end_date"]!="")
                    //echo "code:".$record["h_list_title_or_code"]."today1:".$today."end_date:".$row_temp["end_date"]."在庫限り:".$row_temp["x_while_stocks_last"]."。<br>";//debug
                    if($record["end_date"]!="" && $row_temp["end_date"]!=0 && $row_temp["end_date"]!="" && $row_temp["x_while_stocks_last"] !="TRUE"){
                      if(strtotime($today)>strtotime($row_temp["end_date"]))
                        echo "code:".$record["h_list_title_or_code"]." は中止商品です。<br>";
                        $exist_enddateproduct = 1;
                    }

                    $statement2->bindValue(':proc_type', $row_temp["proc_type"], PDO::PARAM_STR);
                    $statement2->bindValue(':proc_name', $row_temp["proc_name"], PDO::PARAM_STR);
                    $statement2->bindValue(':price_p', $row_temp["price_p"], PDO::PARAM_STR);
                    $statement2->bindValue(':specification', $row_temp["specification"], PDO::PARAM_STR);
                    $statement2->bindValue(':notes', $row_temp["notes"], PDO::PARAM_STR);
                    $statement2->bindValue(':price_d1', $row_temp["price_d1"], PDO::PARAM_STR);
                    $statement2->bindValue(':price_d2', $row_temp["price_d2"], PDO::PARAM_STR);
                    $statement2->bindValue(':x_price_d3', $row_temp["x_price_d3"], PDO::PARAM_STR);
                    $statement2->bindValue(':price_m', $row_temp["price_m"], PDO::PARAM_STR);
                    $statement2->bindValue(':end_date', $row_temp["end_date"], PDO::PARAM_STR);
                    $statement2->bindValue(':x_while_stocks_last', $row_temp["x_while_stocks_last"], PDO::PARAM_STR);
                    }
                }else{
                  $errors['error'] = "検索失敗しました。";
                }
              }
            }else{

              //中止商品を登録しようとすると警告メッセージを残す
              //if($record["end_date"]!="")
              //echo  "code:".$record["h_list_title_or_code"]."today2:".$today."end_date:".$record["end_date"]."在庫限り:".$record["x_while_stocks_last"]."。<br>";//debug
              if($record["end_date"]!=0 && $record["end_date"]!="" && $record["x_while_stocks_last"] !="TRUE"){
                if(strtotime($today)>strtotime($record["end_date"]))
                  echo "code:".$record["h_list_title_or_code"]."は中止商品です。<br>";
                  $exist_enddateproduct = 1;
              }

              /*商品番号からIFSと紐づける(これがメイン) */
              $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
              $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
              $statement2->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
              $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
              $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
              $statement2->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
              $statement2->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
              $statement2->bindValue(':x_price_d3', $record["price_d2"], PDO::PARAM_STR);
              $statement2->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
              $statement2->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR);
              $statement2->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            }
            //echo "code:".$record["code"]."です。<br>";
            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        //echo $i."件連結成功です。<br>";
      }else{
        echo "連結失敗です。<br>";
      }
      //echo "結果:".$row_count."件です。<br>";
    }else{
      echo "新規テーブル(全体)の作成失敗しました。<br>";
    }
    /**********************************************/
    //h_list_noで並べ替えたh_list_tmp を h_list_result にコピー
    echo "データベースの並べ替え<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    //新規テーブル準備
    $statement2 = $dbh -> prepare("INSERT INTO h_list_result(
      h_list_no,h_list_instrument_type_name,h_list_page_index,h_list_row,
      h_list_title_or_code,h_list_number_string,
      proc_type, proc_name, price_p,specification, notes, price_d1, price_d2, x_price_d3, price_m) 
      VALUES (:h_list_no,:h_list_instrument_type_name,:h_list_page_index,:h_list_row,:h_list_title_or_code, :h_list_number_string, 
      :proc_type, :proc_name, :price_p, :specification, :notes, :price_d1, :price_d2, :x_price_d3, :price_m)");
  

    $statement = $dbh->prepare("SELECT * FROM h_list_tmp GROUP BY (h_list_no + 0 ) ASC ");
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        echo  $row_count."件です。<br>";
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          //新規テーブルへ挿入
          if($statement2){
            $statement2->bindValue(':h_list_no', $record["h_list_no"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_instrument_type_name', $record["h_list_instrument_type_name"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_page_index', $record["h_list_page_index"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_row', $record["h_list_row"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_title_or_code', $record["h_list_title_or_code"], PDO::PARAM_STR);
            $statement2->bindValue(':h_list_number_string', $record["h_list_number_string"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement2->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
            $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_price_d3', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
            
            //echo "code:".$record["code"]."です。<br>";
            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
      }else{
        echo "データベースの並べ替え失敗<br>";
      }
    }else{
      echo "データベースの並べ替え<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    if($exist_enddateproduct>0){
      print('中止商品が登録されています。確認してください。<br>');
    }else{
      print('データベース登録完了！<br>');
    }
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

  //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
  //echo "<script type='text/javascript'>history.back();</script>";//力技でウィンドウを閉じる

?>
