
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>価格表</title>
  <link rel="stylesheet" href="css/list_tablestyle.css?230613">
	<link rel="stylesheet" type="text/css" href="css/style_flex.css?230613">
	<link rel="stylesheet" type="text/css" href="css/search_items.css?230613">
  <link rel="stylesheet" href="css/under_bar_menu.css?230613">
  <link rel="stylesheet" href="css/list_menu.css?231005">
  <!-- jQuery.jsの読み込み -->
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

  <!-- スムーズスクロール部分の記述 -->
  <script>
  $(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('a[href^=#]').click(function() {
        // スクロールの速度
        var speed = 200; // ミリ秒
        // アンカーの値取得
        var href= $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
  });
  </script>
</head>
</html>