<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>検索結果</title>
  <link rel="stylesheet" href="css/list_tablestyle.css?230617">
  <link rel="stylesheet" href="css/under_bar_menu.css?230302">
  <link rel="stylesheet" href="css/annotation.css?230302">
  <link rel="stylesheet" href="css/fix_sheet.css?230302">
	<link rel="stylesheet" type="text/css" href="css/style_flex.css?230322">
	<!--link rel="stylesheet" type="text/css" href="css/disable_focus_outline.css"-->
  <!--<script type="text/javascript" src="js/intense.js"></script>-->
  <link rel="stylesheet" href="../lightbox2-2.11.2/dist/css/lightbox.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css">
  <!--初期非表示列の設定-->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="js/row_show_hide.js?230729"></script>
  <link rel="stylesheet" href="css/on_off_switch.css?230302" />
</head>
<style type="text/css">
  .fullscreen-image {
     cursor: url("img/plus_cursor.png"), pointer; /* マウスポインタを指定 */
     display: inline-block;   /* 横方向に並べる指定 */
     margin: 0px 5px 5px 0px; /* 周囲の余白量(右と下に5pxずつ) */
  }
</style> 
<body>
  <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php" class="class_white_text">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
		    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div>
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
          <a href="javascript:void(0);" onclick="confirmLogout()">
	    	    <img src="image/logout.png" width="40" height="40" alt="logout" title="ログアウト" style="margin-top:5px;margin-right:5px;float:right;">
          </a>
		    </div>
	   </div>
  </div>
  <script>
      function confirmLogout() {
          if (confirm('ログアウトします。よろしいですか？')) {
              window.location.href = 'login.php';
          }
      }
  </script>
  <!--アンダーバーメニュー表示と重複した部分をスクロールして表示するため-->
  <script src="../lightbox2-2.11.2/dist/js/lightbox-plus-jquery.min.js"></script>
