﻿<?php
  header("Content-type: text/html; charset=utf-8");

  //データベースへ接続設定
  require("./config/dbConnect.php");
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。".$filename1."<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(バックアップ)の削除前<br>";
    //既存データベース(前半)の削除
    $statement = $dbh->prepare("SHOW TABLES LIKE 'h_list_result_bkup'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE h_list_result_bkup");
      if($statement){
        if($statement->execute())
          echo "データベース(バックアップ)の削除成功です。<br>";
        else
          echo "データベース(バックアップ)の削失敗しました。<br>";
      }else{
        echo "データベース(バックアップ)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //データベース(バックアップ)のコピー
    echo "データベース(バックアップ)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE h_list_result_bkup LIKE h_list_result");
    if($statement){
      if($statement->execute())
        echo "データベース(バックアップ)の作成成功です。<br>";
      else
        echo "データベース(バックアップ)の作成失敗しました。<br>";
    }else{
      echo "データベース(バックアップ)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(バックアップ)の中身詰める<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("INSERT INTO h_list_result_bkup SELECT * FROM h_list_result");
    if($statement){
      if($statement->execute())
        echo "データベース(検索用)のコピー成功です。<br>";
      else
        echo "データベース(検索用)のコピー失敗しました。<br>";
    }else{
      echo "データベース(検索用)のコピー失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************
     //インデックスの作成→インデックスはコピーされていた
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_bkup ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    //インデックスの作成（特注/品目番号）時に必要
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_bkup ADD INDEX `number_string_idx` (number_string)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    /**********************************************/
    print('データベース登録完了！<br>');
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

?>
