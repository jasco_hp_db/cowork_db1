﻿<?php
  header("Content-type: text/html; charset=utf-8");
  //echo "検索ワード:".htmlspecialchars($_POST["input_searchWord"],ENT_QUOTES)."です。<br>";
  $text1 = '';
  $dummy_code = '';
  //データベースへ接続設定
  require("./config/dbConnect.php");
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！

    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(全体)の削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "データベース(全体)の行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベースの削除
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer");
      if($statement){
        if($statement->execute())
          echo "データベース(全体)の削除成功です。<br>";
        else
          echo "データベース(全体)の削除失敗しました。<br>";
      }else{
        echo "データベース(全体)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //データベース(全体)の作成（枠だけ）
    echo "データベース(全体)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE new_list_set_buffer(
    col_id varchar(32),
    reg_id varchar(32),
    code varchar(32),
    number_string varchar(32),
    proc_type varchar(64),
    proc_name varchar(64),
    price_p int(11),
    price_d1 int(11),
    price_d2 int(11),
    x_price_d3 int(11),
    price_m int(11),
    x_group varchar(32),
    specification varchar(128),
    notes text,
    x_site varchar(16),
    class1 varchar(16),
    domestic_classification varchar(64),
    service_classification varchar(64),
    overseas_classification varchar(64),
    end_date varchar(64),
    x_while_stocks_last varchar(16),
    parts_center varchar(16),
    x_valid varchar(16),
    substitutional_goods varchar(32),
    x_stock_pc varchar(32),
    x_discontinuance varchar(512),
    x_insttype_dont_use varchar(512),
    proc_name_eng varchar(512),
    eng_annotation text,
    price_e varchar(32),
    price_d_us varchar(32),
    price_d_eu varchar(32),    
    precaution1 varchar(128),
    x_precaution2 varchar(128),
    x_department_in_charge varchar(64),
    x_inventory_section varchar(64),
    x_start_date varchar(64),
    instrument_type varchar(128),
    compatible_models text,
    category varchar(256)/*,
    emplacement_cost int(16),
    memo varchar(32)*/
    )");
    if($statement){
      if($statement->execute())
        echo "データベース(全体)の作成成功です。<br>";
      else
        echo "データベース(全体)の作成失敗しました。<br>";
    }else{
      echo "データベース(全体)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(計算過程_ソート前)の削除前<br>";
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer_pre'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(計算過程_ソート)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_pre");
      if($statement){
        if($statement->execute())
          echo "データベース(計算過程_ソート前)の削除成功です。<br>";
        else
          echo "データベース(計算過程_ソート前)の削失敗しました。<br>";
      }else{
        echo "データベース(計算過程_ソート前)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /*********************************************
    //データベース(計算過程_ソート前)の作成（枠だけ）処理用
    echo "データベース(計算過程_ソート前)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_pre LIKE new_list_set_buffer");
    if($statement){
      if($statement->execute())
        echo "データベース(計算過程_ソート前)の作成成功です。<br>";
      else
        echo "データベース(計算過程_ソート前)の作成失敗しました。<br>";
    }else{
      echo "データベース(計算過程_ソート前)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /*********************************************/
    //データベース(計算過程_ソート前)の作成（枠だけ）処理用
    echo "データベース(計算過程_ソート前)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_pre(
    col_id varchar(32),
    reg_id varchar(32),
    code varchar(32),
    number_string varchar(32),
    proc_type varchar(64),
    proc_name varchar(64),
    price_p varchar(32),
    price_d1 varchar(32),
    price_d2 varchar(32),
    x_price_d3 varchar(32),
    price_m varchar(32),
    x_group varchar(32),
    specification varchar(128),
    notes text,
    x_site varchar(16),
    class1 varchar(16),
    domestic_classification varchar(64),
    service_classification varchar(64),
    overseas_classification varchar(64),
    end_date varchar(64),
    x_while_stocks_last varchar(16),
    parts_center varchar(16),
    x_valid varchar(16),
    substitutional_goods varchar(32),
    x_stock_pc varchar(32),
    x_discontinuance varchar(512),
    x_insttype_dont_use varchar(512),
    proc_name_eng varchar(512),
    eng_annotation text,
    price_e varchar(32),
    price_d_us varchar(32),
    price_d_eu varchar(32),    
    precaution1 varchar(128),
    x_precaution2 varchar(128),
    x_department_in_charge varchar(64),
    x_inventory_section varchar(64),
    x_start_date varchar(64),
    instrument_type varchar(128),
    compatible_models text,
    category varchar(256)/*,
    emplacement_cost int(16),
    memo varchar(32)*/
    )");
    if($statement){
      if($statement->execute())
        echo "データベース(計算過程_ソート前)の作成成功です。<br>";
      else
        echo "データベース(計算過程_ソート前)の作成失敗しました。<br>";
    }else{
      echo "データベース(計算過程_ソート前)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    //処理1 new_list_set_bufferで前半配列と後半配列を連結
    //新規テーブル準備
    $statement2 = $dbh -> prepare("INSERT INTO new_list_set_buffer_pre (col_id, reg_id, code, number_string, 
    proc_type, proc_name, price_p, price_d1, price_d2, x_price_d3, price_m, x_group, specification, notes,
    x_site, class1, domestic_classification, service_classification, overseas_classification, 
    end_date, x_while_stocks_last, parts_center, x_valid, substitutional_goods, x_stock_pc, x_discontinuance,
    x_insttype_dont_use,proc_name_eng,eng_annotation,price_e,price_d_us,price_d_eu,precaution1, x_precaution2,
    x_department_in_charge, x_inventory_section,x_start_date, 
    instrument_type, compatible_models, category) 
    VALUES (:col_id,:reg_id,:code, :number_string, 
    :proc_type, :proc_name, :price_p, :price_d1, :price_d2, :x_price_d3, :price_m, :x_group, :specification, :notes,
    :x_site, :class1, :domestic_classification, :service_classification, :overseas_classification, 
    :end_date, :x_while_stocks_last, :parts_center, :x_valid, :substitutional_goods, :x_stock_pc, :x_discontinuance,
    :x_insttype_dont_use,:proc_name_eng,:eng_annotation,:price_e,:price_d_us,:price_d_eu,:precaution1, :x_precaution2,
    :x_department_in_charge, :x_inventory_section,:x_start_date, 
    :instrument_type, :compatible_models, :category)");
    $start = microtime(true);
    echo "データベースの連結開始:".$start."です。<br>";
    
    //インデックスの作成
    /*$statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_proc ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_lh ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }*/
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_proc "); 
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_proc LEFT OUTER JOIN new_list_set_buffer_lh ON new_list_set_buffer_proc.code = new_list_set_buffer_lh.code "); 
    //new_list_set_buffer_procが前半分しなかいからか、各項目どちらから取るか明示しないと商品コードなどにNULLが入る
    $statement = $dbh->prepare("SELECT new_list_set_buffer_proc.code, new_list_set_buffer_lh.reg_id, new_list_set_buffer_proc.number_string, 
    new_list_set_buffer_proc.proc_type, new_list_set_buffer_proc.proc_name, new_list_set_buffer_proc.price_p, 
    new_list_set_buffer_proc.price_d1, new_list_set_buffer_proc.price_d2, new_list_set_buffer_proc.x_price_d3,
    new_list_set_buffer_proc.price_m, new_list_set_buffer_proc.x_group, new_list_set_buffer_proc.specification, 
    new_list_set_buffer_proc.notes, new_list_set_buffer_proc.x_site, new_list_set_buffer_proc.class1, 
    new_list_set_buffer_proc.domestic_classification, new_list_set_buffer_proc.service_classification, 
    new_list_set_buffer_proc.overseas_classification, 
    new_list_set_buffer_proc.end_date, new_list_set_buffer_proc.x_while_stocks_last, new_list_set_buffer_proc.parts_center,
    new_list_set_buffer_proc.x_valid, new_list_set_buffer_proc.substitutional_goods,  new_list_set_buffer_proc.x_stock_pc,
    new_list_set_buffer_proc.x_discontinuance,new_list_set_buffer_proc.x_insttype_dont_use,
    new_list_set_buffer_proc.proc_name_eng, new_list_set_buffer_proc.eng_annotation,
    new_list_set_buffer_proc.price_e,new_list_set_buffer_proc.price_d_us,new_list_set_buffer_proc.price_d_eu,    
    new_list_set_buffer_proc.precaution1, new_list_set_buffer_proc.x_precaution2, 
    new_list_set_buffer_proc.x_department_in_charge, new_list_set_buffer_proc.x_inventory_section, 
    new_list_set_buffer_proc.x_start_date, 
    new_list_set_buffer_lh.instrument_type, new_list_set_buffer_lh.compatible_models, 
    new_list_set_buffer_lh.category
    FROM new_list_set_buffer_proc 
    LEFT OUTER JOIN new_list_set_buffer_lh 
    ON new_list_set_buffer_proc.code = new_list_set_buffer_lh.code "); 
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $r = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          //新規テーブルへ挿入
          if($statement2){
            $statement2->bindValue(':col_id', $i, PDO::PARAM_STR);
            //$statement2->bindValue(':reg_id', $record["reg_id"], PDO::PARAM_STR);//new_list_set_buffer_lh
            if($record["reg_id"] !="" && $record["reg_id"] !="\\n" && $record["reg_id"] !=NULL){
              $statement2->bindValue(':reg_id', str_pad($record["reg_id"],8,0, STR_PAD_LEFT), PDO::PARAM_STR);//特に桁数に意味はないがゼロパディングしないと並べ替えが上手くいかない
            }else{
              //$statement2->bindValue(':reg_id', $r, PDO::PARAM_STR);//マイナスが入ると並べ替えが上手くいかなかったので昇順に変更
              //$r--;
              $statement2->bindValue(':reg_id', str_pad(($row_count+$i+1),8,0, STR_PAD_LEFT), PDO::PARAM_STR);
            }
            $statement2->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement2->bindValue(':number_string', $record["number_string"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement2->bindValue(':price_p', $record["price_p"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d1', $record["price_d1"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d2', $record["price_d2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_price_d3', $record["x_price_d3"], PDO::PARAM_STR);
            $statement2->bindValue(':price_m', $record["price_m"], PDO::PARAM_STR);
            $statement2->bindValue(':x_group', $record["x_group"], PDO::PARAM_STR);
            $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement2->bindValue(':x_site', $record["x_site"], PDO::PARAM_STR);
            $statement2->bindValue(':class1', $record["class1"], PDO::PARAM_STR);
            $statement2->bindValue(':domestic_classification', $record["domestic_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':service_classification', $record["service_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':overseas_classification', $record["overseas_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR);
            $statement2->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            $statement2->bindValue(':parts_center', $record["parts_center"], PDO::PARAM_STR);
            $statement2->bindValue(':x_valid', $record["x_valid"], PDO::PARAM_STR);
            $statement2->bindValue(':substitutional_goods', $record["substitutional_goods"], PDO::PARAM_STR);
            $statement2->bindValue(':x_stock_pc', $record["x_stock_pc"], PDO::PARAM_STR);
            $statement2->bindValue(':x_discontinuance', $record["x_discontinuance"], PDO::PARAM_STR);
            $statement2->bindValue(':x_insttype_dont_use', $record["x_insttype_dont_use"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name_eng', $record["proc_name_eng"], PDO::PARAM_STR);
            $statement2->bindValue(':eng_annotation', $record["eng_annotation"], PDO::PARAM_STR);
            $statement2->bindValue(':price_e', $record["price_e"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_us', $record["price_d_us"], PDO::PARAM_STR);
            $statement2->bindValue(':price_d_eu', $record["price_d_eu"], PDO::PARAM_STR);
            $statement2->bindValue(':precaution1', $record["precaution1"], PDO::PARAM_STR);
            $statement2->bindValue(':x_precaution2', $record["x_precaution2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_department_in_charge', $record["x_department_in_charge"], PDO::PARAM_STR);
            $statement2->bindValue(':x_inventory_section', $record["x_inventory_section"], PDO::PARAM_STR);
            $statement2->bindValue(':x_start_date', $record["x_start_date"], PDO::PARAM_STR);
            $statement2->bindValue(':instrument_type', $record["instrument_type"], PDO::PARAM_STR);
            $statement2->bindValue(':compatible_models', $record["compatible_models"], PDO::PARAM_STR);
            $statement2->bindValue(':category', $record["category"], PDO::PARAM_STR);
            
            //TEST
            //if($record["code"]=="7183J006AV"){
            //    echo "proc_name_eng;".$record["proc_name_eng"]."<br>";
            //    echo "price_e;".$record["price_e"]."<br>";
            //    echo "price_d_us;".$record["price_d_us"]."<br>";
            //    echo "price_d_eu;".$record["price_d_eu"]."<br>";
            //    echo "instrument_type;".$record["instrument_type"]."<br>";
            //}
            //echo "code:".$record["code"]."です。<br>";
            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件連結成功です。<br>";
      }else{
        echo "連結失敗です。<br>";
      }
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "連結失敗しました。";
      echo "連結失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    

    /**********************************************/
    //new_list_set_buffer_preに入った値をreg_id順にソートしてnew_list_set_bufferに詰める
    //新規テーブル準備
    $statement2 = $dbh -> prepare("INSERT INTO new_list_set_buffer (col_id, reg_id, code, number_string, 
    proc_type, proc_name, price_p, price_d1, price_d2, x_price_d3, price_m, x_group, specification, notes,
    x_site,class1, domestic_classification, service_classification, overseas_classification, 
    end_date, x_while_stocks_last, parts_center, x_valid, substitutional_goods,  x_stock_pc, x_discontinuance,
    x_insttype_dont_use,proc_name_eng,eng_annotation,price_e,price_d_us,price_d_eu,precaution1, x_precaution2,
    x_department_in_charge, x_inventory_section,x_start_date,
    instrument_type, compatible_models, category) 
    VALUES (:col_id,:reg_id,:code, :number_string, 
    :proc_type, :proc_name, :price_p, :price_d1, :price_d2, :x_price_d3, :price_m, :x_group, :specification, :notes,
    :x_site, :class1, :domestic_classification, :service_classification, :overseas_classification, 
    :end_date, :x_while_stocks_last, :parts_center, :x_valid, :substitutional_goods, :x_stock_pc,:x_discontinuance, 
    :x_insttype_dont_use,:proc_name_eng,:eng_annotation,:price_e, :price_d_us, :price_d_eu,:precaution1, :x_precaution2,  
    :x_department_in_charge, :x_inventory_section,:x_start_date, 
    :instrument_type, :compatible_models, :category)");
    $start = microtime(true);
    echo "データベースの連結開始:".$start."です。<br>";
    //new_list_set_buffer_pre
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_pre ORDER BY reg_id DESC");//文字列なので9始まり優先に...→数値化する
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_pre ORDER BY reg_id ASC");//9始まりだと優先される
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
         //新規テーブルへ挿入/*********検索用データベース最終***********/
          if($statement2){
            $statement2->bindValue(':col_id', $record["col_id"], PDO::PARAM_STR);
            $statement2->bindValue(':reg_id', $record["reg_id"], PDO::PARAM_STR);
            $statement2->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement2->bindValue(':number_string', $record["number_string"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_type', $record["proc_type"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name', $record["proc_name"], PDO::PARAM_STR);
            $statement2->bindValue(':price_p', (int)str_replace(",","",$record["price_p"]), PDO::PARAM_STR);
            $statement2->bindValue(':price_d1', (int)str_replace(",","",$record["price_d1"]), PDO::PARAM_STR);
            $statement2->bindValue(':price_d2', (int)str_replace(",","",$record["price_d2"]), PDO::PARAM_STR);
            $statement2->bindValue(':x_price_d3', (int)str_replace(",","",$record["x_price_d3"]), PDO::PARAM_STR);
            $statement2->bindValue(':price_m', (int)str_replace(",","",$record["price_m"]), PDO::PARAM_STR);
            $statement2->bindValue(':x_group', $record["x_group"], PDO::PARAM_STR);
            $statement2->bindValue(':specification', $record["specification"], PDO::PARAM_STR);
            $statement2->bindValue(':notes', $record["notes"], PDO::PARAM_STR);
            $statement2->bindValue(':x_site', $record["x_site"], PDO::PARAM_STR);
            $statement2->bindValue(':class1', $record["class1"], PDO::PARAM_STR);
            $statement2->bindValue(':domestic_classification', $record["domestic_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':service_classification', $record["service_classification"], PDO::PARAM_STR);
            $statement2->bindValue(':overseas_classification', $record["overseas_classification"], PDO::PARAM_STR);
            //$statement2->bindValue(':end_date', date('Y-m-d',strtotime($record["end_date"])), PDO::PARAM_STR);
            if($record["end_date"] == "")
              $statement2->bindValue(':end_date', NULL, PDO::PARAM_NULL);
            else
              $statement2->bindValue(':end_date', $record["end_date"], PDO::PARAM_STR); ///在庫限りが「TRUE」と、パーツセンター対象に「TRUE」が入っている商品の有効終了日をブランクにしてあるので注意
            $statement2->bindValue(':x_while_stocks_last', $record["x_while_stocks_last"], PDO::PARAM_STR);
            $statement2->bindValue(':parts_center', $record["parts_center"], PDO::PARAM_STR);
            $statement2->bindValue(':x_valid', $record["x_valid"], PDO::PARAM_STR);
            $statement2->bindValue(':substitutional_goods', $record["substitutional_goods"], PDO::PARAM_STR);
            $statement2->bindValue(':x_stock_pc', $record["x_stock_pc"], PDO::PARAM_STR);
            $statement2->bindValue(':x_discontinuance', $record["x_discontinuance"], PDO::PARAM_STR);
            $statement2->bindValue(':x_insttype_dont_use', $record["x_insttype_dont_use"], PDO::PARAM_STR);
            $statement2->bindValue(':proc_name_eng', $record["proc_name_eng"], PDO::PARAM_STR);
            $statement2->bindValue(':eng_annotation', $record["eng_annotation"], PDO::PARAM_STR);
            $statement2->bindValue(':price_e', (int)str_replace(",","",$record["price_e"]), PDO::PARAM_STR);
            $statement2->bindValue(':price_d_us', (int)str_replace(",","",$record["price_d_us"]), PDO::PARAM_STR);
            $statement2->bindValue(':price_d_eu', (int)str_replace(",","",$record["price_d_eu"]), PDO::PARAM_STR);
            $statement2->bindValue(':precaution1', $record["precaution1"], PDO::PARAM_STR);
            $statement2->bindValue(':x_precaution2', $record["x_precaution2"], PDO::PARAM_STR);
            $statement2->bindValue(':x_department_in_charge', $record["x_department_in_charge"], PDO::PARAM_STR);
            $statement2->bindValue(':x_inventory_section', $record["x_inventory_section"], PDO::PARAM_STR);
            $statement2->bindValue(':x_start_date', $record["x_start_date"], PDO::PARAM_STR);
            $statement2->bindValue(':instrument_type', $record["instrument_type"], PDO::PARAM_STR);
            $statement2->bindValue(':compatible_models', $record["compatible_models"], PDO::PARAM_STR);
            $statement2->bindValue(':category', $record["category"], PDO::PARAM_STR);
            if($statement2->execute()){
              //echo "処理2配列への挿入成功です。<br>";
              $i++;
            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件処理1成功です。<br>";
      }else{
        echo "処理1失敗です。<br>";
      }//execute
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "処理1失敗しました。";
      echo "処理2失敗しました。<br>";
    }
  
$end = microtime(true);
echo "終了:".$end."です。<br>";
$sec = ($end - $start);
echo "処理1時間:".$sec."です。<br><br>";

    //インデックスの作成
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer ADD INDEX `code_idx` (code)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    //インデックスの作成（特注/品目番号）時に必要
    $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer ADD INDEX `number_string_idx` (number_string)");
    if($statement){
      $statement->execute();
      echo "インデックス作成成功です。<br>";
    }else{
      echo "インデックス作成失敗しました。<br>";
    }
    print('データベース登録完了！<br>');
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
  //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
  //echo "<script type='text/javascript'>history.back();</script>";//力技でウィンドウを閉じる

?>
