
<?php
class Jasco_List_Class{
  //properties
  public $JASCO_List_chCode;//商品番号
  public $JASCO_List_chNumber_string;//品目番号
  public $JASCO_List_chProc_type;//型式
  public $JASCO_List_chProc_name;//商品名称
  public $JASCO_List_iPrice_p;//販売価格
  public $JASCO_List_iPrice_d1;//卸1
  public $JASCO_List_iPrice_d2;//卸2
  public $JASCO_List_x_iPrice_d3;//卸3
  public $JASCO_List_iPrice_m;//工場卸
  public $JASCO_List_x_iGroup;//販売ｸﾞﾙｰﾌﾟ
  public $JASCO_List_chSpecification;//規格
  public $JASCO_List_chNotes;//仕様・備考→（規格に入りきらない物）
  public $JASCO_List_x_chSite;//ｻｲﾄ
  public $JASCO_List_chClass1;//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  public $JASCO_List_chDomestic_classification;//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  public $JASCO_List_chService_classification;//価格表区分2→価格表区分（サービス）
  public $JASCO_List_chOverseas_classification;//価格表区分3→価格表区分（輸出）
  public $JASCO_List_chEnd_date;//有効終了日
  public $JASCO_List_x_chWhile_stocks_laste;//在庫限り
  public $JASCO_List_chParts_centere;//パーツセンタ対象
  public $JASCO_List_x_chValid;//有効品目
  public $JASCO_List_chSubstitutional_goods;//置換商品
  public $JASCO_List_x_chDiscontinuance;//商品中止理由
  public $JASCO_List_x_insttype_dont_use;//対応機種(情報もってるだけ　Excel側の情報を使用)
  public $JASCO_List_chProc_name_eng;//英名
  public $JASCO_List_chEng_annotation;//英名注釈
  public $JASCO_List_iPrice_e;//輸出価格
  public $JASCO_List_iPrice_us;//US卸
  public $JASCO_List_iPrice_eu;//EU卸
  public $JASCO_List_chPrecaution1;//特記事項1
  public $JASCO_List_x_chPrecaution2;//特記事項2
  public $JASCO_List_x_chDepartment_in_charge;//担当課
  public $JASCO_List_x_chInventory_section;//在庫課
  public $JASCO_List_x_start_date;//有効開始日
  public $JASCO_List_chInstrument_type;//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  public $JASCO_List_chCompatible_models;//対応機種/用途
  public $JASCO_List_chCategory;//小見出しカテゴリー
  
  function __construct(){
    $JASCO_List_chCode = "null";//商品番号
    $JASCO_List_chNumber_string = "null";//品目番号
    $JASCO_List_chProc_type = "null";//型式
    $JASCO_List_chProc_name = "null";//商品名称
    $JASCO_List_iPrice_p = "null";//販売価格
    $JASCO_List_iPrice_d1 = "null";//卸1
    $JASCO_List_iPrice_d2 = "null";//卸2
    $JASCO_List_x_iPrice_d3 = "null";//卸3
    $JASCO_List_iPrice_m = "null";//工場卸
    $JASCO_List_x_iGroup = "null";//販売ｸﾞﾙｰﾌﾟ
    $JASCO_List_chSpecification = "null";//規格
    $JASCO_List_chNotes = "null";//仕様・備考→（規格に入りきらない物）
    $JASCO_List_x_chSite = "null";//ｻｲﾄ
    $JASCO_List_chClass1 = "null";//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
    $JASCO_List_chDomestic_classification = "null";//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
    $JASCO_List_chService_classification = "null";//価格表区分2→価格表区分（サービス）
    $JASCO_List_chOverseas_classification = "null";//価格表区分3→価格表区分（輸出）
    $JASCO_List_chEnd_date = "null";//有効終了日
    $JASCO_List_x_chWhile_stocks_laste = "null";//在庫限り
    $JASCO_List_chParts_centere = "null";//パーツセンタ対象
    $JASCO_List_x_chValid = "null";//有効品目
    $JASCO_List_chSubstitutional_goods = "null";//置換商品
    $JASCO_List_x_chDiscontinuance = "null";//商品中止理由
    $JASCO_List_x_insttype_dont_use = "null";//対応機種(情報もってるだけ　Excel側の情報を使用)
    $JASCO_List_chProc_name_eng = "null";//英名
    $JASCO_List_chEng_annotation = "null";//英名注釈
    $JASCO_List_iPrice_e = "null";//輸出価格
    $JASCO_List_iPrice_us = "null";//US卸
    $JASCO_List_iPrice_eu = "null";//EU卸
    $JASCO_List_chPrecaution1 = "null";//特記事項1
    $JASCO_List_x_chPrecaution2 = "null";//特記事項2
    $JASCO_List_x_chDepartment_in_charge = "null";//担当課
    $JASCO_List_x_chInventory_section = "null";//在庫課
    $JASCO_List_x_start_date = "null";//有効開始日
    $JASCO_List_chInstrument_type = "null";//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
    $JASCO_List_chCompatible_models = "null";//対応機種/用途
    $JASCO_List_chCategory = "null";//小見出しカテゴリー
  }
}

class Jasco_Reg_Class extends Jasco_List_Class{
  //sales infos
  public $JASCO_List_iSalesCount;//登録数
  //time stamp
  public $JASCO_List_rec_timestamp;//登録時刻

  function __construct(){
    $JASCO_List_iSalesCount = 0;
    $JASCO_List_rec_timestamp = 0;
  }
}

?>