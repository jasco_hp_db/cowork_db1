﻿<?php 
require_once("../TCPDF/tcpdf.php");
//login***************************************************
require("./config/loadEnv.php");
require("./config/getSessionUserInfo.php");

$userInfo = getSessionUserInfo();

if (!$userInfo) {
    // ユーザー情報が取得できなかった場合の処理
    header("Location: login.php?alert=" . urlencode("ログインが必要です"));
    exit;
}
//login**************************************************
require("./config/section.php");
$viewtype =  getsectiontype($userInfo['scode']);
//viewtype**************************************************
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>カート</title>
  <link rel="stylesheet" type="text/css" href="css/style_flex2.css?230316">
  <link rel="stylesheet" type="text/css" href="css/tablestyle.css">
	<link rel="stylesheet" type="text/css" href="css/details.css">
  <link rel="stylesheet" href="css/under_bar_menu.css">
  <link rel="stylesheet" href="css/annotation.css">
  <!--link rel="stylesheet" type="text/css" href="css/disable_focus_outline.css"-->
  <link rel="stylesheet" href="../lightbox2-2.11.2/dist/css/lightbox.min.css">
  <!--<script type="text/javascript" src="js/intense.js"></script>-->
</head>
<style type="text/css">
  .fullscreen-image {
     cursor: url("img/plus_cursor.png"), pointer; /* マウスポインタを指定 */
     display: inline-block;   /* 横方向に並べる指定 */
     margin: 0px 5px 5px 0px; /* 周囲の余白量(右と下に5pxずつ) */
  }
  table {
  border-collapse: collapse;
  border: solid 1px black;/*表全体を線で囲う*/
  }
  table th, table td {
  border: solid 1px black;
  /*破線 1px オレンジ*/
  }
  table td.col_price {
    text-align: right;
  }
  .center {
      text-align: center;
  }
</style>
<script>
//変数storageにlocalStorageを格納//sessionstorageでも入れ替え可
var storagetmp = localStorage;
var arJasco_List_Class = {
  JASCO_List_Rec_ver:"1161",//登録バージョン番号
  JASCO_List_Rec_No:"-1",//登録番号
  JASCO_List_chCode: "null",//商品番号
  JASCO_List_chNumber_string: "null",//品目番号
  JASCO_List_chProc_type: "null",//型式
  JASCO_List_chProc_name: "null",//商品名称
  JASCO_List_iPrice_p: "null",//販売価格
  JASCO_List_iPrice_d1: "null",//卸1
  JASCO_List_iPrice_d2: "null",//卸2
  JASCO_List_x_iPrice_d3: "null",//卸3
  JASCO_List_iPrice_m: "null",//工場卸
  JASCO_List_x_iGroup: "null",//販売ｸﾞﾙｰﾌﾟ
  JASCO_List_chSpecification: "null",//規格
  JASCO_List_chNotes: "null",//仕様・備考→（規格に入りきらない物）
  JASCO_List_x_chSite: "null",//ｻｲﾄ
  JASCO_List_chClass1: "null",//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  JASCO_List_chDomestic_classification: "null",//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  JASCO_List_chService_classification: "null",//価格表区分2→価格表区分（サービス）
  JASCO_List_chOverseas_classification: "null",//価格表区分3→価格表区分（輸出）
  JASCO_List_chEnd_date: "null",//有効終了日
  JASCO_List_x_chWhile_stocks_laste: "null",//在庫限り
  JASCO_List_chParts_centere: "null",//パーツセンタ対象
  JASCO_List_x_chValid: "null",//有効品目
  JASCO_List_chSubstitutional_goods: "null",//置換商品
  JASCO_List_x_chDiscontinuance: "null",//商品中止理由
  JASCO_List_chProc_name_eng: "null",//英名
  JASCO_List_chEng_annotation:"null",//英名注釈
  JASCO_List_iPrice_e:"null",//輸出価格
  JASCO_List_iPrice_us:"null",//US卸
  JASCO_List_iPrice_eu:"null",//EU卸
  JASCO_List_chPrecaution1: "null",//特記事項1
  JASCO_List_chInstrument_type: "null",//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  JASCO_List_chCompatible_models: "null",//対応機種/用途
  JASCO_List_chCategory: "null",//小見出しカテゴリー
  //写真リンク
  JASCO_List_chPicture: "null",//写真
  //sales infos
  JASCO_List_iSalesCount:"null",//登録数
  //time stamp
  JASCO_List_rec_timestamp:"0"//登録時刻
}
//データをクリアする
function cle() {
  var myRet = confirm("カートを空にします。よろしいですか？");
  if(myRet == true ){
    storagetmp.clear();
    show_result();
  }
}
//データを保存
function saverslt() {
  const fileName = "test.csvr1";//csv形式だとSJIS形式で保存してUTF-8で読みださないと化ける→文字コード変換用ライブラリの利用を避けべた書きでsave load
    
  // データをJSON形式の文字列に変換する。
  //var k = storagetmp.key(0);
  var data = "";
  data = convert_array_to_csv();//このままCSVだと日本語が文字化け...
  //alert(data);alert
  const link = document.createElement("a");
  
  // リンク先にJSON形式の文字列データを置いておく。
  link.href = "data:text/plain," + encodeURIComponent(data);
  
  // 保存するJSONファイルの名前をリンクに設定する。
  link.download = fileName;
  
  // ファイルを保存する。
  link.click();    
}
// 読み込んだCSVデータを二次元配列に変換する関数convertCSVtoArray()の定義
function convert_array_to_csv(){ // csv対応した文字列を書き込む
    var result = []; // 最終的な二次元配列を入れるための配列
    //alert(storagetmp.length);
    var str = "";
    var strsplit = "\t";//","だともともと使われている
    // 各行ごとにカンマで区切った文字列を要素とした配列を生成
    for(var i=0; i<storagetmp.length; i++){    
        var k = storagetmp.key(i);
        if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
          continue;
        }
        if(str.length < 0)
            str = k;
        else
            str += k;
        arJasco_ProductRec = JSON.parse(storagetmp.getItem(k));
        str += strsplit;
        str += "JASCO_List_Rec_ver"+strsplit + parseInt(arJasco_ProductRec.JASCO_List_Rec_ver)+strsplit;//登録バージョン番号
        str += "JASCO_List_Rec_No"+strsplit + parseInt(arJasco_ProductRec.JASCO_List_Rec_No)+strsplit;//登録番号
        str += "JASCO_List_chCode"+strsplit + arJasco_ProductRec.JASCO_List_chCode+strsplit;//商品番号
    //alert(str);
        str += "JASCO_List_chNumber_string"+strsplit + arJasco_ProductRec.JASCO_List_chNumber_string+strsplit;///品目番号
        str += "JASCO_List_chProc_type"+strsplit + arJasco_ProductRec.JASCO_List_chProc_type+strsplit;///型式
        str += "JASCO_List_chProc_name"+strsplit + arJasco_ProductRec.JASCO_List_chProc_name+strsplit;///商品名称
        str += "JASCO_List_iPrice_p"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_p+strsplit;///販売価格
        str += "JASCO_List_iPrice_d1"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_d1+strsplit;///卸1
        str += "JASCO_List_iPrice_d2"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_d2+strsplit;///卸2
    //alert(str);
        str += "JASCO_List_x_iPrice_d3"+strsplit + arJasco_ProductRec.JASCO_List_x_iPrice_d3+strsplit;///卸3
        str += "JASCO_List_iPrice_m"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_m+strsplit;///工場卸
        str += "JASCO_List_x_iGroup"+strsplit + arJasco_ProductRec.JASCO_List_x_iGroup+strsplit;///販売ｸﾞﾙｰﾌﾟ
        str += "JASCO_List_chSpecification"+strsplit + arJasco_ProductRec.JASCO_List_chSpecification+strsplit;///規格
        str += "JASCO_List_chNotes"+strsplit + arJasco_ProductRec.JASCO_List_chNotes+strsplit;///仕様・備考→（規格に入りきらない物）
        str += "JASCO_List_x_chSite"+strsplit + arJasco_ProductRec.JASCO_List_x_chSite+strsplit;///ｻｲﾄ
        str += "JASCO_List_chClass1"+strsplit + arJasco_ProductRec.JASCO_List_chClass1+strsplit;///登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
        str += "JASCO_List_chDomestic_classification"+strsplit + arJasco_ProductRec.JASCO_List_chDomestic_classification+strsplit;///価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
        str += "JASCO_List_chService_classification"+strsplit + arJasco_ProductRec.JASCO_List_chService_classification+strsplit;///価格表区分2→価格表区分（サービス）
        str += "JASCO_List_chOverseas_classification"+strsplit + arJasco_ProductRec.JASCO_List_chOverseas_classification+strsplit;///価格表区分3→価格表区分（輸出）
        str += "JASCO_List_chEnd_date"+strsplit + arJasco_ProductRec.JASCO_List_chEnd_date+strsplit;///有効終了日
        str += "JASCO_List_x_chWhile_stocks_laste"+strsplit + arJasco_ProductRec.JASCO_List_x_chWhile_stocks_laste+strsplit;///在庫限り
        str += "JASCO_List_chParts_centere"+strsplit + arJasco_ProductRec.JASCO_List_chParts_centere+strsplit;///パーツセンタ対象
        str += "JASCO_List_x_chValid"+strsplit + arJasco_ProductRec.JASCO_List_x_chValid+strsplit;///有効品目
        str += "JASCO_List_chSubstitutional_goods"+strsplit + arJasco_ProductRec.JASCO_List_chSubstitutional_goods+strsplit;///置換商品
        str += "JASCO_List_x_chDiscontinuance"+strsplit + arJasco_ProductRec.JASCO_List_x_chDiscontinuance+strsplit;///商品中止理由
        str += "JASCO_List_chProc_name_eng"+strsplit + arJasco_ProductRec.JASCO_List_chProc_name_eng+strsplit;///英名
        str += "JASCO_List_chEng_annotation"+strsplit + arJasco_ProductRec.JASCO_List_chEng_annotation+strsplit;///英名注釈
        str += "JASCO_List_iPrice_e"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_e+strsplit;///輸出価格＿存在しているけどまだ書き込んでいない
        str += "JASCO_List_iPrice_us"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_us+strsplit;///US卸＿存在しているけどまだ書き込んでいない
        str += "JASCO_List_iPrice_eu"+strsplit + arJasco_ProductRec.JASCO_List_iPrice_eu+strsplit;///EU卸＿存在しているけどまだ書き込んでいない
        str += "JASCO_List_chPrecaution1"+strsplit + arJasco_ProductRec.JASCO_List_chPrecaution1+strsplit;///特記事項1
        str += "JASCO_List_chInstrument_type"+strsplit + arJasco_ProductRec.JASCO_List_chInstrument_type+strsplit;///機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
        str += "JASCO_List_chCompatible_models"+strsplit + arJasco_ProductRec.JASCO_List_chCompatible_models+strsplit;///対応機種/用途
        str += "JASCO_List_chPicture"+strsplit + arJasco_ProductRec.JASCO_List_chPicture+strsplit;///写真
        //sales infos
        str += "JASCO_List_iSalesCount"+strsplit + parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///登録数
        //time stamp
        str += "JASCO_List_rec_timestamp"+strsplit + arJasco_ProductRec.JASCO_List_rec_timestamp;///登録時刻
        
    //alert(str);
        str +="\n";
        
    }
    return str;
}
function addCommma(tmp_value){
  var tmpstring = String(tmp_value).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  return tmpstring;
}
function reverse_to_num(tmp_value){
  var tmpnum =str_replace(',','',$tmp_value);
  return tmpnum;
}
function array_to_txt(){
  var arTmp = [];//ローカルストレージに入っている順に納める変数
  var tmpClass = arJasco_List_Class;
  var strRgst = "Jasco_shopping_cart_";
  for(var i=0; i<storagetmp.length; i++){    
    var k = storagetmp.key(i);
    if(k.match(strRgst) == -1){//その手ストレージに入っているのを飛ばす
      continue;
    }
    tmpClass = JSON.parse(storagetmp.getItem(k));
    arTmp.push(tmpClass);
  }
  var iCount = arTmp.length;
  //alert(iCount);
  var arrSource = new Array(arTmp.length);//登録しているリストナンバー順に納める変数
  for(i=0; i<iCount; i++){
    tmpClass = arTmp[i];
    arrSource[tmpClass.JASCO_List_Rec_No] = tmpClass;
  }
  
  var str = "";
  var strsplit = "\t";//","だともともと使われている
  var sum = 0;//定価合計
  var sum_d1 = 0;//卸1合計
  var sum_d2 = 0;//卸2合計
  var sum_m = 0;//工場卸合計
  //タイトル行
  str ="個数"+strsplit+"商品番号"+strsplit+"品目番号"+strsplit+"型式"+strsplit+"商品名称"+strsplit+"販売価格"+strsplit+"規格"+strsplit;
  str +="卸1"+strsplit+"卸2"+strsplit+"工場卸"+strsplit+"仕様・備考"+strsplit+"対応機種/用途"+"\n";
  // 各行ごとにカンマで区切った文字列を要素とした配列を生成
  for(i=0; i<iCount; i++){    
      arJasco_ProductRec = arrSource[i];
      str += arJasco_ProductRec.JASCO_List_iSalesCount+strsplit;///登録数
      str += arJasco_ProductRec.JASCO_List_chCode+strsplit;//商品番号
      str += arJasco_ProductRec.JASCO_List_chNumber_string+strsplit;///品目番号
      str += arJasco_ProductRec.JASCO_List_chProc_type+strsplit;///型式
      str += arJasco_ProductRec.JASCO_List_chProc_name+strsplit;///商品名称
      //str += parseInt(arJasco_ProductRec.JASCO_List_iPrice_p)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///販売価格        
      str += parseInt(arJasco_ProductRec.JASCO_List_iPrice_p)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///販売価格
      str += arJasco_ProductRec.JASCO_List_chSpecification+strsplit;///規格
      str += parseInt(arJasco_ProductRec.JASCO_List_iPrice_d1)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///卸1
      str += parseInt(arJasco_ProductRec.JASCO_List_iPrice_d2)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///卸2
      str += parseInt(arJasco_ProductRec.JASCO_List_iPrice_m)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount)+strsplit;///工場卸
      str += arJasco_ProductRec.JASCO_List_chNotes+strsplit;///仕様・備考→（規格に入りきらない物）
      str += arJasco_ProductRec.JASCO_List_chCompatible_models+strsplit;///対応機種/用途
  //alert(str);
      str +="\n";
      /*//単価
      str += strsplit+strsplit+strsplit+strsplit+strsplit;
      str +=arJasco_ProductRec.JASCO_List_iPrice_p+strsplit;///販売価格
      str +=strsplit;//規格
      str +=arJasco_ProductRec.JASCO_List_iPrice_d1+strsplit;///卸1
      str +=arJasco_ProductRec.JASCO_List_iPrice_d2+strsplit;///卸2
      str +=arJasco_ProductRec.JASCO_List_iPrice_m;///工場卸
      str +="\n";*/
      sum += parseInt(arJasco_ProductRec.JASCO_List_iPrice_p)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);
      sum_d1 += parseInt(arJasco_ProductRec.JASCO_List_iPrice_d1)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);
      sum_d2 += parseInt(arJasco_ProductRec.JASCO_List_iPrice_d2)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);
      sum_m += parseInt(arJasco_ProductRec.JASCO_List_iPrice_m)*parseInt(arJasco_ProductRec.JASCO_List_iSalesCount);
  }
  str += strsplit+strsplit+strsplit+strsplit+"合計"+strsplit;
  str += sum +strsplit;
  str +=strsplit;//規格
  str += sum_d1 +strsplit;
  str += sum_d2 +strsplit;
  str += sum_m;
  return str;
}
function copy_to_clipboard()
{
  // テキストエリアを用意する
  var copyFrom = document.createElement("textarea");
  // テキストエリアへ値をセット
  copyFrom.textContent = array_to_txt();
 
  // bodyタグの要素を取得
  var bodyElm = document.getElementsByTagName("body")[0];
  // 子要素にテキストエリアを配置
  bodyElm.appendChild(copyFrom);
 
  // テキストエリアの値を選択
  copyFrom.select();
  // コピーコマンド発行
  var retVal = document.execCommand('copy');
  // 追加テキストエリアを削除
  bodyElm.removeChild(copyFrom);

  // コピー結果をデバッグ
  alert("クリップボードへコピーしました。");
}
//データを読み込み
function loadrslt() {
  var myRet = confirm("カートを空にし保存したデータを読み込みます。よろしいですか？");
  if(myRet == true ){
      //Firefox, Chrome, Edge (IE非対応ココから)
      const showOpenFileDialog = () => {
      return new Promise(resolve => {
          const input = document.createElement('input');
          input.type = 'file';
          input.accept = '.csvr1, text/plain';//csv形式だとSJIS形式で保存してUTF-8で読みださないと化ける→文字コード変換用ライブラリの利用を避けべた書きでsave load
          input.onchange = event => { resolve(event.target.files[0]); };
              input.click();
          });
      };

      const readAsText = file => {
          return new Promise(resolve => {
              const reader = new FileReader();
              reader.readAsText(file);
              reader.onload = () => { resolve(reader.result); };
          });
      };

      (async () => {
          const file = await showOpenFileDialog();
          const content = await readAsText(file);
          // 内容表示
          //alert(content);
          // ファイルの中身をstoragetmpにそのまま投げる→セキュリティ的に危険な気がする...
          //storagetmp.clear();//全部消しちゃう→消し過ぎ？
        convert_csv_to_array(content); // 渡されるのは読み込んだCSVデータ    
        show_result();
      })();
      
      //Firefox, Chrome, Edge (IE非対応ココから)
  }
}
// 読み込んだCSVデータを二次元配列に変換する関数convertCSVtoArray()の定義
function convert_csv_to_array(str){ // 読み込んだCSVデータが文字列として渡される
    var result = []; // 最終的な二次元配列を入れるための配列
    var tmp = str.split("\n"); // 改行を区切り文字として行を要素とした配列を生成
    var arJasco_Productmp = arJasco_List_Class;
//alert(str);
    if(tmp.length<2)
    return;
    storagetmp.clear();
    // 各行ごとにカンマで区切った文字列を要素とした二次元配列を生成
    for(var i=0;i<tmp.length-1;i++){//tmp.lengthだと最後に余分に\n入っている
      result = tmp[i].split("\t");//','だともともと使われている
      var item = 2;//0→KeyName 1→"JASCO_List_chCode"
      arJasco_Productmp.JASCO_List_Rec_ver=parseInt(result[item]); item =item+2;//登録バージョン番号
      arJasco_Productmp.JASCO_List_Rec_No=parseInt(result[item]); item =item+2;//登録番号
//alert("1_"+result[item]);
      arJasco_Productmp.JASCO_List_chCode=result[item]; item =item+2;//商品番号
//alert("3_"+result[item]);        
      arJasco_Productmp.JASCO_List_chNumber_string=result[item]; item =item+2;//品目番号
//alert("4_"+result[item]);        
      arJasco_Productmp.JASCO_List_chProc_type=result[item]; item =item+2;//型式
//alert("5_"+result[item]);        
      arJasco_Productmp.JASCO_List_chProc_name=result[item]; item =item+2;//商品名称
//alert("6_"+result[item]);        
      arJasco_Productmp.JASCO_List_iPrice_p=result[item]; item =item+2;//販売価格
//alert("7_"+result[item]);        
      arJasco_Productmp.JASCO_List_iPrice_d1=result[item]; item =item+2;//卸1
//alert("8_"+result[item]);        
      arJasco_Productmp.JASCO_List_iPrice_d2=result[item]; item =item+2;//卸2
//alert("9_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_iPrice_d3=result[item]; item =item+2;//卸3
//alert("10_"+result[item]);        
      arJasco_Productmp.JASCO_List_iPrice_m=result[item]; item =item+2;//工場卸
//alert("11_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_iGroup=result[item]; item =item+2;//販売ｸﾞﾙｰﾌﾟ
//alert("12_"+result[item]);        
      arJasco_Productmp.JASCO_List_chSpecification=result[item]; item =item+2;//規格
//alert("13_"+result[item]);        
      arJasco_Productmp.JASCO_List_chNotes=result[item]; item =item+2;//仕様・備考→（規格に入りきらない物）
//alert("14_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_chSite=result[item]; item =item+2;//ｻｲﾄ
//alert("26_"+result[item]);        
arJasco_Productmp.JASCO_List_chClass1=result[item]; item =item+2;//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
//alert("16_"+result[item]);        
      arJasco_Productmp.JASCO_List_chDomestic_classification=result[item]; item =item+2;//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
//alert("17_"+result[item]);        
      arJasco_Productmp.JASCO_List_chService_classification=result[item]; item =item+2;//価格表区分2→価格表区分（サービス）
//alert("18_"+result[item]);        
      arJasco_Productmp.JASCO_List_chOverseas_classification=result[item]; item =item+2;//価格表区分3→価格表区分（輸出）
//alert("19_"+result[item]);        
      arJasco_Productmp.JASCO_List_chEnd_date=result[item]; item =item+2;//有効終了日
//alert("20_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_chWhile_stocks_laste=result[item]; item =item+2;//在庫限り
//alert("21_"+result[item]);        
      arJasco_Productmp.JASCO_List_chParts_centere=result[item]; item =item+2;//パーツセンタ対象
//alert("22_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_chValid=result[item]; item =item+2;//有効品目
//alert("23_"+result[item]);        
      arJasco_Productmp.JASCO_List_chSubstitutional_goods=result[item]; item =item+2;//置換商品
//alert("24_"+result[item]);        
      arJasco_Productmp.JASCO_List_x_chDiscontinuance=result[item]; item =item+2;//商品中止理由
//alert("27_"+result[item]);        
      arJasco_Productmp.JASCO_List_chProc_name_eng=result[item]; item =item+2;//英名
      arJasco_Productmp.JASCO_List_chEng_annotation=result[item]; item =item+2;//英名注釈
//alert("6_"+result[item]);        
      arJasco_Productmp.JASCO_List_iPrice_e=result[item]; item =item+2;//輸出価格＿存在しているけどまだ書き込んでいない
      arJasco_Productmp.JASCO_List_iPrice_us=result[item]; item =item+2;//US卸＿存在しているけどまだ書き込んでいない
      arJasco_Productmp.JASCO_List_iPrice_eu=result[item]; item =item+2;//EU卸＿存在しているけどまだ書き込んでいない
//alert("25_"+result[item]);        
      arJasco_Productmp.JASCO_List_chPrecaution1=result[item]; item =item+2;//特記事項1
//alert("29_"+result[item]);        
      arJasco_Productmp.JASCO_List_chInstrument_type=result[item]; item =item+2;//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:
//alert("30_"+result[item]);        
      arJasco_Productmp.JASCO_List_chCompatible_models=result[item]; item =item+2;//対応機種/用途
//alert("32_"+result[item]);
      arJasco_Productmp.JASCO_List_chPicture=result[item]; item =item+2;//写真
      //sales infos
      arJasco_Productmp.JASCO_List_iSalesCount=parseInt(result[item]); item =item+2;//登録数
//alert("33_"+result[item]);        
      //time stamp
      var date = new Date();
      var nowtimemm = date.getTime();     
      arJasco_Productmp.JASCO_List_rec_timestamp=nowtimemm;//登録時刻→読み込んだ時刻に
//alert("34_"+result[item]);        
      
      var stockKey = "";
      stockKey = "Jasco_shopping_cart_" + arJasco_Productmp.JASCO_List_Rec_No;
      storagetmp.setItem(stockKey, JSON.stringify(arJasco_Productmp));
    }
}

function isLocalStorageAvlbl(){// localStorage 自体が使えるかどうかの判定
  if (typeof localStorage !== 'undefined') {
    try {
      localStorage.setItem('dummy', '1');
      if (localStorage.getItem('dummy') === '1') {
        localStorage.removeItem('dummy');
        return true;
      } else {
        return false;
      }
    } catch(e) {
      return false;
    }
  } else {
    return false;
  }
}
//引数秒以上前に登録した結果をクリアする
function cle_olditem(int_seconds) {
  if(!isLocalStorageAvlbl())return;
  var date = new Date();
  var nowtimemm = date.getTime();
  var nowtime = Math.floor( nowtimemm / 1000 ) ;
  var bdelete_first = 0;
  var arJasco_Product = 0;
//alert("date:"+date+",nowtime:"+nowtime+",bdelete_first:"+bdelete_first);
  if(storagetmp.length > 0){//ここが抜けられていない？
    for(var i=storagetmp.length-1; i>=0; i--){    
      var k = storagetmp.key(i);
           // 初期時にstoragetmp.getItem(k)の中身がないと判定されるエラーを捕捉
           // その際、カートを空にする初期化を行う
	    try{
	    	arJasco_Product = JSON.parse(storagetmp.getItem(k));
	    }catch{
		    storagetmp.clear();
		    show_result();
	    }
      
      if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
          continue;
      }
	      if(nowtime - arJasco_Product.JASCO_List_rec_timestamp > int_seconds){
	  //alert("Name:"+arJasco_Product.JASCO_List_chCode+"nowtime:"+nowtime+",arJasco_Product.JASCO_List_rec_timestamp:"+arJasco_Product.JASCO_List_rec_timestamp+",int_seconds:"+int_seconds);      
	        //if(bdelete_first < 1){ //bdelete_first=0の時だけ質問。  
	          //var myRet = confirm("追加後、24時間を経過した商品を削除します。よろしいですか？");
	          //if(myRet == true ){
	          //  bdelete_first = 1;
	          //}else{
	          //  bdelete_first = 2;
	          //}
	        //}
	        //if(bdelete_first == 1){
	        //  alert("削除:"+arJasco_Product.JASCO_List_chCode);
	          storagetmp.removeItem(k);
	        //}
	      }
    }
  }
}

function return_reginfo_by_number(inum_row){
  var tmpClass = arJasco_List_Class;
  for(var i=0; i<storagetmp.length; i++){    
    k = storagetmp.key(i);
    if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
    tmpClass = JSON.parse(storagetmp.getItem(k));
    if(tmpClass.JASCO_List_Rec_No == inum_row){
      tmpClass = JSON.parse(storagetmp.getItem(k));
      return tmpClass;
    }
  }
  return null;
}
function delete_strageinfo(iReg_No){
//詰めた順にナンバリングしているので　JASCO_List_Rec_No　と　key名修正（Key名順にストレージに入っていなくても良いように）
  var arTmp = [];//ローカルストレージに入っている順に納める変数
  var tmpClass = arJasco_List_Class;
  var strRgst = "Jasco_shopping_cart_";
  for(var i=0; i<storagetmp.length; i++){    
    var k = storagetmp.key(i);
    if(k.match(strRgst) == -1){//その手ストレージに入っているのを飛ばす
      continue;
    }
    tmpClass = JSON.parse(storagetmp.getItem(k));
    arTmp.push(tmpClass);
  }
  var iCount = arTmp.length;
   //alert(iCount);
  var arrSource = new Array(arTmp.length);//登録しているリストナンバー順に納める変数
  for(i=0; i<iCount; i++){
    tmpClass = arTmp[i];
    arrSource[tmpClass.JASCO_List_Rec_No] = tmpClass;
  }
  if(iReg_No == iCount-1){
    storagetmp.removeItem(strRgst+iReg_No);//Jasco_shopping_cart_x が削除するキー名
  }else{
    for(i=iReg_No; i<iCount-1; i++){
      arrSource[i+1].JASCO_List_Rec_No = i;
      storagetmp.setItem(strRgst+i, JSON.stringify(arrSource[i+1]));
    }
    if(iCount>1){
      storagetmp.removeItem(strRgst+parseInt(iCount-1));
      //alert("removeItem:"+strRgst+parseInt(iCount-1));
    }else{
      //storagetmp.clear();
      storagetmp.removeItem(strRgst+0);
    }
  }
}
//カウントアップダウン兼ねる
function replace_jascoproc_count(strCode, iCount){//increment decrement兼ねる
  //保存されているデータの数だけループ
  for(var i=0; i<storagetmp.length; i++){    
    var k = storagetmp.key(i);
    if(k.match("Jasco_shopping_cart_") != -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
  }

  for(var i=0; i<storagetmp.length; i++){    
    var k = storagetmp.key(i);
    if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
    var arJasco_Product_tmp = JSON.parse(storagetmp.getItem(k));
    if(arJasco_Product_tmp.JASCO_List_chCode == strCode){
      if(arJasco_Product_tmp.JASCO_List_iSalesCount == iCount)
        break;
      if(iCount<1){
        var Ret = window.confirm(arJasco_Product_tmp.JASCO_List_chProc_name+"を削除します。よろしいですか？");
        if(Ret == false)
           break;
        //storagetmp.removeItem(k);
        /*********末端以外を削除したときにキーNoと登録Noに調整が必要********/
        delete_strageinfo(arJasco_Product_tmp.JASCO_List_Rec_No);
        break;
      }else{
        arJasco_Product_tmp.JASCO_List_iSalesCount = iCount;
        storagetmp.setItem(k, JSON.stringify(arJasco_Product_tmp));
        break;
      }
    }
  }
}
/*個数操作の表示*/
function getText(varaltmpidx) {
  var string_temp = "combofield_"+varaltmpidx;
  var obj = document.getElementById(string_temp);
  var index = obj.tfield2.value;

  var arJasco_Product_tmp = return_reginfo_by_number(varaltmpidx);
  if(arJasco_Product_tmp != null){
    replace_jascoproc_count(arJasco_Product_tmp.JASCO_List_chCode,index);
    show_result(); 
  }
}
function deleteselectcol(varaltmpidx)
{
  var arJasco_Product_tmp = return_reginfo_by_number(varaltmpidx);
  if(arJasco_Product_tmp != null){
    replace_jascoproc_count(arJasco_Product_tmp.JASCO_List_chCode,0);
    show_result();
  }
}
function getSelect(varaltmpidx) {
  var string_temp = "combofield_"+varaltmpidx;
  var obj = document.getElementById(string_temp);
  var index = obj.selbox2.selectedIndex;
  var arJasco_Product_tmp = return_reginfo_by_number(varaltmpidx);
  if(arJasco_Product_tmp != null){
    replace_jascoproc_count(arJasco_Product_tmp.JASCO_List_chCode,index); //JASCO_Product_strNumberで渡すべきかどうか…
    show_result();
  }
}
//保存されているデータをリスト表示する
function show_result() {
  var resultTable = "";
  //タイトル部
  resultTable+="<table style='margin:10px;'><thead><tr>";
  resultTable+="<th width=\"2%\">削除<br></th>";
  resultTable+="<th width=\"1%\">個数<br></th>";
  //resultTable+="<th class=xxx>価格表分類<br></th>";
  //resultTable+="<th class=xxx>機種分類<br></th>";
  resultTable+="<th class=xxx width=\"5%\">商品番号*<br></th>";
  resultTable+="<th class=xxx width=\"5%\">品目番号*<br></th>";
  resultTable+="<th class=xxx width=\"5%\">型　式*<br></th>";
  resultTable+="<th nowrap class=xxx width=\"50%\">商品名称*<br></th>";
  resultTable+="<th class=xxx width=\"4%\">販売価格</th>";
  resultTable+="<th class=xxx width=\"7%\">規　格*<br></th>";
  resultTable+="<th class=xxx width=\"4%\">卸　1<br></th>";//*
  resultTable+="<th class=xxx width=\"4%\">卸　2<br></th>";//*
  resultTable+="<th class=xxx width=\"4%\">工場卸</th>";//*
  resultTable+="<th class=xxx width=\"6%\">仕様 備考<br></th>";
  resultTable+="<th class=xxx width=\"8%\">対応機種/用途<br></th>";
  resultTable+="<th class=xxx width=\"5%\">写真<br></th>";
  resultTable+="</tr></thead>";
  resultTable+="<tbody>";
  var sum = 0;//定価合計
  var sum_d1 = 0;//卸1合計
  var sum_d2 = 0;//卸2合計
  var sum_m = 0;//工場卸合計
  var iregcount = 0;

  //Key名でソートされているのでJASCO_List_Rec_No使って配列並べ直し
  var arTmp = [];//ローカルストレージに入っている順に納める変数
  var tmpClass = arJasco_List_Class;
  for(var i=0; i<storagetmp.length; i++){    
    var k = storagetmp.key(i);
   // 初期時にstoragetmp.getItem(k)の中身がないと判定されるエラーを捕捉
   // その際、カートを空にする初期化を行う
    try{
    	arJasco_Product = JSON.parse(storagetmp.getItem(k));
    }catch{
	    storagetmp.clear();
	    show_result();    
    }

    if(k.match("Jasco_shopping_cart_") == -1){//前の履歴Shopping_cartその他設定を覚えるようになった時にカート情報のみ抽出できるように
      continue;
    }
    tmpClass = JSON.parse(storagetmp.getItem(k));
    //alert(storagetmp.getItem(k));
    //alert(tmpClass.JASCO_List_Rec_No+":"+tmpClass.JASCO_List_chCode);
    arTmp.push(tmpClass);
  }
  var iCount = arTmp.length;
  
  //alert(iCount);
  var arrSource = new Array(arTmp.length);//登録しているリストナンバー順に納める変数
  for(i=0; i<iCount; i++){
    tmpClass = arTmp[i];
//  alert(arTmp[i].JASCO_List_Rec_No);
    arrSource[tmpClass.JASCO_List_Rec_No] = tmpClass;
  }
  //保存されているデータの数だけループ
  var arJasco_Product = arJasco_List_Class;
  for(var i=0; i<iCount; i++){
    arJasco_Product = arrSource[i];
    //alert(arJasco_Product.JASCO_List_Rec_No);
    //resultTable+="<tbody><td class=XXX>"+ arJasco_Product.JASCO_List_iSalesCount +"</td>";
    //resultTable+="<tr><td class=XXX></td>";
    resultTable+="<tr>";
    //resultTable+="<td class=XXX><button type=\"submit\" title=\"削除\"><img src=\"image/trashbox_128.png\" width=\"20\" alt=\"削除\" onClick=\"deleteselectcol("+i+")\"></button></td>";
    resultTable+="<td class=XXX width=\"2%\"><img src=\"image/trashbox_128.png\" width=\"20\" alt=\"削除\" onClick=\"deleteselectcol("+i+")\" style=\"cursor:pointer;\"></td>";
    resultTable+="<td class=XXX width=\"1%\"><form id=\"combofield_"+i+"\">";
    resultTable+="<select name=\"select\" class=\"selectBox\" id=\"selbox2\" onChange=\"getSelect("+i+")\">";
    resultTable+="<option value=\"0\">0</option>";
    resultTable+="<option value=\"1\""; if(arJasco_Product.JASCO_List_iSalesCount==1)resultTable+="selected";resultTable+=">1</option>";
    resultTable+="<option value=\"2\""; if(arJasco_Product.JASCO_List_iSalesCount==2)resultTable+="selected";resultTable+=">2</option>";
    resultTable+="<option value=\"3\""; if(arJasco_Product.JASCO_List_iSalesCount==3)resultTable+="selected";resultTable+=">3</option>";
    resultTable+="<option value=\"4\""; if(arJasco_Product.JASCO_List_iSalesCount==4)resultTable+="selected";resultTable+=">4</option>";
    resultTable+="<option value=\"5\""; if(arJasco_Product.JASCO_List_iSalesCount==5)resultTable+="selected";resultTable+=">5</option>";
    resultTable+="<option value=\"6\""; if(arJasco_Product.JASCO_List_iSalesCount==6)resultTable+="selected";resultTable+=">6</option>";
    resultTable+="<option value=\"7\""; if(arJasco_Product.JASCO_List_iSalesCount==7)resultTable+="selected";resultTable+=">7</option>";
    resultTable+="<option value=\"8\""; if(arJasco_Product.JASCO_List_iSalesCount==8)resultTable+="selected";resultTable+=">8</option>";
    resultTable+="<option value=\"9\""; if(arJasco_Product.JASCO_List_iSalesCount==9)resultTable+="selected";resultTable+=">9</option>";
    resultTable+="<option value=\"10\""; if(arJasco_Product.JASCO_List_iSalesCount>9)resultTable+="selected";resultTable+=">10+</option>";
    resultTable+="</select>";
    if(arJasco_Product.JASCO_List_iSalesCount <= 9){
      resultTable+="<input style=\"display:none\" name=\"textfield\" type=\"tel\" class=\"textfield\" id=\"tfield2\" onChange=\"getText("+i+")\" value=\""+arJasco_Product.JASCO_List_iSalesCount+"\">"; 
    }else{
      resultTable+="<input style=\"display:block\" name=\"textfield\" type=\"tel\" class=\"textfield\" id=\"tfield2\" onChange=\"getText("+i+")\" value=\""+arJasco_Product.JASCO_List_iSalesCount+"\">"; 
    }
    resultTable+="</form></td>";
    resultTable+="<td width=\"5%\" align='left'>"+"<a href = \"detail_list_window_n3.php?List_CodeID="+arJasco_Product.JASCO_List_chCode+"\">"+ arJasco_Product.JASCO_List_chCode+"</a>"+"</td>";
    resultTable+="<td width=\"5%\" align='left'>"+ arJasco_Product.JASCO_List_chNumber_string +"</td>";
    resultTable+="<td width=\"5%\" align='left'>"+ arJasco_Product.JASCO_List_chProc_type +"</td>";
    resultTable+="<td nowrap width=\"50%\" align='left'>"+ arJasco_Product.JASCO_List_chProc_name +"</td>";
    //定価
    if(arJasco_Product.JASCO_List_iPrice_p != 0)
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'>"+ addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_p)*parseInt(arJasco_Product.JASCO_List_iSalesCount))+"<br>";
    else
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'><br>";    
    //規格
    resultTable+="<td width=\"7%\" align='left'>"+ arJasco_Product.JASCO_List_chSpecification  +"</td>";//規格
    //卸1
    if(arJasco_Product.JASCO_List_iPrice_d1 != 0)
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'>"+ addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_d1)*parseInt(arJasco_Product.JASCO_List_iSalesCount))+"<br>";
    else
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'><br>";
    //卸2
    if(arJasco_Product.JASCO_List_iPrice_d2 != 0)
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'>"+ addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_d2)*parseInt(arJasco_Product.JASCO_List_iSalesCount))+"<br>";
    else
      resultTable+="<td class=\"col_price\" width=\"4%\" align='left'><br>";
    //工場卸
    resultTable+="<td class=\"col_price\" width=\"4%\" align='left'>"+ addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_m)*parseInt(arJasco_Product.JASCO_List_iSalesCount))+"<br>";
    //// resultTable+="("+addCommma(parseInt(arJasco_Product.JASCO_List_iPrice_m))+")</td>";//工場卸
    resultTable+="<td width=\"6%\" align='left'>"+ arJasco_Product.JASCO_List_chNotes +"</td>";
    resultTable+="<td width=\"8%\" align='left'>"+ arJasco_Product.JASCO_List_chCompatible_models +"</td>";
    resultTable+="<td width=\"5%\" align='left'>";
    //resultTable+="<div class=\"fullscreen-image\" data-image=\""+arJasco_Product.JASCO_List_chPicture+"\" data-title=\""+arJasco_Product.JASCO_List_chCode+"\" data-caption=\""+arJasco_Product.JASCO_List_chNumber_string+"\">";
    //resultTable+="<img src=\""+arJasco_Product.JASCO_List_chPicture+"\" width=\"50\" alt=\" \">";
    resultTable+="<div><a class=\"example-image-link\" href=\""+arJasco_Product.JASCO_List_chPicture+"\" data-lightbox=\"cart"+i+"\" data-title=\""+arJasco_Product.JASCO_List_chCode+"<br>"+arJasco_Product.JASCO_List_chProc_name+"\"><img class=\"example-image\" src=\""+arJasco_Product.JASCO_List_chPicture+"\" width=\"50\" alt=\" \"></a></div>";
    resultTable+="</div></td>";
    resultTable+="</tr>";
    if(arJasco_Product.JASCO_List_iPrice_p!=0)
    sum += parseInt(arJasco_Product.JASCO_List_iPrice_p)*parseInt(arJasco_Product.JASCO_List_iSalesCount);
    if(arJasco_Product.JASCO_List_iPrice_d1!=0)
    sum_d1 += parseInt(arJasco_Product.JASCO_List_iPrice_d1)*parseInt(arJasco_Product.JASCO_List_iSalesCount);
    if(arJasco_Product.JASCO_List_iPrice_d2!=0)
    sum_d2 += parseInt(arJasco_Product.JASCO_List_iPrice_d2)*parseInt(arJasco_Product.JASCO_List_iSalesCount);
    sum_m += parseInt(arJasco_Product.JASCO_List_iPrice_m)*parseInt(arJasco_Product.JASCO_List_iSalesCount);
    //sum += parseInt(arJasco_Product.JASCO_Product_dPriceP);
  }
  resultTable+="<td></td>"+"<td></td>"+"<td></td>"+"<td></td>"+"<td></td>"+
  "<td align='left'>合計<span style='color:red;'>（税抜き価格）</span><br></td>";
  resultTable+="<td align=\"right\">" + addCommma(sum) + "</td>";
  resultTable+="<td></td>";
  if(sum_d1!=0)
    resultTable+="<td align=\"right\">" + addCommma(sum_d1) + "</td>";
  else
    resultTable+="<td align=\"right\"></td>";
  if(sum_d2!=0)
    resultTable+="<td align=\"right\">" + addCommma(sum_d2) + "</td>";
  else
    resultTable+="<td align=\"right\"></td>";
  resultTable+="<td align=\"right\">" + addCommma(sum_m) + "</td>";
  resultTable+="<td></td>"+"<td></td>"+"<td></td>";
  resultTable+="</tbody>";
  //resultTable+="<sup id=\"annotation1\">※1: 税抜き価格(円)です。</sup>";//ここに書くとTableの上に表記されてしまう。
  //resultTable+="<tfoot><tr>";
  //resultTable+="<td colspan=\"12\"align=\"right\">"+"合計(税抜)：" + addCommma(sum) + "円です。</td>";
  //resultTable+="</tfoot></tr></table>";
  //上のループで作成されたテキストを表示する
  document.getElementById("show_result_field").innerHTML = resultTable;
  //</body>の直前画像拡大用

  //var elements = document.querySelectorAll( '.fullscreen-image' );
  //Intense( elements );
}
//初期Load
function init(){
  //cle_olditem(86400);//86400 sec＝1day
  show_result();
}
</script>
<body onLoad="init()">
  <div class="bg-success padding-y-20">
    <div class="container text-center">
    <h1></h1><br><br><br><br><br>
    <div style="margin:0px auto;width:875px;">
    	    <!--<hr>-->
	    <p style="font-size:small;text-align:left;width:875px;margin-left:10px;">
	    <b>お知らせ：</b>カート一覧画面は現在試作中です。一時保存やコピー用としてご利用ください。<br>
	    <span style="color:red;">商品がカートに反映されない場合は「カートを空にする」ボタンを押して、もう一度トライしてください。</span><br>
	    活用法やご意見を
	    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdJ9e_ScQ8UC7ZDBxh4mpNeTFXIOuo-_puKpvel658SF2Zy8A/viewform" target="new" rel="noopner">
	    	提案フォーム
	    </a>
	    にお寄せください。
	    </p>
	    <!--<hr>-->
    </div>
<?php require("./component/return.php"); ?>
    <p style="font-size:xx-large;font-weight:bold;margin-top:10px;margin-bottom:10px;">カート一覧</p>
    <div id="show_result_field"></div><br>
    <!--<sup id="annotation1">※1: 税抜き価格(円)です。</sup><br>-->
    <?php $dval = $_GET["direct"];?>
    
    <div class="btn-flat-simple" onClick="cle()" style="font-size:small;font-weight:normal;">カートを空にする</div>
    <div class="btn-flat-simple" onClick="saverslt()" style="font-size:small;font-weight:normal;">保存</div>
    <div class="btn-flat-simple" onClick="loadrslt()" style="font-size:small;font-weight:normal;">読込</div>
    <div class="btn-flat-simple" onClick="copy_to_clipboard()" style="font-size:small;font-weight:normal;">コピー</div>
    <!--<a href ="print_cart_list_to_pdf.php"class="btn-flat-simple" target="_blank"> 印刷</a>-->
    <!--アンダーバーメニュー表示と重複した部分をスクロールして表示するため-->
    
    <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php" class="class_white_text">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
		    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div>
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
          <a href="javascript:void(0);" onclick="confirmLogout()">
	    	    <img src="image/logout.png" width="40" height="40" alt="logout" title="ログアウト" style="margin-top:5px;margin-left:35px;float:right;">
          </a>
		    </div>
	   </div>
  </div>
    <br><br>
    <?php require("./component/return.php"); ?>       
    <br>
    <br><br><br>
  
<script src="../lightbox2-2.11.2/dist/js/lightbox-plus-jquery.min.js"></script>
<script>
  function confirmLogout() {
      if (confirm('ログアウトします。よろしいですか？')) {
          window.location.href = 'login.php';
      }
  }
</script>
</body>
</html>