﻿<?php
  //require_once("jasco_class_definition.php");
  //直接ページにアクセスされたときの対応
  /*if(isset($_POST["input_searchWord"])==false){
    header("Location: input_post_message.php")
    exit;
  }*/
  //データベースへ接続設定
  require("./config/dbConnect.php");
  header("Content-type: text/html; charset=utf-8");
  require("./component/header.php");
  //写真フォルダパスの設定
  require("./config/filePath.php");
  require("create_search_words_list.php");

//login***************************************************
  require("./config/loadEnv.php");
  require("./config/getSessionUserInfo.php");

  $userInfo = getSessionUserInfo();

  if (!$userInfo) {
      // ユーザー情報が取得できなかった場合の処理
      header("Location: login.php?alert=" . urlencode("ログインが必要です"));
      exit;
  }
//login**************************************************
  require("./config/section.php");
  $viewtype =  getsectiontype($userInfo['scode']);
  //viewtype**************************************************

  //echo "検索ワード:".htmlspecialchars($_POST["input_searchWord"],ENT_QUOTES)."です。<br>";
  $text1 = '';
  $dummy_code = '';
  //送信データの取得(詳細検索)
  $text1 = htmlspecialchars($_POST["input_searchWord"],ENT_QUOTES);
  $text_input_searchWord = $text1;
  $freeword_size=0;
  $where = "";
  if (strlen($text1) >0 ){
    //受け取ったキーワードの全角スペースを半角スペースに変換する
    $text1 = str_replace("\t", "", $text1);//表を多めにコピーしたら\tが入ってしまうので削除
    $text1 = str_replace("\n", "", $text1);//改行も削除
    $text1 = str_replace("&amp;", " ", $text1);;//&を削除
    $text1 = str_replace("&quot;", " ", $text1);;//"を削除
    $text1 = str_replace("'", " ", $text1);;//'を削除
    $text1 = str_replace("&lt;", " ", $text1);;//<を削除
    $text1 = str_replace("&gt;", " ", $text1);;//>を削除
    
    //echo"<br><br><br><br><br><br><br><br><br><br><br><br>フリーワード:".$text1."です。<br>"; //debug

    $text2 = str_replace("　", " ", $text1);

    //キーワードを空白で分割する
    $array = explode(" ",$text2);
    $freeword_size=count($array);
    //分割された個々のキーワードをSQLの条件where句に反映する
    $where = " (";
    for($i = 0; $i <$freeword_size;$i++){

      $operate_key1 = "LIKE";
      $operate_key2 = "OR";
      $operate_key3 = "";

      if($array[$i]=='@OR'){
        //echo "<script>alert('OR');</script>";
        continue;
      }
      
      if($array[$i]=='@NOT'){
        //echo "<script>alert('NOT');</script>";
        if ($i >= $freeword_size-1)
           break;//roop抜ける
        $i++;
        $operate_key1 = "NOT LIKE";
        $operate_key2 = "AND";
        $operate_key3 = "OR precaution1 IS NULL";
      }

      $where .= "(";
      $where .= "code $operate_key1 (:cCodeFree".$i.") collate utf8_unicode_ci $operate_key2 ";
      $where .= "number_string $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "proc_type $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "proc_name $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "price_p $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 "; 
      $where .= "price_d1 $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 "; 
      $where .= "price_d2 $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "price_m $operate_key1 (:cCodeFree".$i.") collate utf8_unicode_ci $operate_key2 ";
      $where .= "specification $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "notes $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "parts_center $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "substitutional_goods $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "(precaution1 $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key3) $operate_key2 ";
      $where .= "class1 $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "proc_name_eng $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "instrument_type  $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci $operate_key2 ";
      $where .= "compatible_models $operate_key1 (:cCodeFree".$i.")  collate utf8_unicode_ci";
      $where .= ")";      

      if ($i <$freeword_size-1){
        if($array[$i+1]=='@OR'){
          $where .= " OR ";
        }else{
          $where .= " AND ";
        }
      }
    }
  }
  //$where = "code LIKE '%$text1%' collate utf8_unicode_ci";//debug
  //echo"フリーワード:".$where."です。<br>"; //debug
  
  $text_input_chModel = '';
  //<!--対応機種-->
  $icount_compatible_models = 0;//検索ワード数　参照渡しなので注意
  $arr_compatible_models = [];//検索ワードが入った配列
  $text_input_chModel = "";
  if( isset( $_POST["input_chModel"]))
  $text_input_chModel = htmlspecialchars($_POST["input_chModel"],ENT_QUOTES);
  if (strlen($text_input_chModel) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="compatible_models LIKE (:chModel) collate utf8_unicode_ci";

    //echo"<br><br><br><br><br><br><br><br><br><br><br><br>";//debug
    /*echo"対応機種:".create_search_word($text_input_chModel,"compatible_models","chModel",$where_counttest,$array_returntest)."です。<br>"; //debug
    echo"対応機種検索ワード数:".$where_counttest."個です ".count($array_returntest)."個です。<br>"; //debug
    for($i = 0; $i <$where_counttest;$i++){//debug
      echo"対応機種検索ワード:".$array_returntest[$i]."です。<br>"; 
    }*/
    //echo "対応機種:".create_search_word($text_input_chModel,"compatible_models","chModel",$icount_compatible_models,$arr_compatible_models)."です。<br>"; //debug
    $where .=create_search_word($text_input_chModel,"compatible_models","chModel",$icount_compatible_models,$arr_compatible_models);
  }  
 
  //<!--機種分類表-->
  $text_input_iInstrumentType1 = '';
  $text_input_iInstrumentType2 = '';
  if( isset( $_POST["input_iInstrumentType"]))
  $text_input_iInstrumentType1 = htmlspecialchars($_POST["input_iInstrumentType"],ENT_QUOTES);
  if (strlen($text_input_iInstrumentType1) >0 ){
    if(strlen($where) >0)
      $where .= " AND ";
    $text_input_iInstrumentType2 = intval($text_input_iInstrumentType1);//01と1がある…→修正により0なしの1はなさそう
    if(intval($text_input_iInstrumentType1) < 10)
      $text_input_iInstrumentType2 = str_pad($text_input_iInstrumentType1, 2, 0, STR_PAD_LEFT);//2桁目に0を入れる01～09
    $where .="instrument_type LIKE (:iInstrumentType) collate utf8_unicode_ci";
  }
  //<!--商品番号-->
  $icount_code = 0;//検索ワード数　参照渡しなので注意
  $arr_code = [];//検索ワードが入った配列
  $text_input_cCode = '';
  if( isset( $_POST["input_cCode"]))
  $text_input_cCode = htmlspecialchars($_POST["input_cCode"],ENT_QUOTES);
  if (strlen($text_input_cCode) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="code LIKE (:cCode) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_cCode,"code","cCode",$icount_code,$arr_code);
  }  
  //<!--品目番号-->
  $icount_number_string = 0;//検索ワード数　参照渡しなので注意
  $arr_number_string = [];//検索ワードが入った配列
  $text_input_strNumber = '';
  if( isset( $_POST["input_strNumber"]))
  $text_input_strNumber = htmlspecialchars($_POST["input_strNumber"],ENT_QUOTES);
  if (strlen($text_input_strNumber) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="number_string LIKE (:strNumber) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_strNumber,"number_string","strNumber",$icount_number_string,$arr_number_string);
  }  
  //<!--型式-->
  $icount_proc_type = 0;//検索ワード数　参照渡しなので注意
  $arr_proc_type = [];//検索ワードが入った配列
  $text_input_strType = '';
  if( isset( $_POST["input_strType"]))
  $text_input_strType = htmlspecialchars($_POST["input_strType"],ENT_QUOTES);
  if (strlen($text_input_strType) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
      //可能であればここで品目番号を曖昧検索する（末端のアルファベットを無視、-有無を無視など）
    ///$where .="proc_type LIKE (:strType) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_strType,"proc_type","strType",$icount_proc_type,$arr_proc_type);
  }  
  //<!--商品名称-->
  $icount_proc_name = 0;//検索ワード数　参照渡しなので注意
  $arr_proc_name = [];//検索ワードが入った配列
  $text_input_strName = '';
  if( isset( $_POST["input_strName"]))
  $text_input_strName = htmlspecialchars($_POST["input_strName"],ENT_QUOTES);
  if (strlen($text_input_strName) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="proc_name LIKE (:strName) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_strName,"proc_name","strName",$icount_proc_name,$arr_proc_name);
  }  
  //<!--規格-->
  $icount_specification = 0;//検索ワード数　参照渡しなので注意
  $arr_specification = [];//検索ワードが入った配列
  $text_input_strSpec = '';
  if( isset( $_POST["input_strSpec"]))
  $text_input_strSpec = htmlspecialchars($_POST["input_strSpec"],ENT_QUOTES);
  if (strlen($text_input_strSpec) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="specification LIKE (:strSpec) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_strSpec,"specification","strSpec",$icount_specification,$arr_specification);
    //echo"<br><br><br><br><br><br><br><br><br><br><br><br>";//debug
    //echo "対応機種:".create_search_word($text_input_strSpec,"specification","strSpec",$icount_specification,$arr_specification);
  } 
  //<!--仕様・備考-->
  $icount_notes = 0;//検索ワード数　参照渡しなので注意
  $arr_notes = [];//検索ワードが入った配列
  $text_input_strNotes = '';
  if( isset( $_POST["input_strNotes"]))
  $text_input_strNotes = htmlspecialchars($_POST["input_strNotes"],ENT_QUOTES);
  if (strlen($text_input_strNotes) >0 ){
    if(strlen($where) >0)
      $where .= " AND "; 
    ///$where .="notes LIKE (:strNotes) collate utf8_unicode_ci";
    $where .=create_search_word($text_input_strNotes,"notes","strNotes",$icount_notes,$arr_notes);
  }  

  //<!--表示非表示-->
  $ival1 = false;
  $ival2 = false;
  $ival3 = false;
  $ival4 = false;
  $ival5 = false;
  $ival6 = false;
  $ival_v1 = false;
  $ival_v2 = false;
  $ival_v3 = false;

  if( isset( $_POST["div_01"]))
  $ival1 = $_POST["div_01"];//checked == true で入る"本体"
  if( isset( $_POST["div_02"]))
  $ival2 = $_POST["div_02"];//checked == true で入る"付属品"
  if( isset( $_POST["div_03"]))
  $ival3 = $_POST["div_03"];//checked == true で入る"ソフトウェア"
  if( isset( $_POST["div_04"]))
  $ival4 = $_POST["div_04"];//checked == true で入る"消耗品"
  if( isset( $_POST["div_05"]))
  $ival5 = $_POST["div_05"];//checked == true で入る"その他"
  if( isset( $_POST["div_06"]))
  $ival6 = $_POST["div_06"];//checked == true で入る"中止商品"
  if( isset( $_POST["price_01"]))
  $ival_v1 = $_POST["price_01"];//checked == true で入る 国内
  if( isset( $_POST["price_02"]))
  $ival_v2 = $_POST["price_02"];//checked == true で入る J/E
  if( isset( $_POST["price_03"]))
  $ival_v3 = $_POST["price_03"];//checked == true で入る 海外
  $text1 = "";
  $today = date("Y/m/d");
  //$where .="class1 IN ('本体','付属品')";//括弧内のいずれかの値に等しい TEST
  if($ival1 == true)
    $text1 .= "'本体'";
  if($ival2 == true){
    if(strlen($text1) >0)$text1 .= ",";
    $text1 .= "'付属品'";
  }
  if($ival3 == true){
    if(strlen($text1) >0)$text1 .= ",";
    $text1 .= "'ソフトウェア'";
  }
  if($ival4 == true){
    if(strlen($text1) >0)$text1 .= ",";
    $text1 .= "'消耗品'";
  }
  if($ival5 == true){
    if(strlen($text1) >0)$text1 .= ",";
    $text1 .= "''";//その他（ブランク）
  }

  if(strlen($text1) >0){
    if(strlen($where) >0)
      $where .= " AND "; 
    //echo"text1:".$text1."です。<br>";//for debug
    $where .="class1 IN ($text1)";//括弧内のいずれかの値に等しい
  }

  //表示項目(ここから)
  //if($ival_v3 == false && $ival_v2 == false && $ival_v1 == false){
  //  echo "<script>alert('表示項目：国内・J/E・海外のうち一つは選択してください');</script>";
  //  echo "<script>history.go(-1);</script>";
  //}
  if(strlen($text_input_searchWord) >0 )
   $where .= " )";
  //国内(価格表区分1)
  if($ival_v1 == true && $ival_v2 == true && $ival_v3 == true ){
  }else{
    if($ival_v1 == true){//価格表区分1が細分化されたときにここで非表示を権限で細分化する
      if($ival_v2 != true && $ival_v3 != true){
        if(strlen($where) >0)
          $where .= " AND "; 
        //$text1 = "テスト1";
        //$where .= "domestic_classification LIKE '%$text1%' collate utf8_unicode_ci";
        $where .= "domestic_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
      }else{
        if($ival_v2 == true){
          if(strlen($where) >0)
            $where .= " AND "; 
          //$text1 = "テスト1";
          $where .="(";
          //$where .= "domestic_classification LIKE '%$text1%' collate utf8_unicode_ci";
          //$where .=" OR ";
          //$where .= "service_classification LIKE '%$text1%' collate utf8_unicode_ci";
          $where .= "domestic_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
          $where .=" OR ";
          $where .= "service_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
          $where .=")";
        }
        if($ival_v3 == true){
          if(strlen($where) >0)
            $where .= " AND "; 
          //$text1 = "テスト1";
          $where .="(";
          //$where .= "domestic_classification LIKE '%$text1%' collate utf8_unicode_ci";
          //$where .=" OR ";
          //$where .= "overseas_classification LIKE '%$text1%' collate utf8_unicode_ci";
          $where .= "domestic_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
          $where .=" OR ";
          $where .= "overseas_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
          $where .=")";
        }
      }
    }elseif($ival_v2 == true){//JE(価格表区分2)
      if($ival_v3 != true){
        if(strlen($where) >0)
          $where .= " AND "; 
        //$text1 = "テスト1";
        //$where .= "service_classification LIKE '%$text1%' collate utf8_unicode_ci";
        $where .= "service_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
      }else{
        if(strlen($where) >0)
          $where .= " AND "; 
        //$text1 = "テスト1";
        $where .="(";
        //$where .= "service_classification LIKE '%$text1%' collate utf8_unicode_ci";
        //$where .=" OR ";
        //$where .= "overseas_classification LIKE '%$text1%' collate utf8_unicode_ci";
        $where .= "service_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
        $where .=" OR ";
        $where .= "overseas_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
        $where .=")";
      }
    }elseif($ival_v3 == true){//海外(価格表区分3)
      if(strlen($where) >0)
        $where .= " AND "; 
      //$text1 = "テスト1";
      //$where .= "overseas_classification LIKE '%$text1%' collate utf8_unicode_ci";
      $where .= "overseas_classification NOT LIKE '' collate utf8_unicode_ci";//空白を除く
    }
  }
  //<!--表示非表示-->
  //echo"検索条件:".$where."です。<br>";//for debug
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer WHERE ".$where);
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer");
    if($statement){

      //プレースホルダへ実際の値を設定する
      if ($freeword_size >0){
        for($i = 0; $i <$freeword_size;$i++){
          $statement->bindValue(":cCodeFree".$i, "%$array[$i]%", PDO::PARAM_STR);
        }
      }
      
      //<!--対応機種-->
      if (strlen($text_input_chModel) >0){
      ///$statement->bindValue(':chModel', "%$text_input_chModel%", PDO::PARAM_STR);
        if ($icount_compatible_models >0){
          for($i = 0; $i <$icount_compatible_models;$i++){
            $statement->bindValue(":chModel_".$i, "%$arr_compatible_models[$i]%", PDO::PARAM_STR);
            //echo ":chModel-".$i.":". $arr_compatible_models[$i]."です。<br>"; //debug
          }
        }
      }
      //<!--機種分類表-->
      if (strlen($text_input_iInstrumentType2) >0)
      $statement->bindValue(':iInstrumentType', "%$text_input_iInstrumentType2%", PDO::PARAM_STR);
      
      //<!--商品番号-->
      if (strlen($text_input_cCode) >0){
        ///$statement->bindValue(':cCode', "%$text_input_cCode%", PDO::PARAM_STR);
          if ($icount_code >0){
            for($i = 0; $i <$icount_code;$i++){
              $statement->bindValue(":cCode_".$i, "%$arr_code[$i]%", PDO::PARAM_STR);
            }
          }
      }
      //<!--品目番号-->
      if (strlen($text_input_strNumber) >0){
      ///$statement->bindValue(':strNumber', "%$text_input_strNumber%", PDO::PARAM_STR);
          if ($icount_number_string >0){
            for($i = 0; $i <$icount_number_string;$i++){
              $statement->bindValue(":strNumber_".$i, "%$arr_number_string[$i]%", PDO::PARAM_STR);
            }
          }
      }
      //<!--型式-->
      if (strlen($text_input_strType) >0){
      ///$statement->bindValue(':strType', "%$text_input_strType%", PDO::PARAM_STR);
        if ($icount_proc_type >0){
          for($i = 0; $i <$icount_proc_type;$i++){
            $statement->bindValue(":strType_".$i, "%$arr_proc_type[$i]%", PDO::PARAM_STR);
          }
        }
      }
      //<!--商品名称-->
      if (strlen($text_input_strName) >0){
      ///$statement->bindValue(':strName', "%$text_input_strName%", PDO::PARAM_STR);
        if ($icount_proc_name >0){
          for($i = 0; $i <$icount_proc_name;$i++){
            $statement->bindValue(":strName_".$i, "%$arr_proc_name[$i]%", PDO::PARAM_STR);
          }
        }
      }
      //<!--規格-->
      if (strlen($text_input_strSpec) >0){
        ///$statement->bindValue(':strSpec', "%$text_input_strSpec%", PDO::PARAM_STR);
        if ($icount_specification >0){
          for($i = 0; $i <$icount_specification;$i++){
            $statement->bindValue(":strSpec_".$i, "%$arr_specification[$i]%", PDO::PARAM_STR);
          }
        }
      }
      //<!--仕様・備考-->
      if (strlen($text_input_strNotes) >0){
      ///$statement->bindValue(':strNotes', "%$text_input_strNotes%", PDO::PARAM_STR);
        if ($icount_notes >0){
          for($i = 0; $i <$icount_notes;$i++){
            $statement->bindValue(":strNotes_".$i, "%$arr_notes[$i]%", PDO::PARAM_STR);
          }
        }
      }
      if($statement->execute()){
        //echo "検索実行開始です。<br>";//for debug
        //レコード件数取得
        $row_count = $statement->rowCount();
        $dbh->beginTransaction();//必須
        
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          
          //中止商品・非表示
          if($ival6 != true){
            if($record["end_date"] != 0 && strtotime($today) > strtotime($record["end_date"]) && $record["x_while_stocks_last"] != "TRUE"){
              $row_count--;
              continue;
            }
          }

          $rows[] = $record;
        }
        $dbh->commit();//必須
        //echo "検索結果:".$row_count."件です。<br>";
      }else{
        $errors['error'] = "検索失敗しました。";
        echo "検索失敗しました。<br>";
      }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

  /////1回目の表示/////////
  echo "<h1></h1><br><br><br>";
  //表示件数
  //$text_tmp = "";
  //if( isset( $_POST["dataNum"]))
  //$text_tmp = htmlspecialchars($_POST["dataNum"],ENT_QUOTES);
  $view_dataNum = 0;
/* //表示件数は使わないため一旦削除  
  switch ($text_tmp) {
    case "25件":
      $view_dataNum = 25;
      break; 
    case "50件":
      $view_dataNum = 50;
      break; 
    case "100件":
      $view_dataNum = 100;
      break; 
  }*/
  //$tmp_dataNum_page = htmlspecialchars($_POST["dataNum_currpage"],ENT_QUOTES);
  $tmp_dataNum_page = 0;//これが編集できれば最低限表部分はページングされる
  //echo "検索結果:".$row_count."件です。<br>";//for debug
  
  ///javascriptで書いていた関数の焼き直し
  //商品番号に対して登録数を返す
 // function get_jascoproc_count($arg_strCode)
 // {
 //   $ircount = 0;
    //保存されているデータの数だけループ
    /* foreach($rows as $row2){
      if($row2["code"] == $arg_strCode){
        $ircount = $row2["JASCO_List_iSalesCount"];//登録数はローカルストレージで持っている情報
        break;
      }
    } */
 //   return $ircount;
 // }  
  ///javascriptで書いていた関数の焼き直し

  //$start = microtime(true);//for debug
  //echo "開始:".$start."です。<br>";//for debug
  //ここからshow_result()***********************************************************************
  echo "<br>";
  require("./component/return.php");
  //対応機種/用途/国内/サービス/海外　表示・非表示切換用ボタン
  echo "<div style=\"width:1660px;padding-top:0px;margin:0 auto;\">";
  echo "<div style=\"display:grid;grid-template-columns:6fr 1fr;\">";
  echo "<div>検索結果: $row_count 件です。<br>";
  echo "<sup id=\"annotation1\" style=\"font-size:13px;color:black;\">	
  *帳票類(納品請求書、出荷ラベルなど)記載項目&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P/C=パーツセンター対象商品
  </sup></div>";
  
  echo "<div>&nbsp;<br>";
  echo "<sup style=\"font-size:13px;color:black;text-align:right;\">対応機種、価格表区分を表示&nbsp;　</sup>";
  echo "<span class=\"toggle_button\">";
  echo "<input id=\"toggle\" class=\"toggle_input\" type='checkbox' />";
  echo "<label for=\"toggle\" class=\"toggle_label\"/>";
  echo "</div></div>";

  // 直近で必要な海外だけ、位置調整
  $leftMargin ="";
  if($viewtype == DEP_ABROAD){
    $leftMargin = "style='margin-left:-120px;'"; 
  }

  echo "<table id=\"resultlist\" width=\"1660\" $leftMargin>";
  echo "<thead>";
  echo "<tr>";
  echo "<th class=\"fixed_col\" width=\"15\">カ<br>｜<br>ト</th>";
  if($viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col\" width=\"13\">P<br>/<br>C<br></th>";
  echo "<th class=\"fixed_col\" width=\"50\">写　真<br></th>";
  echo "<th class=\"fixed_col\" width=\"74\">商品番号*</th>";
  echo "<th class=\"fixed_col\" width=\"80\">品目番号*</th>";
  echo "<th class=\"fixed_col\" width=\"105\">型　式*</th>";
  echo "<th class=\"fixed_col\" width=\"196\">商品名称*</th>";
  echo "<th class=\"fixed_col\" width=\"71\">販売価格<br></th>";
  if($viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU ){
    echo "<th class=\"fixed_col\" width=\"175\">規　格*</th>";
    echo "<th class=\"fixed_col\" width=\"200\">仕様  備考</th>";
  }
  if($viewtype != DEP_ABROAD  && $viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col\" width=\"71\">卸　1<br></th>";
  if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col\" width=\"71\">卸　2<br></th>";
  if($viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_JI  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col\" width=\"71\">工場卸<br></th>";
  //echo "<th class=\"fixed_col\" width=\"145\">対応機種/用途<br></th>";
  echo "<th class=\"fixed_col_int_hide\" width=\"145\" style=\"display:none;\">対応機種/用途<br></th>";
  if($ival6 == true){//<!--中止商品表示-->
    echo "<th class=\"fixed_col\" width=\"75\">有効終了日<br></th>";
  }
  if($ival6 == true){//<!--中止商品表示-->
    echo "<th class=\"fixed_col\" width=\"70\">置換商品<br></th>";
  }
  //<!--th >特記事項1<br></th-->
  //<!--th >登録区分1<br></th-->
  //echo "<th class=\"fixed_col\" width=\"7px\">機種分類<br></th>";
  //if($ival_v3 == true){//海外フラグ
    if($viewtype != DEP_DOMESTIC  && $viewtype != DEP_JE  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1){
      echo "<th class=\"fixed_col international_must_items\" width=\"150\">英　名*<br></th>";
      echo "<th class=\"fixed_col international_items\" width=\"150\">英名注釈<br></th>";
    }
    //if($viewtype != DEP_DOMESTIC  && $viewtype != DEP_JE  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_JI)
    echo "<th class=\"fixed_col international_items\" width=\"71\">輸出価格<br></th>";
    if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_ABROAD_US)   
    echo "<th class=\"fixed_col international_items\" width=\"71\">US 卸<br></th>";
    if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_ABROAD_EU)
    echo "<th class=\"fixed_col international_items\" width=\"71\">EU 卸<br></th>";
  //}
  if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col_int_hide\" width=\"11\" style=\"display:none;\">国　内<br></th>";
  if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col_int_hide\" width=\"11\" style=\"display:none;\">サ<br>｜<br>ビス<br></th>";
  if($viewtype != DEP_DOMESTIC  && $viewtype != DEP_JI  && $viewtype != DEP_JE && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
  echo "<th class=\"fixed_col_int_hide\" width=\"11\" style=\"display:none;\">海　外<br></th>";
  //<!--<th >価格表区分1<br></th>-->
  //<!--<th >価格表区分2<br></th>-->
  //<!--<<th >小見出しカテゴリー<br></th>-->
  //<!--<<th >搬入料<br></th>-->
  //<!--<<th >メモ、要請欄<br></th>-->
  echo "</tr>";
  echo "</thead>";
  echo "<tbody>";
  $loopindex = 0;
  $lfix_col_num = 20;
  $today = date("Y/m/d");
  //$lconnect_num = 17;
  $lconnect_num = 13;//最小限必須項目(パーツセンタ対象空～工場卸価格)＋写真

  switch($viewtype){
    case DEP_MAINOFFICE:
    case DEP_ABROAD:
    case DEP_DOMESTIC:
    case DEP_JE:
      break;
    case DEP_OUTSIDECMP_D1: 
    case DEP_OUTSIDECMP_D2:
      $lconnect_num--;
    break;
    case DEP_JI: //＋英名　英名注釈　輸出価格、-卸１卸2工場卸
    break;
    case DEP_ABROAD_CH:
      $lconnect_num= $lconnect_num-2;//＋英名　英名注釈　-パーツセンタ対象、価格、卸１、工場卸
    break;
    case DEP_ABROAD_US:
    case DEP_ABROAD_EU:
    $lconnect_num = $lconnect_num-6;//＋英名　英名注釈 各卸　-パーツセンタ対象～工場卸価格　9
    break;
  }

  if($ival6 == true)//<!--中止商品表示--> 有効終了日、置換商品
    $lconnect_num = $lconnect_num+2;
  //if($ival_v3 == true){//海外向け商品　英名　英名注釈　輸出価格　US卸　EU卸
    switch($viewtype){
      case DEP_MAINOFFICE:
      case DEP_ABROAD:
        $lconnect_num = $lconnect_num+5;
      break;
      case DEP_DOMESTIC:
      case DEP_OUTSIDECMP_D1: 
      case DEP_OUTSIDECMP_D2: 
      case DEP_JE: 
      break;
      case DEP_JI: 
        $lconnect_num = $lconnect_num+3;//英名　英名注釈　輸出価格
      break;
      case DEP_ABROAD_CH:
        $lconnect_num = $lconnect_num+2;//英名　英名注釈
      break;
      case DEP_ABROAD_US:
      case DEP_ABROAD_EU:
      $lconnect_num = $lconnect_num+3;//英名　英名注釈 各卸
      break;
    }
  //}
  $lconnect_num_max = 0;
  $show_hide_add_row = 20;
    foreach($rows as $row){

      //表示数使わないため一旦コメントアウト
      // if($view_dataNum >0 && $row_count > $view_dataNum){//表示数
      //   if($loopindex < $view_dataNum*$tmp_dataNum_page || $loopindex >= $view_dataNum*($tmp_dataNum_page+1) )
      //   {
      //     $loopindex++;
      //     continue;
      //   }
      // }
      //<!--件数表示-->
      if($loopindex%$lfix_col_num==0){
        echo "<tr class=\"col_listnum\" id=\"col_listnum\" >";
        $lconnect_num_max = $loopindex+20;
        if($lconnect_num_max > $row_count)
          $lconnect_num_max= $row_count;
        echo "<td colspan=\"".$lconnect_num."\" rowspan=\"1\" style=\"background-color:#d9fcdd;\">【".($loopindex+1)."～".$lconnect_num_max."】</td>";
        echo "</tr>";
      }

      $col_class_name = "normal";
      if($row["end_date"] != 0 && strtotime($today) > strtotime($row["end_date"])){
        if($row["x_while_stocks_last"] == "TRUE"){
          $col_class_name = "col_limited_disable";
        }else{
          $col_class_name = "col_disable";
        }
      }
    //<!--行の開始-->
    echo "<tr class=\"$col_class_name\">";
    //<!--カートへ直接追加-->
    echo "<td nowrap width=\"10px\">";
    //<!--カートに追加(中止商品でないとき)-->
    if($row["end_date"] == 0 || strtotime($today) <= strtotime($row["end_date"])|| $row["x_while_stocks_last"] == "TRUE"){
      //$icountnow = get_jascoproc_count($row["code"]);
      //直接set()を呼ぶつくりだとものすごく描画に時間がかかったので苦肉の策で別phpに飛び別phpを自動で閉じる
      echo "<form action=\"direct_add_cart_n.php?List_CodeID=".$row["code"]."\" method=\"post\" enctype=\"multipart/form-data\"target=\"_blank\" rel=\"noopener noreferrer\">";
      //echo "<img src=\"image/cart.png\" width=\"30\" height=\"30\" alt=\"cart\" class=\"cart_button\" id=\"cart_button\">";
      echo "<input type=\"image\" src=\"image/cart.png\" width=\"20\" height=\"20\" alt=\"cart\" class=\"cart_button\" id=\"cart_button\">";
      echo "<input id=\"viewtype\" type=\"hidden\" value=\"$viewtype\">";
      echo "<input id=\"international_items\" type=\"hidden\" value=\"$ival_v3\">";
      echo "</form>";
    }
    echo "</td>";
    //<!--パーツセンタ対象-->
    if($viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {
      echo "<td width=\"7px\" class=\"col_center\">";
      if($row["parts_center"] =="TRUE")
        echo "○";
      echo "</td>";
    }
    //<!--写真-->
    echo "<td width=\"12px\" style='text-align:center;'>";
        //<!--サムネイルの導入によって表示件数が増大した時に展開が遅くなっています-->
        if(file_exists(filePath::$picture_path.$row["number_string"].".jpg")==true){
          echo "<div><a class=\"example-image-link\" href=\"".filePath::$picture_path.$row["number_string"].".jpg\" data-lightbox=\"cart_".$row["code"]."\" data-title=\"".$row["code"]."<br>".$row["proc_name"]."\"><img class=\"example-image\" src=\"".filePath::$picture_path.$row["number_string"].".jpg\" width=\"50\" alt=\" \" style=\"margin-top:5px;\"></a></div>";
          echo "</div>";
        }else if(file_exists(filePath::$picture_path.$row["code"].".jpg")==true){
          echo "<div><a class=\"example-image-link\" href=\"".filePath::$picture_path.$row["code"].".jpg\" data-lightbox=\"cart_".$row["code"]."\" data-title=\"".$row["code"]."<br>".$row["proc_name"]."\"><img class=\"example-image\" src=\"".filePath::$picture_path.$row["code"].".jpg\" width=\"50\" alt=\" \" style=\"margin-top:5px;\"></a></div>";
          echo "</div>";
        }
        echo "</td>";
    //<!--商品番号-->
    echo "<td style='white-space:nowrap;'>";
    echo "<a href = \"detail_list_window_n3.php?List_CodeID=".$row["code"]."\" style='display:inline;'>".$row["code"]."</a>";
    echo "<div style='display:inline;'>"." "."</div>";//少しスペース
    echo "<div class=\"btn_click_clipboard\" id=\"".$row["code"]."\" style='display:inline;cursor:pointer;'><img src=\"image/copy.png\" width=\"10\"></div>";//title=\"コピー\"
    echo "</td>";
    //<!--品目番号-->
    echo "<td style='white-space:nowrap;'style='display:inline;'>".$row["number_string"];
    echo "<div style='display:inline;'>"." "."</div>";//少しスペース
    if(mb_strlen($row["number_string"])>0)
    echo "<div class=\"btn_click_clipboard\" id=\"".$row["number_string"]."\" style='display:inline;cursor:pointer;'><img src=\"image/copy.png\" width=\"10\"></div>";// title=\"コピー\"
    echo "</td>";
    //<!--型式-->
    echo "<td>".$row["proc_type"]."</td>";
    //<!--商品名称-->
    echo "<td>".$row["proc_name"]."</td>";
    //<!--販売価格-->
    if("col_disable"==$col_class_name){//<!--中止行-->
        if($row["price_p"]!=0)
          echo "<td class=\"col_price\" width=\"12px\">".number_format($row["price_p"])."</td>";//グレー行の色反転を防ぐ
        else
          echo "<td class=\"col_price\" width=\"12px\"></td>";
    //}else if("col_limited_disable"==$col_class_name){
    //  echo "<td class=\"col_price3\" width=\"12px\">".number_format($row["price_p"])."</td>";//在庫限りの定価部分表示色を変更したもののイマイチ
    }else{
      if($row["price_p"]!=0)
        echo "<td class=\"col_price2\" width=\"12px\">".number_format($row["price_p"])."</td>";
      else
        echo "<td class=\"col_price2\" width=\"12px\"></td>";
    }
    //<!--規格-->
    if($viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU ){
    echo "<td style='width:10em;'>".$row["specification"]."</td>";
    //<!--仕様・備考-->
    echo "<td style='width:10em;'>".$row["notes"]."</td>";
    }
    //<!--卸1-->
    if($viewtype != DEP_ABROAD  && $viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {
      if($row["price_d1"]!=0)
        echo "<td class=\"col_price\" width=\"12px\">".number_format($row["price_d1"])."</td>";
      else
        echo "<td class=\"col_price\" width=\"12px\"></td>";
    }
    //<!--卸2-->
    if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {    
      if($row["price_d2"]!=0)
        echo "<td class=\"col_price\" width=\"12px\">".number_format($row["price_d2"])."</td>";
      else
        echo "<td class=\"col_price\" width=\"12px\"></td>";
    }
    //<!--工場卸-->
    if($viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_JI  && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
      echo "<td class=\"col_price\" width=\"12px\">".number_format($row["price_m"])."</td>";
    //<!--対応機種/用途-->
    echo "<td class=\"int_hiderow\" style=\"width:10em; display:none;\">".$row["compatible_models"]."</td>";
    //<!--有効終了日-->
    if($ival6 == true){//<!--中止商品表示-->
      //echo "<td>".$row["end_date"]."</td>";
      if(strtotime($row["end_date"]) > strtotime("1900/01/01")){$ENDDATE=$row["end_date"];}else{$ENDDATE="";}//仮に0埋めになっていても大丈夫なように
      echo "<td>".str_replace("-", "/", $ENDDATE). "</td>";// 表記修正　2022-01-01　→　2022/01/01 
    }
    //<!--置換商品-->
    if($ival6 == true){//<!--中止商品表示-->
      echo "<td width=\"5px\">".$row["substitutional_goods"]."</td>";
    }
    //<!--特記事項1-->
        //<!-- $row["precaution1"];-->
    //<!--登録区分1-->
        //<!--$row["class1"];-->
    //<!--機種分類-->
    //echo "<td width=\"7px\">".$row["instrument_type"]."</td>";
      //$tmp = $row["instrument_type"];
      /*if("01" == $tmp)$tmp ="01:FT/IR";//ほんとはswitchしたい..,
      if("02" == $tmp)$tmp ="NRS";
      if("03" == $tmp)$tmp ="UV";
      if("04" == $tmp)$tmp ="FP";
      if("05" == $tmp)$tmp ="DT";
      if("06" == $tmp)$tmp ="UVetc";
      if("07" == $tmp)$tmp ="J/P";
      if("08" == $tmp)$tmp ="NFS";
      if("09" == $tmp)$tmp ="LC";
      if("10" == $tmp)$tmp ="SCF";
      if("11" == $tmp)$tmp ="COM";
      if("12" == $tmp)$tmp ="BKK";
      if("13" == $tmp)$tmp ="LAMP";
    if("14" == $tmp)$tmp ="14:other";*/
   // echo "<td width=\"7px\">".$tmp."</td>";
    //<!--英名-->  
    //if($ival_v3 == true){//海外フラグ
      if($viewtype != DEP_DOMESTIC  && $viewtype != DEP_JE  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1){
        echo "<td class=\"international_must_items\" width=\"15px\">".$row["proc_name_eng"]."</td>";//<!--英名-->  
        echo "<td class=\"international_items\" width=\"15px\">".$row["eng_annotation"]."</td>";//<!--英名注釈--> 
      }   
      if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_JI)
      {
        //<!--輸出価格-->
        if($row["price_e"]!=0)
          echo "<td class=\"col_price international_items\" width=\"12px\">".number_format($row["price_e"])."</td>";
        else
          echo "<td class=\"col_price international_items\" width=\"12px\"></td>";
      } 
      if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_ABROAD_US)
      {
        //<!--US 卸-->
        if($row["price_d_us"]!=0)
          echo "<td class=\"col_price international_items\" width=\"12px\">".number_format($row["price_d_us"])."</td>";
        else
          echo "<td class=\"col_price international_items\" width=\"12px\"></td>";
      }
      if($viewtype == DEP_MAINOFFICE || $viewtype == DEP_ABROAD  || $viewtype == DEP_ABROAD_EU)
      {
        //<!--EU 卸-->
        if($row["price_d_eu"]!=0)
          echo "<td class=\"col_price international_items\" width=\"12px\">".number_format($row["price_d_eu"])."</td>";
        else
          echo "<td class=\"col_price international_items\" width=\"12px\"></td>";
      }
    //}
    //<!--価格表区分1-->
    if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {
      //echo "<td width=\"5px\">".$row["domestic_classification"]."</td>";
      $tmptext="";
      if($row["domestic_classification"] !="")
        $tmptext="○";
      echo "<td width=\"5px\" class=\"int_hiderow\" style=\"display:none;\">".$tmptext."</td>";
     }
    //<!--価格表区分2-->
    if($viewtype != DEP_JI  && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {
      //echo "<td width=\"5px\">".$row["service_classification"]."</td>";
      $tmptext="";
      if($row["service_classification"] !="")
        $tmptext="○";
      echo "<td width=\"5px\" class=\"int_hiderow\" style=\"display:none;\">".$tmptext."</td>";
    }
    //<!--価格表区分3-->
    if($viewtype != DEP_DOMESTIC  && $viewtype != DEP_JI  && $viewtype != DEP_JE && $viewtype != DEP_OUTSIDECMP_D2  && $viewtype != DEP_OUTSIDECMP_D1 && $viewtype != DEP_ABROAD_CH  && $viewtype != DEP_ABROAD_US  && $viewtype != DEP_ABROAD_EU )
    {
      //echo "<td width=\"5px\">".$row["overseas_classification"]."</td>";
      $tmptext="";
      if($row["overseas_classification"] !="")
        $tmptext="○";
      echo "<td width=\"5px\" class=\"int_hiderow\" style=\"display:none;\">".$tmptext."</td>";
    }
    echo "</tr>";
    $loopindex++;
    }
  echo "</tbody>";
  echo "</table>";  
  echo "</div>";
  require("./component/return.php");
  echo "<br>";
  echo "<script src=\"../lightbox2-2.11.2/dist/js/lightbox-plus-jquery.min.js\"></script>";
  echo "<script> lightbox.option({'resizeDuration': 10})</script>";
  echo "<script src=\"js/copy_to_clipboard.js\"></script>";
  echo "</body></html>";
  //ここまで************************************************************************************////
?>
