<?php
  //データベースへ接続設定
  require("./config/dbConnect.php");
  //echo "<script>alert(\"update_data.php\");</script>";
  try {
    $dbh = new PDO($dsn,$user,$password);
    $buffer1 ="SELECT CREATE_TIME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=\"pricelist\" AND TABLE_NAME=\"new_list_set_buffer\"";
    $statement = $dbh->prepare($buffer1);
    if($statement){
      if($statement->execute()){
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $create_table_date = $record["CREATE_TIME"];
        }
        $date = new DateTime($create_table_date);
        //echo $date->format('Y年m月d日');
        $buffer2 = $date->format('Y年m月d日');
        print("最終更新日：".$buffer2);
      }
      //データベース接続切断
      $dbh = null;
    }
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  } 
?>