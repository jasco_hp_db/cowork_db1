﻿<?php
  require_once("jasco_class_definition.php");
  header("Content-type: text/html; charset=utf-8");

  echo "<script>alert(\"edit_req_photo.php\");</script>";
  //送信データの取得
  $SearchWord = htmlspecialchars($_POST["List_CodeID"],ENT_QUOTES);//($_POST["List_ResultID"]
  $rq_count = htmlspecialchars($_POST["Update_ReqCount"],ENT_QUOTES);
  echo "<script>alert(\"". $SearchWord."\");</script>";
  echo "<script>alert(\"". $rq_count."\");</script>";

  //データベースへ接続設定
  require("./config/dbConnect.php");
  
  $tmpstr_JASCO_List_chCode = "";//商品番号
  $tmpstr_JASCO_List_chNumber_string = "";//品目番号

  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM new_list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    $buffer ="SELECT code FROM new_list_set_buffer";
    $statement = $dbh->prepare($buffer );
    if($statement){
      $row_count = $statement->rowCount();
    }
    if(!$statement || $row_count<1){
      /**********************************************/
      //写真リクエスト用テーブルの作成(import_table4で作っているはずだが消してしまった場合も想定)
      $statement = $dbh->prepare("CREATE TABLE request_photo_count(
      code varchar(32),
      number_string varchar(32),
      updatedate varchar(64),
      req_count varchar(32)
      )");
      if($statement)
        $statement->execute();
      /**********************************************/
    }

    $num_now = 0;
    $bexists = false;
    //$buffer ="SELECT code,number_string FROM new_list_set_buffer WHERE code = \"".$SearchWord."\"";
    $buffer ="SELECT code,number_string FROM new_list_set_buffer WHERE code = (:List_CodeID)";
    $statement = $dbh->prepare($buffer);
    if($statement){
      //プレースホルダへ実際の値を設定する
      $statement->bindValue(':List_CodeID', $SearchWord, PDO::PARAM_STR);
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        //echo "<script>alert(\"".$row_count."件。\")</script>";
        //検索結果を配列に詰める
        //$arrSearhedProducts= new ArrayObject();
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $tmpstr_JASCO_List_chCode = $record["code"];//商品番号
          $tmpstr_JASCO_List_chNumber_string = $record["number_string"];//品目番号
        }
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //echo "<script>alert(\"".$tmpstr_JASCO_List_chCode."。\")</script>";
      $buffer ="SELECT * FROM request_photo_count WHERE code = \"".$tmpstr_JASCO_List_chCode."\"";
      $statement2 = $dbh->prepare($buffer);
      if($statement2){
        //echo "<script>alert(\"statement2\")</script>";
        if($statement2->execute()){   
          // echo "<script>alert(\"statement2 true\")</script>";
          while($record2 = $statement2->fetch(PDO::FETCH_ASSOC)){
            $tmpstr_JASCO_List_chCode = $record2["code"];//商品番号
            $tmpstr_JASCO_List_chNumber_string = $record2["number_string"];//品目番号
            $tmpstr_JASCO_List_chUpdatedata = $record2["updatedate"];//更新日時
            $tmpstr_JASCO_List_chCount = $record2["req_count"];//カウント数
            $num_now = $tmpstr_JASCO_List_chCount;
            $bexists = true;
          }
        }
      }

      if($bexists==false){//今登録されていないcontorol_photo_req.phpからの呼び出しでは起こり得ない
        $buffer = "INSERT INTO request_photo_count (code, number_string, updatedate, req_count) 
        VALUES (:Req_code, :Req_number_string, :Req_updatedate, :Req_req_count)";
      }else{
        //$buffer = "UPDATE request_photo_count SET req_count = ".$rq_count.",updatedate = \"".date("Y/m/d H:i:s")."\" WHERE code = \"".$tmpstr_JASCO_List_chCode."\"";
        $buffer = "UPDATE request_photo_count SET req_count = :Req_code, updatedate = :Req_updatedate WHERE code = :Req_code";
      }
      $statement3 = $dbh->prepare($buffer);
      if($statement3){
        //if($bexists==false){
          $statement3->bindValue(':Req_code', $tmpstr_JASCO_List_chCode, PDO::PARAM_STR);
          if($bexists==false) $statement3->bindValue(':Req_number_string',  $tmpstr_JASCO_List_chNumber_string, PDO::PARAM_STR);
          $statement3->bindValue(':Req_updatedate', date("Y/m/d H:i:s"), PDO::PARAM_STR);
          $statement3->bindValue(':Req_req_count', $rq_count , PDO::PARAM_STR);
        //}
        if($statement3->execute()){
          //echo "処理2配列への挿入成功です。<br>";
          echo "<script>alert(\"".$tmpstr_JASCO_List_chCode.":".$num_now."回リクエストされています。\")</script>";
        }
      }
    }
    //データベース接続切断
    $dbh = null;
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
  echo "<script>window.close();</script>";
?>
