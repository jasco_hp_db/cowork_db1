<?php
    require("./config/loadEnv.php");
    require("./config/getSessionUserInfo.php");

    $userInfo = getSessionUserInfo();

    if (!$userInfo) {
        // ユーザー情報が取得できなかった場合の処理
        header("Location: login.php?alert=" . urlencode("ログインが必要です"));
        exit;
    }
    header("Content-type: text/html; charset=utf-8");
    
    //login**************************************************
    require("./config/section.php");
    $viewtype =  getsectiontype($userInfo['scode']);
    //echo "<br><br><br><br><br><br><br><br>表示：".$userInfo['name'].$viewtype."です。<br>";
    //viewtype**************************************************

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品検索</title>
	<link rel="stylesheet" type="text/css" href="css/style_flex2.css?230721">
	<link rel="stylesheet" type="text/css" href="css/checkbox.css?230224">
	<link rel="stylesheet" type="text/css" href="css/search_items.css?230727">
 	<link rel="stylesheet" href="css/under_bar_menu.css?230722">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
     //dummy data 更新日時取得
    $(function(){
      $(window).on('load', function() {
        $('#recent_update').text('更新中...');
        // dummy data
        var text1 = "blank1";
        var id = "blank2";
        $.ajax({
          url: 'update_data.php',
          type: 'POST',
          data: {
            List_CodeID: String(id),
            Update_ReqCount: text1,
          },
          //timeout: 5000,//50だと間に合わない
        }).done(function(data){
          $('#recent_update').html(data);
        /* 通信成功時 */
        }).fail(function(data){
          $('#recent_update').text('更新日時----');
        /* 通信失敗時 */
        }).always(function(data){
        /* 通信成功・失敗問わず */
        });
      });
    });
    function check_divitem_all() {
      var target = document.getElementById("div_00");
      var icheck_state = target.checked;
      target = document.getElementById("div_01");
      target.checked = icheck_state;
      target = document.getElementById("div_02");
      target.checked = icheck_state;
      target = document.getElementById("div_03");
      target.checked = icheck_state;
      target = document.getElementById("div_04");
      target.checked = icheck_state;
      target = document.getElementById("div_05");
      target.checked = icheck_state;
      target = document.getElementById("div_06");
      target.checked = icheck_state;
    }
    function check_viewitem_all() {
      var target = document.getElementById("price_00");
      var icheck_state = target.checked;
      target = document.getElementById("price_01");
      target.checked = icheck_state;
      target = document.getElementById("price_02");
      target.checked = icheck_state;
      target = document.getElementById("price_03");
      target.checked = icheck_state;
    }
    function setdefaultck() {
//alert("setdefaultck in");
      var target;
      var section_type = <?php echo $viewtype; ?>;
      const dev_mainoffice = <?php echo DEP_MAINOFFICE; ?>,
      dev_abroad = <?php echo DEP_ABROAD; ?>,
      dev_domestic = <?php echo DEP_DOMESTIC; ?>,
      dev_outsidecomp_d1 = <?php echo DEP_OUTSIDECMP_D1; ?>,
      dev_outsidecomp_d2 = <?php echo DEP_OUTSIDECMP_D2; ?>,
      dev_je = <?php echo DEP_JE; ?>,
      dev_ji = <?php echo DEP_JI; ?>,
      dev_abroad_ch = <?php echo DEP_ABROAD_CH; ?>,
      dev_abroad_us = <?php echo DEP_ABROAD_US; ?>;
      dev_abroad_eu = <?php echo DEP_ABROAD_EU; ?>;
//alert("section_type:"+dev_je);
      switch(section_type){
        case dev_mainoffice:
        case dev_je:
//alert("DEP_MAINOFFICE");
          target = document.getElementById("price_00");
          target.checked = false;
          target = document.getElementById("price_01");
          target.checked = true;
          target = document.getElementById("price_02");
          target.checked = true;
          target = document.getElementById("price_03");
          target.checked = false;
        break;                 
        case dev_domestic:
//alert("DEP_DOMESTIC");
          target = document.getElementById("price_00");
          target.checked = false;
          var target1 = document.getElementById("price_01");
          target1.checked = true;
          target = document.getElementById("price_02");
          target.checked = false;
          //target = document.getElementById("price_03");
          //target.checked = false;
        break;
        case dev_abroad:
//alert("DEP_ABROAD");
          target = document.getElementById("price_00");
          target.checked = true;
          target = document.getElementById("price_01");
          target.checked = true;
          target = document.getElementById("price_02");
          target.checked = true;
          target = document.getElementById("price_03");
          target.checked = true;
        break;
        case dev_outsidecomp_d1:
        case dev_outsidecomp_d2:
          target = document.getElementById("price_01");
          target.checked = true;
        break;
        case dev_ji:
        case dev_abroad_ch:
        case dev_abroad_us:
        case dev_abroad_eu:
          target = document.getElementById("price_03");
          target.checked = true;
        break;
      } 
    }
    function clear_all_info(){
//alert("clear_all_info");
      var target = document.getElementById("div_00");
      target.checked="checked";
      check_divitem_all();
//alert("check_divitem_all");
      //target = document.getElementById("price_00");
      //target.checked="checked";
      //target = document.getElementById("price_03");
      //target.checked=false;
      setdefaultck();
//alert("setdefaultck");
      //target = document.getElementById("div_06");
      //target.checked=false;
      var name = document.getElementsByName("input_searchWord");
      name[0].value = "";
      var name = document.getElementsByName("input_chModel");
      name[0].value = "";
      var name = document.getElementsByName("input_iInstrumentType");
      name[0].value = "";
      var name = document.getElementsByName("input_cCode");
      name[0].value = "";
      var name = document.getElementsByName("input_strNumber");
      name[0].value = "";
      var name = document.getElementsByName("input_strType");
      name[0].value = "";
      var name = document.getElementsByName("input_strName");
      name[0].value = "";
      var name = document.getElementsByName("input_strSpec");
      name[0].value = "";
      var name = document.getElementsByName("input_strNotes");
      name[0].value = "";
    }
    function add_or_string() {
      var name = document.getElementsByName("input_searchWord");
      var text1 = name[0].value +" @OR ";
      name[0].value = text1;
      name[0].focus();
    }
    function add_not_string() {
      var name = document.getElementsByName("input_searchWord");
      var text1 = name[0].value +" @NOT ";
      name[0].value = text1;
      name[0].focus();
    }
    function reset_string() {
      var name = document.getElementsByName("input_searchWord");
      name[0].value = "";
      name[0].focus();
    }
</script>
<body>  
  <header>
    <h1></h1><br><br>
    <div class = "grid_contents">
      <div class ="grid_item_01" style="width:120px;background-color:white;"> </div>
      <div class ="grid_item_02" style="background-color:white;">
        <p style="font-size:small;text-align:right;text-decoration:underline;"><?php echo $userInfo['name'] ?> さん
      </div>
   </div>
  </header>
  <div class="main" style="margin-top:-10px;">
    <form action="rslt_list_table_1109_n.php" method="post" >
     <div class ="XXX">
         <!--<button type="button" class="clear_button_flot" title="クリア"><img src="image/clear.png" width="30px"alt="クリア" onclick="clear_all_info()">クリア</button><br>-->
        <button type="submit" class="search_button_flot" title="検索"><img src="image/roope_gray.png" width="30px"alt="検索" >検索</button><br><br> 
      </div>
      <div class = "grid_contents">
        <div class ="grid_item_01" style="background-color:white;"></div>
        <div class ="grid_item_02" style="background-color:white;">
         <p class="style_updatedata" id="recent_update" style="margin-top:-50px;text-align:right;"></p>
        </div>
      </div>
      <div class = "grid_contents" style="margin-top:-40px;">
        <div class ="grid_item_01" style="font-size:120%;width:120px;">フリーワード<sup><p style="margin-top:5px;margin-bottom:0px;font-size:80%;">OR検索→@OR</p><p style="margin-top:0px;margin-bottom:0px;font-size:80%;">NOT検索→@NOT</p></sup></div>
        <div class ="grid_item_02">          
          <div class = "inner_grid_contents">
            <div class ="grid_item_01">
              <input type="text" class="wide_textbox_style" style="margin-top:-5px;margin-left:-35px;" name="input_searchWord" value="" placeholder="Enterキー入力で検索開始" >
            </div>
            <div class ="grid_item_02" >
              <button type="button" class="search_button" style="width:50px;margin-top:-5px;" title="OR" onclick="add_or_string()">OR</button>
              <button type="button" class="search_button" style="width:50px;margin-top:5px;margin-bottom:-5px;"title="NOT"onclick="add_not_string()">NOT</button>
            </div>
            <div>
          	<button type="submit" class="search_button" title="検索" style="width:120px;float:left;margin-top:-15px;font-size:18px;">検索</button>
          	<button type="button"class="search_button" title="クリア" onclick="clear_all_info()" style="float:left;margin-left:10px;margin-top:-15px;">クリア</button>
            </div>
          </div>
        </div>
      </div>
      <div class = "grid_contents">
          <div class ="grid_item_01" style="width:120px;"><p class="grid_letter" style="font-size:120%;">検索対象</p></div>
          <div class ="grid_style_left">
              <input type="checkbox" id="div_00" name="div_00" checked="checked"  onclick="check_divitem_all()">
              <label for="div_00" class="checkbox03" style="font-size:120%;">全選択</label>
              <input type="checkbox" id="div_01" name="div_01" checked="checked">
              <label for="div_01" class="checkbox02" style="font-size:120%;">本体</label>
              <input type="checkbox" id="div_02" name="div_02" checked="checked">
              <label for="div_02" class="checkbox02" style="font-size:120%;">付属品</label>
              <input type="checkbox" id="div_03" name="div_03" checked="checked">
              <label for="div_03" class="checkbox02" style="font-size:120%;">ソフトウェア</label>
              <input type="checkbox" id="div_04" name="div_04" checked="checked">
              <label for="div_04" class="checkbox02" style="font-size:120%;">消耗品</label>
              <input type="checkbox" id="div_05" name="div_05" checked="checked">
              <label for="div_05" class="checkbox02" style="font-size:120%;">その他(技術料,搬入料 他)</label>
              <input type="checkbox" id="div_06" name="div_06" checked="checked">&nbsp;&nbsp;
              <label for="div_06" class="checkbox02" style="font-size:120%;">中止商品</label>
          </div>
      </div>
      <div class = "grid_contents">
          <div class ="grid_item_01" style="width:120px;"><p class="grid_letter" style="font-size:120%;">表示項目</p></div>
          <div class ="grid_style_left">
              <?php
                switch($viewtype){
                  case DEP_MAINOFFICE:
                  echo "<input type=\"checkbox\" id=\"price_00\" name=\"price_00\" onclick=\"check_viewitem_all()\">";
                  echo "<label for=\"price_00\" class=\"checkbox03\" style=\"font-size:120%;\">全選択</label>";                  
                  echo "<input type=\"checkbox\" id=\"price_01\" name=\"price_01\" checked=\"checked\">";
                  echo "<label for=\"price_01\" class=\"checkbox02\" style=\"font-size:120%;\">国内向け商品</label>";
                  echo "<input type=\"checkbox\" id=\"price_02\" name=\"price_02\" checked=\"checked\">";
                  echo "<label for=\"price_02\" class=\"checkbox02\" style=\"font-size:120%;\">サービスパーツ</label>";
                  echo "<input type=\"checkbox\" id=\"price_03\" name=\"price_03\">";
                  echo "<label for=\"price_03\" class=\"checkbox02\" style=\"font-size:120%;\">海外向け商品</label>";
                  break;
                  case DEP_DOMESTIC:
                  echo "<input type=\"checkbox\" id=\"price_00\" name=\"price_00\" onclick=\"check_viewitem_all()\">";
                  echo "<label for=\"price_00\" class=\"checkbox03\" style=\"font-size:120%;\">全選択</label>";
                  echo "<input type=\"checkbox\" id=\"price_01\" name=\"price_01\" checked=\"checked\">";
                  echo "<label for=\"price_01\" class=\"checkbox02\" style=\"font-size:120%;\">国内向け商品</label>";
                  echo "<input type=\"checkbox\" id=\"price_02\" name=\"price_02\">";
                  echo "<label for=\"price_02\" class=\"checkbox02\" style=\"font-size:120%;\">サービスパーツ</label>";
                  break;
                  case DEP_JE:
                  echo "<input type=\"checkbox\" id=\"price_00\" name=\"price_00\" onclick=\"check_viewitem_all()\">";
                  echo "<label for=\"price_00\" class=\"checkbox03\" style=\"font-size:120%;\">全選択</label>";                  
                  echo "<input type=\"checkbox\" id=\"price_01\" name=\"price_01\" checked=\"checked\">";
                  echo "<label for=\"price_01\" class=\"checkbox02\" style=\"font-size:120%;\">国内向け商品</label>";
                  echo "<input type=\"checkbox\" id=\"price_02\" name=\"price_02\" checked=\"checked\">";
                  echo "<label for=\"price_02\" class=\"checkbox02\" style=\"font-size:120%;\">サービスパーツ</label>";
                  //echo "<input type=\"checkbox\" id=\"price_03\" name=\"price_03\">";
                 // echo "<label for=\"price_03\" class=\"checkbox02\" style=\"font-size:120%;\">海外向け商品</label>";
                  break;
                  case DEP_ABROAD:
                  echo "<input type=\"checkbox\" id=\"price_00\" name=\"price_00\" checked=\"checked\" onclick=\"check_viewitem_all()\">";
                  echo "<label for=\"price_00\" class=\"checkbox03\" style=\"font-size:120%;\">全選択</label>";
                  echo "<input type=\"checkbox\" id=\"price_01\" name=\"price_01\" checked=\"checked\">";
                  echo "<label for=\"price_01\" class=\"checkbox02\" style=\"font-size:120%;\">国内向け商品</label>";
                  echo "<input type=\"checkbox\" id=\"price_02\" name=\"price_02\"checked=\"checked\">";
                  echo "<label for=\"price_02\" class=\"checkbox02\" style=\"font-size:120%;\">サービスパーツ</label>";
                  echo "<input type=\"checkbox\" id=\"price_03\" name=\"price_03\" checked=\"checked\">";
                  echo "<label for=\"price_03\" class=\"checkbox02\" style=\"font-size:120%;\">海外向け商品</label>";
                  break;
                  case DEP_OUTSIDECMP_D1:
                  case DEP_OUTSIDECMP_D2:
                  echo "<input type=\"checkbox\" id=\"price_01\" name=\"price_01\" checked=\"checked\">";
                  echo "<label for=\"price_01\" class=\"checkbox02\" style=\"font-size:120%;\">国内向け商品</label>";
                  break;
                  case DEP_JI:
                  case DEP_ABROAD_CH:
                  case DEP_ABROAD_US:
                  case DEP_ABROAD_EU:
                  echo "<input type=\"checkbox\" id=\"price_03\" name=\"price_03\" checked=\"checked\">";
                  echo "<label for=\"price_03\" class=\"checkbox02\" style=\"font-size:120%;\">海外向け商品</label>";
                  break;
                }
              ?>
              <!--<input type="checkbox" id="price_00" name="price_00" checked="checked" onclick="check_viewitem_all()">
              <label for="price_00" class="checkbox03" style="font-size:120%;">全選択</label>
              <input type="checkbox" id="price_01" name="price_01" checked="checked">
              <label for="price_01" class="checkbox02" style="font-size:120%;">国内向け商品</label>
              <input type="checkbox" id="price_02" name="price_02" checked="checked">
              <label for="price_02" class="checkbox02" style="font-size:120%;">サービスパーツ</label>
              <input type="checkbox" id="price_03" name="price_03">
              <label for="price_03" class="checkbox02" style="font-size:120%;">海外向け商品</label>-->
          </div>
        </div>
      <!--input id=XXX type="submit"class="btn"value="検索"><br-->
      <!--details open-->
        <!--summary>詳細設定</summary-->
        <div class = "grid_contents_4row">
          <div class ="grid_item_01  multiple" style="font-size:120%;background-color:white;margin-bottom:-15px;"></div>
          <div class ="grid_item_02 multiple" style="text-align:left;background-color:white;margin-bottom:-15px;">
          <!--&nbsp;以下、複数ワード検索非対応です。-->
          </div>
          <div class ="grid_item_01  multiple" style="font-size:120%;background-color:white;margin-bottom:-15px;"></div>
          <div class ="grid_item_02  multiple" style="text-align:left;background-color:white;margin-bottom:-15px;">
          <!--&nbsp;以下、複数ワード検索非対応です。-->
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">機種分類<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <select name="input_iInstrumentType" class="grid_style_98par">
              <option value=""></option>
              <option value="01">FT/IR（赤外分光光度計）</option>
              <option value="02">NRS（レーザラマン分光光度計）</option>
              <option value="03">UV（紫外可視分光光度計）</option>
              <option value="04">FP（分光蛍光光度計）</option>
              <option value="05">DT（溶出試験器）</option>
              <option value="06">UVetc（UV・その他）</option>
              <option value="07">J/P（円二色性・旋光計）</option>
              <option value="08">NFS（近接場）</option>
              <option value="09">LC（高速液体クロマトグラフ）</option>
              <option value="10">SCF（超臨界流体機器）</option>
              <option value="11">COM（コンピュータ機器類）</option>
              <option value="12">BKK（分光計器）</option>
              <option value="13">LAMP（ランプ）</option>
              <option value="14">other（技術料・搬入料・その他）</option>
            </select>    
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">対応機種<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_chModel">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">商品番号<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_cCode">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">品目番号<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_strNumber">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">型　式<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_strType">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">商品名称<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_strName">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">規　格<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_strSpec">
          </div>
          <div class ="grid_item_01" style="font-size:120%;width:120px;">仕様 備考<!--sup> ※2</sup--></div>
          <div class ="grid_item_02">
            <input type="text" class="grid_style_98par" name="input_strNotes">
          </div>
        </div>
        </form>
   </div>
   <div style="width:630px; margin: 0 auto; margin-top:30px;" >
      <hr style="width:630px;">
      <p style="text-align:left;">
              　★お知らせ★<br>
              　新商品検索システム Ver.1.1をリリースしました。ログイン管理になりました。<br>
              　ご意見や不具合は  
              <a href="https://docs.google.com/forms/d/e/1FAIpQLSdJ9e_ScQ8UC7ZDBxh4mpNeTFXIOuo-_puKpvel658SF2Zy8A/viewform" target="new" rel="noopner">
                提案フォーム  
              </a>
                にお寄せください。更新履歴は 
              <a href="release_notes.html" target="new" rel="noopner">
                こちら。
              </a>
              <br>
              　表示崩れが起こる際は、ブラウザのキャッシュを消して、再読み込みしてください。　　　　　2024.08.01<br>
      </p>
      <hr style="width:630px;">
   </div>
   <div id="headerFloatingMenu">
	   <div class="header_layout">
		    <div>
		    	<a href="start_window.php">
		    		<img class="img_jascologo" src="image/jasco_logo2.png">
		    	</a>
		    </div>
		    <div>
	    	    	<img id="center_title" class="img_jascologo" src="image/kensaku2.png">
		    </div>
		    <div id="menu_button_top">
			    <a href="start_window.php" class="class_white_text">
			    	<img src="image/home.png" width="40" height="40" alt="home" title="ホーム" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="client_cart_list_n.php?direct=0" class="class_white_text">
			    	<img src="image/cart-top.png" width="40" height="40" alt="cart" title="カート" style="margin-top:5px;">
			    </a>&nbsp;&nbsp;&nbsp;
			    <a href="view_list_all.php" class="class_white_text" target="_blank" rel="noopener noreferrer">
			    	<img src="image/return_to_list.png" width="40" height="40" alt="pricelist" title="価格表" style="margin-top:5px;">
			    </a>
          <a href="javascript:void(0);" onclick="confirmLogout()">
	    	    <img src="image/logout.png" width="40" height="40" alt="logout" title="ログアウト" style="margin-top:5px;margin-right:5px;float:right;">
          </a>
		    </div>
	   </div>
   </div>
   <script>
      function confirmLogout() {
          if (confirm('ログアウトします。よろしいですか？')) {
              window.location.href = 'login.php';
          }
      }
  </script>
</body>
</html>