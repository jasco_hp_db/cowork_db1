﻿<?php
    require("./config/loadEnv.php");
    require("./config/getSessionUserInfo.php");

    $userInfo = getSessionUserInfo();

    if (!$userInfo) {
        // ユーザー情報が取得できなかった場合の処理
        header("Location: login.php?alert=" . urlencode("ログインが必要です"));
        exit;
    }
    header("Content-type: text/html; charset=utf-8");
    
    //login**************************************************
    require("./config/section.php");
    $viewtype =  getsectiontype($userInfo['scode']);
    //echo "<br><br><br><br><br><br><br><br>表示：".$userInfo['name'].$viewtype."です。<br>";
    //viewtype**************************************************

?>

<?php
  //require_once("jasco_class_definition.php");
  header("Content-type: text/html; charset=utf-8");

  //送信データの取得
  $SearchWord = htmlspecialchars($_GET["List_CodeID"],ENT_QUOTES);//($_POST["List_ResultID"]
  //echo "検索ID:".$SearchWord."です。<br>";  
  //echo "<script>alert(\"".$SearchWord."リクエストしました。\")</script>";

  //データベースへ接続設定
  require("./config/dbConnect.php");
  
  $tmpstr_JASCO_List_chCode = "";//商品番号
  $tmpstr_JASCO_List_chNumber_string = "";//品目番号
  $tmpstr_JASCO_List_chUpdatedata = "";//更新日時
  $tmpstr_JASCO_List_chCount = "";//カウント数

  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM new_list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    $buffer ="SELECT code FROM new_list_set_buffer";
    $statement = $dbh->prepare($buffer );
    if($statement){
      $row_count = $statement->rowCount();
    }

    if(!$statement || $row_count<1){
      /**********************************************
      //写真リクエスト用テーブルの作成(import_table4で作っているはずだが消してしまった場合も想定)
      $statement = $dbh->prepare("CREATE TABLE request_photo_count(
      code varchar(32),
      number_string varchar(32),
      updatedate varchar(64),
      req_count varchar(32)
      )");
      if($statement){
        $statement->execute();
      }
      /**********************************************/
    }

    $num_now = 0;
    $bexists = false;
    //$buffer ="SELECT code,number_string FROM new_list_set_buffer WHERE code = \"".$SearchWord."\"";
    $buffer ="SELECT code,number_string FROM new_list_set_buffer WHERE code = (:List_CodeID)";
    $statement = $dbh->prepare($buffer );
    if($statement){
      //プレースホルダへ実際の値を設定する
      $statement->bindValue(':List_CodeID', $SearchWord, PDO::PARAM_STR);
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        //echo "<script>alert(\"".$row_count."件。\")</script>";
        //検索結果を配列に詰める
        //$arrSearhedProducts= new ArrayObject();
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $tmpstr_JASCO_List_chCode = $record["code"];//商品番号
          $tmpstr_JASCO_List_chNumber_string = $record["number_string"];//品目番号
        }
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //echo "<script>alert(\"".$tmpstr_JASCO_List_chCode."。\")</script>";
      $buffer ="SELECT * FROM request_photo_count WHERE code = (:List_CodeID)";
      $statement2 = $dbh->prepare($buffer);
      if($statement2){
        $statement2->bindValue(':List_CodeID', $tmpstr_JASCO_List_chCode, PDO::PARAM_STR);
        //echo "<script>alert(\"statement2\")</script>";
        if($statement2->execute()){   
         // echo "<script>alert(\"statement2 true\")</script>";
          while($record2 = $statement2->fetch(PDO::FETCH_ASSOC)){
            $tmpstr_JASCO_List_chCode = $record2["code"];//商品番号
            $tmpstr_JASCO_List_chNumber_string = $record2["number_string"];//品目番号
            $tmpstr_JASCO_List_chUpdatedata = $record2["updatedate"];//更新日時
            $tmpstr_JASCO_List_chCount = $record2["req_count"];//カウント数
            $tmpstr_JASCO_List_chID = $record2["req_id"];//要求者
            $num_now = $tmpstr_JASCO_List_chCount;
            $bexists = true;
          }
        }
      }
      $num_now++;//カウントアップ

      //新規テーブル準備
      if($bexists==true){
        $buffer = "UPDATE request_photo_count SET req_count = :Leq_req_count, updatedate = :Leq_updatedate, req_id = :Leq_req_id WHERE code = :Leq_code";
      }
      else{
        $buffer = "INSERT INTO request_photo_count (code, number_string, updatedate, req_count, req_id, is_active) VALUES (:Leq_code, :Leq_number_string, :Leq_updatedate, :Leq_req_count, :Leq_req_id, :Leq_is_active)";
      }
      
      $statement3 = $dbh->prepare($buffer);
      
      if($statement3){

          if($bexists==true){
            $statement3->bindValue(':Leq_code', $tmpstr_JASCO_List_chCode, PDO::PARAM_STR);
            $statement3->bindValue(':Leq_updatedate', date("Y/m/d H:i:s"), PDO::PARAM_STR);
            $statement3->bindValue(':Leq_req_count', $num_now, PDO::PARAM_STR);
            $statement3->bindValue(':Leq_req_id', $tmpstr_JASCO_List_chID . '_' . $userInfo['idno'], PDO::PARAM_STR);
          }
          else{
            $statement3->bindValue(':Leq_code', $tmpstr_JASCO_List_chCode, PDO::PARAM_STR);
            $statement3->bindValue(':Leq_number_string',  $tmpstr_JASCO_List_chNumber_string, PDO::PARAM_STR);
            $statement3->bindValue(':Leq_updatedate', date("Y/m/d H:i:s"), PDO::PARAM_STR);
            $statement3->bindValue(':Leq_req_count', $num_now, PDO::PARAM_STR);
            $statement3->bindValue(':Leq_req_id', $userInfo['idno'], PDO::PARAM_STR);
            $statement3->bindValue(':Leq_is_active', 1, PDO::PARAM_STR);
          }

          if($statement3->execute()){
            echo "<script>alert(\"商品番号：".$tmpstr_JASCO_List_chCode." の写真リクエストを受け付けました。\")</script>";
          }
        }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
  echo "<script>window.close();</script>";
?>
