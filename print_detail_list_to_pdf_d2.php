﻿<?php
  //require_once("jasco_class_definition.php");
  header("Content-type: text/html; charset=utf-8");
  //写真フォルダパスの設定
  require("./config/filePath.php");

  //login***************************************************
  require("./config/loadEnv.php");
  require("./config/getSessionUserInfo.php");

  $userInfo = getSessionUserInfo();

  if (!$userInfo) {
      // ユーザー情報が取得できなかった場合の処理
      header("Location: login.php?alert=" . urlencode("ログインが必要です"));
      exit;
  }
  //login**************************************************
  require("./config/section.php");
  $viewtype =  getsectiontype($userInfo['scode']);
  //viewtype**************************************************

  //送信データの取得
  $SearchWord = htmlspecialchars($_GET["List_ResultID"],ENT_QUOTES);
  //echo "検索ID:".$SearchWord."です。<br>";  

  //データベースへ接続
  require("./config/dbConnect.php");
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    //SQLの実行 
    //$statement = $dbh->prepare("SELECT * FROM list WHERE ProcName LIKE (:ProcName) ");
    //あいまい検索で全項目を対象に検索
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer WHERE code LIKE (:ProcName) ");
    if($statement){
      $like_SearchWord = "%".$SearchWord."%";
      //プレースホルダへ実際の値を設定する
      $statement->bindValue(':ProcName', $SearchWord, PDO::PARAM_STR);
      echo "検索ワード:".$like_SearchWord."です。<br>";
      if($statement->execute()){
        //レコード件数取得
        $row_count = $statement->rowCount();
        //検索結果を配列に詰める
        //$arrSearhedProducts= new ArrayObject();
        
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $rows[] = $record;
        }
        echo "検索結果:".$row_count."件です。<br>";
      }else{
        $errors['error'] = "検索失敗しました。";
      }
      //データベース接続切断
      $dbh = null;	
    }        
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  } 
  //データベースへ接続(セッションへ保存できるようになれば不要)
  //properties

  $tmpstr_JASCO_List_chCode = "";//商品番号
  $tmpstr_JASCO_List_chNumber_string = "";//品目番号
  $tmpstr_JASCO_List_chProc_type = "";//型式
  $tmpstr_JASCO_List_chProc_name = "";//商品名称
  $tmpstr_JASCO_List_iPrice_p = "";//販売価格
  $tmpstr_JASCO_List_iPrice_d1 = "";//卸1
  $tmpstr_JASCO_List_iPrice_d2 = "";//卸2
  $tmpstr_JASCO_List_x_iPrice_d3 = "";//卸3
  $tmpstr_JASCO_List_iPrice_m = "";//工場卸
  $tmpstr_JASCO_List_x_iGroup = "";//販売ｸﾞﾙｰﾌﾟ
  $tmpstr_JASCO_List_chSpecification = "";//規格
  $tmpstr_JASCO_List_chNotes = "";//仕様・備考→（規格に入りきらない物）
  $tmpstr_JASCO_List_x_chSite = "";//ｻｲﾄ
  $tmpstr_JASCO_List_x_iStock_pc = "";//引当可能在庫数（PC）
  $tmpstr_JASCO_List_chDomestic_classification = "";//価格表区分1→価格表区分（国内）　営業価格表、価格表、その他（特注、キャンペーン、顧客専用品他）
  $tmpstr_JASCO_List_chService_classification = "";//価格表区分2→価格表区分（サービス）
  $tmpstr_JASCO_List_chOverseas_classification = "";//価格表区分3→価格表区分（輸出）
  $tmpstr_JASCO_List_chEnd_date = "";//有効終了日
  $tmpstr_JASCO_List_x_chWhile_stocks_laste = "";//在庫限り
  $tmpstr_JASCO_List_chParts_centere = "";//パーツセンタ対象
  $tmpstr_JASCO_List_x_chValid = "";//有効品目
  $tmpstr_JASCO_List_chSubstitutional_goods = "";//置換商品
  $tmpstr_JASCO_List_x_chDiscontinuance = "";//商品中止理由
  $tmpstr_JASCO_List_chPrecaution1 = "";//特記事項1
  $tmpstr_JASCO_List_chClass1 = "";//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他）
  $tmpstr_JASCO_List_chProc_name_eng = "";//英名
  $tmpstr_JASCO_List_chPicture = "";//写真
  $tmpstr_JASCO_List_chInstrument_type = "";//機種分類 01:FT、02:ラマン、03:UV、04:FP、05:DT、06:UVETC、07:JP、08:NFS、09:LC、10:SCF、11:COM、12:計器、13:ランプ、14:搬入費他
  $tmpstr_JASCO_List_chCompatible_models = "";//対応機種/用途
  //$tmpstr_JASCO_List_chShift1 = "";//価格表区分1 
  //$tmpstr_JASCO_List_chShift2 = "";//価格表区分2
  //$tmpstr_JASCO_List_chCategory = "";//小見出しカテゴリー
  $tmpstr_JASCO_List_chEmplacement_cost = "";//搬入料(大型、中型、小型）
  //$tmpstr_JASCO_List_chMemo = "";//メモ、要請欄

  foreach($rows as $row){ 
    if(isset($row["code"]))
      $tmpstr_JASCO_List_chCode = $row["code"];//商品番号
    if(isset($row["number_string"]))
      $tmpstr_JASCO_List_chNumber_string = $row["number_string"];//品目番号
    if(isset($row["proc_type"]))
      $tmpstr_JASCO_List_chProc_type = $row["proc_type"];//型式
    if(isset($row["proc_name"]))
      $tmpstr_JASCO_List_chProc_name = $row["proc_name"];//商品名称
    if(isset($row["price_p"]))
      $tmpstr_JASCO_List_iPrice_p = $row["price_p"];//定価
    if(isset($row["price_d1"]))
      $tmpstr_JASCO_List_iPrice_d1 = $row["price_d1"];//卸1
    if(isset($row["price_d2"]))
      $tmpstr_JASCO_List_iPrice_d2 = $row["price_d2"];//卸2
    if(isset($row["price_m"]))
      $tmpstr_JASCO_List_iPrice_m = $row["price_m"];//工場卸
    if(isset($row["specification"]))
      $tmpstr_JASCO_List_chSpecification = $row["specification"];//規格
    if(isset($row["notes"]))
      $tmpstr_JASCO_List_chNotes = $row["notes"];//仕様・備考
    if(isset($row["domestic_classification"]))
      $tmpstr_JASCO_List_chDomestic_classification = $row["domestic_classification"];//価格表区分（国内）    
    if(isset($row["service_classification"]))
      $tmpstr_JASCO_List_chService_classification = $row["service_classification"];//価格表区分（サービス）
    if(isset($row["overseas_classification"]))
      $tmpstr_JASCO_List_chOverseas_classification = $row["overseas_classification"];//価格表区分3→価格表区分（輸出）
    if(isset($row["end_date"]))
      $tmpstr_JASCO_List_chEnd_date = $row["end_date"];//有効終了日
    if(isset($row["x_while_stocks_last"]))
      $tmpstr_JASCO_List_x_chWhile_stocks_laste = $row["x_while_stocks_last"];//在庫限り
    if(isset($row["parts_center"]))
      $tmpstr_JASCO_List_chParts_centere = $row["parts_center"];//パーツセンタ対象   
    if(isset($row["substitutional_goods"]))
      $tmpstr_JASCO_List_chSubstitutional_goods = $row["substitutional_goods"];//置換商品
    if(isset($row["precaution1"]))
      $tmpstr_JASCO_List_chPrecaution1 = $row["precaution1"];//特記事項1
    if(isset($row["class1"]))
      $tmpstr_JASCO_List_chClass1 = $row["class1"];//登録区分1 本体、付属品、消耗品、ソフトウェア、（その他） 
    if(isset($row["proc_name_eng"]))
      $tmpstr_JASCO_List_chProc_name_eng = $row["proc_name_eng"];//英名
    if(isset($row["instrument_type"]))
      $tmpstr_JASCO_List_chInstrument_type = $row["instrument_type"];//機種分類    
    if(isset($row["compatible_models"]))
      $tmpstr_JASCO_List_chCompatible_models = $row["compatible_models"];//対応機種/用途

    //$tmpstr_JASCO_Product_linkPicture = "image/data/".$tmpstr_JASCO_List_chNumber_string.".jpg";//$row["Picture"];//画像
    $tmpstr_JASCO_Product_linkPicture = filePath::$picture_path.$tmpstr_JASCO_List_chNumber_string.".jpg";//$row["Picture"];//画像
    $tmpstr_JASCO_Product_linkPicture2 = filePath::$picture_path.$tmpstr_JASCO_List_chCode.".jpg";//$row["Picture"];//画像

    $tmpstr_JASCO_Product_dPriceP_view = number_format($tmpstr_JASCO_List_iPrice_p);//定価3桁区切り
  }
  $tmpstr_JASCO_List_rec_timestamp = time();//date("Y/m/d H:i:s");/strtotime("now")

  //定価
  if($tmpstr_JASCO_List_iPrice_p!=0)
  $tmpstr_JASCO_Product_dPriceP_view = number_format($tmpstr_JASCO_List_iPrice_p);
  else
  $tmpstr_JASCO_Product_dPriceP_view ="";
  //卸1
  if($tmpstr_JASCO_List_iPrice_d1!=0)
    $tmpstr_JASCO_List_dPriceD1_view = number_format($tmpstr_JASCO_List_iPrice_d1);
  else
    $tmpstr_JASCO_List_dPriceD1_view ="";
  //卸2
  if($tmpstr_JASCO_List_iPrice_d2!=0)
    $tmpstr_JASCO_List_dPriceD2_view = number_format($tmpstr_JASCO_List_iPrice_d2);
  else
    $tmpstr_JASCO_List_dPriceD2_view ="";
  //工場卸 
  $tmpstr_JASCO_List_dPriceM_view = number_format($tmpstr_JASCO_List_iPrice_m);

  $today = date("Y/m/d");
?>

<?php
require_once("../TCPDF/tcpdf.php");
$tcpdf = new TCPDF("Portrait");
//$tcpdf->setPrintHeader(FALSE);//横線消える
$tcpdf->setPrintFooter(FALSE);
$tcpdf->SetMargins(10,10,10);
$tcpdf->SetY(0);
$tcpdf->SetAutoPageBreak(true, 0);//これを設定しないとFooter部分に空白ができる

$tcpdf->AddPage();
$tcpdf->SetFont("kozminproregular", "", 10);
////
  //$image_file_h =  "image/jasco_logo.png"; 
  //$tcpdf->setHeaderData($image_file_h,10,"","");
  $image_file = "image/footer_all.jpg"; 
  $tcpdf->Image($image_file, 0, 252, 210, "", "jpg", "", "", false, 300, "C", false, false, 0, false, false, false);//252 フッター画像Y位置　210画像幅
  // Position at 15 mm from bottom 
  // Page number
  // Logo 
////
if(file_exists($tmpstr_JASCO_Product_linkPicture)==false){
  if(file_exists($tmpstr_JASCO_Product_linkPicture2)==false){
    $tmpstr_JASCO_Product_linkPicture = "";
  }else{//code.jpg名で商品写真がある
    $tmpstr_JASCO_Product_linkPicture = $tmpstr_JASCO_Product_linkPicture2;
  }
}
$tmpstr_Caution = "";
if($tmpstr_JASCO_List_chEnd_date != 0  && $tmpstr_JASCO_List_chEnd_date != "" && strtotime($today) > strtotime($tmpstr_JASCO_List_chEnd_date)){
  if($tmpstr_JASCO_List_x_chWhile_stocks_laste != "TRUE")
    $tmpstr_Caution = "※この商品は中止商品です。";
  else
    $tmpstr_Caution = "※この商品は在庫限りで販売終了となります。";
}
//html content
$html =  <<< EOF
<style>
    table {
      border: 1px #ffa500; 
      table-layout: fixed;
    }
    table th {
    background-color: #eee;
    }
    th.col_title {
      width: 20%;
    }
    td.col_contents {
      width: 80%;
    }
    div.caution {
      color:red;
      font-size:large;
    }
    img.pic1 {
      text-align:center;
      margin: 0 auto;
      width: 300px;
    }
</style>
<h1>{$tmpstr_JASCO_List_chProc_name}</h1>

<img><!--理由不明だがここに<img>を挟むとPictureが下の表にかかる動作が解消される-->
<!--中止商品警告-->
<div style="text-align: center;">
<img class="pic1" src="{$tmpstr_JASCO_Product_linkPicture}">
</div>
<br>
<div class = "caution">{$tmpstr_Caution}</div>
<table border="1"  cellspacing="0" cellpadding="5" bordercolor="#ffa500">
    <tr>
        <th class="col_title">商品番号</th>
        <td class="col_contents">{$tmpstr_JASCO_List_chCode}</td>
    </tr>
    <tr>
        <th class="col_title">型　式</th>
        <td class="col_contents">{$tmpstr_JASCO_List_chProc_type}</td>
    </tr>
    <tr>
        <th class="col_title">商品名称</th>
        <td class="col_contents">{$tmpstr_JASCO_List_chProc_name}</td>
    </tr>
    <tr>
        <th class="col_title">規　格</th>
        <td class="col_contents">{$tmpstr_JASCO_List_chSpecification}</td>
    </tr>
    <tr>
        <th class="col_title">販売価格(税抜き)</th>
        <td class="col_contents">{$tmpstr_JASCO_Product_dPriceP_view}円</td>
    </tr>
    <tr>
        <th class="col_title">貴社卸(税抜き)</th>
        <td class="col_contents">{$tmpstr_JASCO_List_dPriceD2_view}円</td>
    </tr>
</table>
EOF;
/*
<tr>
<th class="col_title">品目番号</th>
<td class="col_contents">{$tmpstr_JASCO_List_chNumber_string}</td>
</tr>
<tr>
    <th class="col_title">対応機種/用途</th>
    <td class="col_contents">{$tmpstr_JASCO_List_chCompatible_models}</td>
</tr>*/
//仕様・備考は営業企画の意向で削除
//<tr>
//<th class="col_title">仕様・備考</th>
//<td class="col_contents">{$tmpstr_JASCO_List_chNotes}</td>
//</tr>
//// CodeIgniterのviewの第三引数をtrueにしてhtml文字列として扱う
//$html = $this->load->view('open_position', $this->data, true);

$tcpdf->writeHTML($html);
ob_end_clean();// 出力用バッファの内容を消去
$tcpdf->Output("resut_".$tmpstr_JASCO_List_chCode.".pdf");
//$tcpdf->Output("test.pdf", 'D');//ソフトウェアで表示ないしダウンロード
?>