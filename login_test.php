<?php
    require("./config/loadEnv.php");
    require("./config/dbConnectT.php");

    // 特注DBに接続
    $db = dbConnectT();

    // SQLクエリの実行
    $stmt = $db->query("SELECT * FROM UserAccountDB WHERE session = '$sessionValue'");
    
    while ($row = $stmt->fetch()) {
        $name = $row['name'];
        $idno = $row['idno'];
        $sectionCode = $row['scode'];
        $authority = $row['authority'];
        $email = $row['email'];
        $managerID = $row['managerId'];
        $session = $row['session'];
    }

    header("Content-type: text/html; charset=utf-8");

?>

<!DOCTYPE html>
<html lang="ja">
<head>
<style>
/* テーブルのスタイル */
table {
    width: 40%;
    border-collapse: collapse;
}

/* テーブルヘッダーのスタイル */
table th {
    background-color: #4CAF50; /* 緑色 */
    color: white; /* 文字色は白 */
    padding: 10px 15px;
    text-align: left;
}

/* 奇数行のスタイル */
table tr:nth-child(odd) {
    background-color: #f2f2f2; /* 薄いグレー */
}

/* 偶数行のスタイル */
table tr:nth-child(even) {
    background-color: #e7e7e7; /* グレー */
}

/* セルのスタイル */
table td {
    padding: 8px 15px;
    border: 1px solid #ddd; /* 薄いグレーの境界線 */
}
</style>
</head>
<body>  
  <header>
  <div class="header-logo-menu">
    <div id="nav-drawer">
        <label id="nav-open" for="nav-input"><span></span></label>
        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
        <div id="nav-content">
          
          <div class="guide_menu">
            <table style="margin:30px; float:center;">
                <tr><td>name: </td><td><?php echo $name ?></td></tr>
                <tr><td>idno: </td><td><?php echo $idno ?></td></tr>
                <tr><td>section code: </td><td><?php echo $sectionCode ?></td></tr>
                <tr><td>authority: </td><td><?php echo $authority ?></td></tr>
                <tr><td>E-mail: </td><td><?php echo $email ?></td></tr>
                <tr><td>manager ID: </td><td><?php echo $managerID ?></td></tr>
                <tr><td>session: </td><td><?php echo $session ?></td></tr>
            </table>
          </div>

        </div>
    </div>
  </div>
  </header>
  <div class="main">
  </div>
</body>
</html>