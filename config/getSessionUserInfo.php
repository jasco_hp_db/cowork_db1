<?php
function getSessionUserInfo() {
    // ユーザー情報を格納するための配列
    $userInfo = [];

    // セッションIDの取得
    $sid = isset($_COOKIE["CGISESSID"]) ? $_COOKIE["CGISESSID"] : null;

    // セッションIDが空ならnullを返す
    if (empty($sid)) {
        return null;
    }

    // データベース接続
    require './config/dbConnectT.php';
    $db = dbConnectT();

    if (!empty($sid)) {
        // データベースでセッションを確認
        $sql = "SELECT account, password, name, idno, scode, email, authority, managerId, session, scApproved FROM UserAccountDB WHERE session = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute([$sid]);
        $userInfo = $stmt->fetch(PDO::FETCH_ASSOC);

        // ユーザー情報が取得できない場合はnullを返す
        if (!$userInfo) {
            return null;
        }

        // idnoのゼロパディング（必要に応じて）
        $userInfo['idno'] = str_pad($userInfo['idno'], 4, '0', STR_PAD_LEFT);

        // ユーザー名の空白削除
        $userInfo['name'] = str_replace(' ', '', $userInfo['name']);
    }

    return $userInfo;
}

?>