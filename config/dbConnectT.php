<?php
    // データベース接続関数 (dbConnect() のPHP版)
    // この関数の中身は、実際のデータベースの設定に合わせて記述する必要があります。
    function dbConnectT() {
        // ドメインURL
        $DOMEIN = $_SERVER['HTTP_HOST']; 

        // .envファイルをロード、本番環境ではアクセスできない所からロードする
        if ($DOMEIN === "www.jasco.co.jp") {
            loadEnv('/temp/.env');
        }
        else{
            loadEnv('.env');            
        }

        $host = 'localhost';
        $db   = 'gtest';
        $user = 'root';
        $pass = $_ENV['DB_PASSWORD'];
        $charset = 'utf8mb4';
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        return new PDO($dsn, $user, $pass, $options);
    }

    // Cookieからセッション情報を取得
    if (isset($_COOKIE['CGISESSID'])) {
        $sessionValue = $_COOKIE['CGISESSID'];
    }