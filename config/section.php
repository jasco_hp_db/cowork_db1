<?php
/*部門分類*/
define('DEP_MAINOFFICE', 1);/*本社*/
define('DEP_ABROAD', 2);/*海外事業部*/
define('DEP_DOMESTIC', 3);/*SC*/
define('DEP_OUTSIDECMP_D1', 4);/*国内卸1代理店*/
define('DEP_OUTSIDECMP_D2', 5);/*国内卸2代理店*/
define('DEP_JE', 6);/*ジャスコエンジ*/
define('DEP_JI', 7);/*ジャスコインター*/
define('DEP_ABROAD_CH', 8);/*海外代理店(China)*/
define('DEP_ABROAD_US', 9);/*海外代理店(US)*/
define('DEP_ABROAD_EU', 10);/*海外代理店(EU)*/

function getsectiontype($section_code) {

    $return_value = -1;

    /*本社*/
    if($section_code>=200 && $section_code<=409){
        $return_value = DEP_MAINOFFICE;
    }
    if($section_code == 489 || $section_code == 500 ){/*489 営業部部長付 500 ジャスコテクノ*/
        $return_value = DEP_MAINOFFICE;
    }

    /*SC*/
    if($section_code>=412 && $section_code<=477){
        $return_value = DEP_DOMESTIC;
    }
    if($section_code == 491){/*491 神奈川SC*/ 
        $return_value = DEP_DOMESTIC;
    }
    if($section_code>=501 && $section_code<=506){/*ジャスコサポート各SC*/
        $return_value = DEP_DOMESTIC;
    }
    /*海外営業部*/
    if($section_code>=480 && $section_code<=485){
        $return_value = DEP_ABROAD;
    }

    /*dummy id*/
    if($section_code ==9990)
        $return_value = DEP_JE;
    if($section_code ==9991)
        $return_value = DEP_OUTSIDECMP_D1;
    if($section_code ==9992)
        $return_value = DEP_OUTSIDECMP_D2;
    if($section_code ==9993)
        $return_value = DEP_JI;
    if($section_code ==9994)
        $return_value = DEP_ABROAD_CH;
    if($section_code ==9995)
        $return_value = DEP_ABROAD_US;
    if($section_code ==9996)
        $return_value = DEP_ABROAD_EU;

    return $return_value;
}