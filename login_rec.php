<?php
    require("./config/loadEnv.php");
    require("./config/dbConnectT.php");

    // セッションパスワード生成関数
    function generate_password($length = 35) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $password;
    }

    // パラメータ取得
    $user_name = $_POST['user_name'];
    $password = $_POST['password'];

    // 特注DBに接続
    $db = dbConnectT();
    $stmt = $db->prepare("SELECT account, password, name, scode FROM UserAccountDB WHERE (account = ? AND is_active = 1)");
    $stmt->execute([$user_name]);
    $row = $stmt->fetch();
    $db_username = $row['account'];
    $db_password = $row['password'];
    $db_name = $row['name'];
    $db_scode = $row['scode'];

    // ユーザー名とパスワードのチェック
    $flag = 0;
    if(!empty($db_username) && $user_name == $db_username && $password == $db_password) {
        $flag = 1;

        $sid = generate_password();

        // クッキー設定
        $domain = $_SERVER['HTTP_HOST'];
        $path = ($domain == "www.jasco.co.jp") ? "/intranet/sales/" : "/";
        $secure = ($domain == "www.jasco.co.jp") ? true : false;
        setcookie("CGISESSID", 
            $sid, [
            'expires' => time() + 21600, // 6 hours
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure, // HTTPS経由でのみクッキーを送信
            'httponly' => true, // JavaScriptからのアクセスを防ぐ
            'samesite' => 'Lax' // SameSite属性をLaxに設定
            ]
        );

        // DB処理（セッションの保存）
        $stmt2 = $db->prepare("UPDATE UserAccountDB SET session=? WHERE account=?");
        $stmt2->execute([$sid, $user_name]);
    }

    header('Content-Type: text/html; charset=utf-8');
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<title>ログイン認証</title>
</head>
<body <?php if($flag) echo "onload=\"window.location.href = 'start_window.php';\""; ?>>
<?php
if($flag) {
    echo "ただいま、ユーザー認証中です<br>";
} else {
    echo "ユーザー名またはパスワードが違います";
}
?>
</body>
</html>