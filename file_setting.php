<?php
    require("./config/loadEnv.php");
    require("./config/getSessionUserInfo.php");

    $userInfo = getSessionUserInfo();

    // 開発者と営業管理室に編集権限付与
    if($userInfo['idno'] == "0811" || $userInfo['idno'] == "0858" || $userInfo['scode'] == 404 || $userInfo['scode'] == 401)
    {
      $Admin = 1;
    }
    else{
      $Admin = 0;     
    }

    if ($Admin == 0) {
        // ユーザー情報が取得できなかった場合の処理
        header("Location: login.php?alert=" . urlencode("ログインが必要です"));
        exit;
    }
    header("Content-type: text/html; charset=utf-8");
    
    //login**************************************************
    require("./config/section.php");
    $viewtype =  getsectiontype($userInfo['scode']);
    //echo "<br><br><br><br><br><br><br><br>表示：".$userInfo['name'].$viewtype."です。<br>";
    //viewtype**************************************************

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/style_flex.css">
  <title>データベース登録</title>
</head>
<body>  
  <header>
    <h1>データベース登録</h1>
  </header>
  <div class="main">
    <h2>商品検索用データベース登録</h2>
    <div class = "grid_contents">
        <div class ="grid_item_01">
          <label>アップロード</label>
        </div>
        <div class ="grid_item_02" >
          <form action="import_table_all_step1.php" method="post" enctype="multipart/form-data">
            IFSデータ　※ファイル名は英数のみ(日本語不可)かつUTF-8：<input type="file" name="fname1"><br>
            <!--Excel補足情報　※ファイル名は英数のみ(日本語不可)かつUTF-8：<input type="file" name="fname2"><br>-->
            <input type="submit" value="アップロード">
          </form>
          <form action="import_table2.php" method="post" enctype="multipart/form-data">
            Excel補足情報　※ファイル名は英数のみ(日本語不可)かつUTF-8：<input type="file" name="fname2"><br>
            <input type="submit" value="アップロード">
          </form>
        </div>
        <!-- <div class ="grid_item_03">
          <form action="export_table1.php?file_name=4_result.txt & database_name=new_list_set_buffer" method="post" enctype="multipart/form-data" target="_blank" rel="noopener noreferrer" >
            <input type="submit" value="ダウンロード">
          </form>
        </div> -->
      <div class ="grid_item_01">
        <label>連結前の処理：</label>
      </div>
      <div class ="grid_item_02">
        <form action="import_table3.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="前処理">
        </form>
      </div>
      <!-- <div class ="grid_item_03">
        <form action="export_table1.php?file_name=3_ifs_base_forcomb.txt & database_name=new_list_set_buffer_proc_sort" method="post" enctype="multipart/form-data"target="_blank" rel="noopener noreferrer" >
          <input type="submit" value="ダウンロード">
        </form>
      </div> -->
      <div class ="grid_item_01">
        <label>連結：</label>
      </div>
      <div class ="grid_item_02">
        <form action="import_table4.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="連結実行">
        </form>
      </div>
      <div class ="grid_item_01">
        <label>バックアップと復元：</label>
      </div>
      <div class ="grid_item_02">
        <form action="backup_table.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="バックアップ">
        </form>
        <form action="restore_table.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="復元">
        </form>
      </div>
      <!-- <div class ="grid_item_03">
        <form action="export_table1.php?file_name=4_result.txt & database_name=new_list_set_buffer" method="post" enctype="multipart/form-data"target="_blank" rel="noopener noreferrer" >
          <input type="submit" value="ダウンロード">
        </form>
      </div> -->
    </div><br>
    <h2>価格表用データベース登録</h2>
    <div class = "grid_contents">
      <div class ="grid_item_01">
        <label>価格表(ALL)：</label>
      </div>
      <div class ="grid_item_02" >
        <form action="import_index_auto.php?index_type=h_list" method="post" enctype="multipart/form-data">
          <input type="file" name="fname">
          <input type="submit" value="アップロード">
        </form>
      </div>
      <div class ="grid_item_01">
        <label>バックアップと復元：</label>
      </div>
      <div class ="grid_item_02">
        <form action="backup_table_h_list.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="バックアップ">
        </form>
        <form action="restore_table_h_list.php" method="post" enctype="multipart/form-data">
          <input type="submit" value="復元">
        </form>
      </div>
    </div><!-- <br>
    <h2>商品登録連絡用データベース登録</h2>
    <div class = "grid_contents">
      <div class ="grid_item_01">
        <label>商品登録連絡ファイル(ADD)：</label>
      </div>
      <div class ="grid_item_02" >
        <form action="import_index_auto.php?index_type=h_list" method="post" enctype="multipart/form-data">
          <input type="file" name="fname">
          <input type="submit" value="アップロード">
        </form>
      </div>
    </div> -->
  </div>
</body>
</html>