﻿<?php
  //pricelist本体の作成
  header("Content-type: text/html; charset=utf-8");
  //データベースへ接続設定
  require("./config/dbConnect.php");
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    if($dbh){
      /**********************************************/
      //言語設定
      echo "言語設定<br>";
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("SET character_set_database=utf8");
      if($statement){
        if($statement->execute())
          echo "言語設定成功です。<br>";
        else
          echo "言語設定失敗しました。<br>";
      }else{
        echo "言語設定失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
      /**********************************************/
      $buffer_string = "SHOW DATABASES LIKE 'pricelist'";
      $statement = $dbh->prepare($buffer_string);
      if($statement){
        if($statement->execute()){
          //レコード件数取得
          $row_count = $statement->rowCount();
          echo "SHOW DATABASES成功".$row_count."件です。<br>";
          if ($row_count)
          {
            echo "存在している！<br>";
          }else{
            //存在していないので$db_nameを作成
            $buffer_string2 = "CREATE DATABASE pricelist CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
            $statement2 = $dbh->prepare($buffer_string2);
            if($statement2){
              if($statement2->execute()){
                echo "データベース作成成功！<br>";
              }
            }
          }
        }else{
          echo "SHOW DATABASES失敗しました。<br>";
        }
      }
    }
    $dbh = null;
    echo('データベース存在確認完了！<br>');
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }
 //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
?>
