
<?php
// CSVファイルに書き込む配列を定義します。
$ary = array(
    array("名前", "年齢", "血液型"),
    array("太郎", "21", "O"),
    array("ジョン", "23", "A"),
    array("ニキータ", "32", "AB"),
    array("次郎", "22", "B")
   );
  // ファイルを書き込み用に開きます。
  $f = fopen("test2.csv", "w");
  // 正常にファイルを開くことができていれば、書き込みます。
  if ( $f ) {
    // $ary から順番に配列を呼び出して書き込みます。
    foreach($ary as $line){
       //mb_convert_variables('SJIS-win', 'UTF-8', $line);//行ずつ変換なので速度的には遅い//保存はSJIS
      // fputcsv関数でファイルに書き込みます。
      fputcsv($f, $line);
    } 
  }
  // ファイルを閉じます。
  fclose($f);
?>
<?php


function create_csv($data, $save_path)
{
  //一時データを開く
  $fp = fopen('php://temp', 'r+b');
 
  //fputcsvでCSVデータを作る
  foreach ($data as $val) {
    fputcsv($fp, $val);
  }
 
  //ファイルポインタを先頭に戻す
  rewind($fp);
 
  //ストリームの中身をテキストデータに変換、
  //さらにテキストデータをUTF-8からSJIS-winに変換する
  $str = str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));
  $str = mb_convert_encoding($str, 'SJIS-win', 'UTF-8');
 
  //一時データのファイルポインタを閉じる
  fclose($fp);
 
  //CSVファイルを生成して、データを書き込んで保存する
  $fp2 = fopen($save_path, "w");
  fwrite($fp2, $str);
  fclose($fp2);
}
?>