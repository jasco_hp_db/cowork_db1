//変数storageにlocalStorageを格納//sessionstorageでも入れ替え可
var storage = localStorage;
var arJasco_Product = {
  //properties
  JASCO_Product_lNumber:"null",//DB登録番号
  JASCO_Product_strNumber:"null",//品目番号
  JASCO_Product_strType:"null",//型式
  JASCO_Product_strName:"null",//商品名称
  JASCO_Product_dPriceP:"null",//定価
  JASCO_Product_dPriceM:"null",//工場卸
  JASCO_Product_dPriceD3:"null",//卸3
  JASCO_Product_dPriceD2:"null",//卸2
  JASCO_Product_dPriceD1:"null",//卸1
  JASCO_Product_cCode:"null",//商品番号
  JASCO_Product_iInstrumentType:"null",//機種分類
  JASCO_Product_strDescription:"null",//内容
  JASCO_Product_strRemarks:"null",//備考
  JASCO_Product_iSupplier:"null",//価格表分類　0：営業　1：エンジ
  JASCO_Product_linkDetails:"null",//詳細
  JASCO_Product_linkPicture:"null",//画像
  //sales infos
  JASCO_Product_iSalesCount:"null",
  //time stamp
  JASCO_Product_rec_timestamp:"0"//登録時刻
}
//データをクリアする
function cle() {
  var myRet = confirm("カートを空にします。よろしいですか？");
  if(myRet == true ){
    storage.clear();
    show_result();
  }
}
//引数秒以上前に登録した結果をクリアする
function cle_olditem(int_seconds) {
  var date = new Date();
  var nowtimemm = date.getTime();
  var nowtime = Math.floor( nowtimemm / 1000 ) ;
  var bdelete_first = 0;
//alert("date:"+date+",nowtime:"+nowtime+",bdelete_first:"+bdelete_first);
  for(var i=storage.length-1; i>=0; i--){    
    var k = storage.key(i);
    arJasco_Product = JSON.parse(storage.getItem(k));
    if(nowtime - arJasco_Product.JASCO_Product_rec_timestamp > int_seconds){
//alert("Name:"+arJasco_Product.JASCO_Product_strName+"nowtime:"+nowtime+",arJasco_Product.JASCO_Product_rec_timestamp:"+arJasco_Product.JASCO_Product_rec_timestamp+",int_seconds:"+int_seconds);      
      if(bdelete_first < 1){ //bdelete_first=0の時だけ質問。  
        var myRet = confirm("追加後、24時間を経過した商品を削除します。よろしいですか？");
        if(myRet == true ){
          bdelete_first = 1;
        }else{
          bdelete_first = 2;
        }
      }
      if(bdelete_first == 1){
        alert("削除-k:"+k);
         storage.removeItem(k);
      }
    }
  }
}
function addCommma(tmp_value){
  var tmpstring = String(tmp_value).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  return tmpstring;
}
function check_fileexist(tmp_value){
//javascript でファイルの有無は見えないらしい（セキュリティ的に）
  var filepath = new air.File(tmp_value);
  if(filepath.exists == true){
    alert("指定された場所にsample.txtファイルはあります");
    return true;
  }else{
    alert("指定された場所にsample.txtファイルはありません!!");
    return false;
  }
}
//カウントアップダウン兼ねる
function replace_jascoproc_count(strProcNumber, iCount){//increment decrement兼ねる
  //保存されているデータの数だけループ
  for(var i=0; i<storage.length; i++){    
    var k = storage.key(i);
    var arJasco_Product_tmp = JSON.parse(storage.getItem(k));
    if(arJasco_Product_tmp.JASCO_Product_strNumber == strProcNumber){
      if(arJasco_Product_tmp.JASCO_Product_iSalesCount == iCount)
        break;
      if(iCount<1){
        var Ret = window.confirm(arJasco_Product_tmp.JASCO_Product_strName+"を削除します。よろしいですか？");
        if(Ret == false)
           break;
        storage.removeItem(k);
        break;
      }else{
        arJasco_Product_tmp.JASCO_Product_iSalesCount = iCount;
        storage.setItem(k, JSON.stringify(arJasco_Product_tmp));
        break;
      }
    }
  }
}
/*個数操作の表示*/
function getText(varaltmpidx) {
  var string_temp = "combofield_"+varaltmpidx;
  var obj = document.getElementById(string_temp);
  var index = obj.tfield2.value;

  var k = storage.key(varaltmpidx);
  var arJasco_Product_tmp = JSON.parse(storage.getItem(k));
  replace_jascoproc_count(arJasco_Product_tmp.JASCO_Product_strNumber,index);
  show_result();
}
function getSelect(varaltmpidx) {
  var string_temp = "combofield_"+varaltmpidx;
  var obj = document.getElementById(string_temp);
  var index = obj.selbox2.selectedIndex;

  var k = storage.key(varaltmpidx);
  var arJasco_Product_tmp = JSON.parse(storage.getItem(k));
  replace_jascoproc_count(arJasco_Product_tmp.JASCO_Product_strNumber,index); //JASCO_Product_strNumberで渡すべきかどうか…
  show_result();
}
//保存されているデータをリスト表示する
function show_result() {
  var resultTable = "";
  //タイトル部
  resultTable+="<table><thead><tr>";
  resultTable+="<th class=xxx>個数<br></th>";
  resultTable+="<th class=xxx>価格表分類<br></th>";
  resultTable+="<th class=xxx>機種分類<br></th>";
  resultTable+="<th class=xxx>商品番号<br></th>";
  resultTable+="<th class=xxx>品目番号<br></th>";
  resultTable+="<th class=xxx>型式<br></th>";
  resultTable+="<th class=xxx>商品名称<br></th>";
  resultTable+="<th class=xxx>単価<br></th>";
  resultTable+="<th class=xxx>販売価格(税抜)<br></th>";
  resultTable+="<th class=xxx>内容<br></th>";
  resultTable+="<th class=xxx>備考<br></th>";
  resultTable+="<th class=xxx>画像<br></th>";
  resultTable+="</tr></thead>";
  resultTable+="<tbody>";
  var sum = 0;

  //Key名でソートされているのでtimestampで配列を並べ替えて表示(予定)⇒getSelect,getTextにも影響するので注意

  //保存されているデータの数だけループ
  for(var i=0; i<storage.length; i++){    
    var k = storage.key(i);
    arJasco_Product = JSON.parse(storage.getItem(k));
    //resultTable+="<tbody><td class=XXX>"+ arJasco_Product.JASCO_Product_iSalesCount +"</td>";
    resultTable+="<tr><td class=XXX><form id=\"combofield_"+i+"\">";
    resultTable+="<select name=\"select\" class=\"selectBox\" id=\"selbox2\" onChange=\"getSelect("+i+")\">";
    resultTable+="<option value=\"0\">0</option>";
    resultTable+="<option value=\"1\""; if(arJasco_Product.JASCO_Product_iSalesCount==1)resultTable+="selected";resultTable+=">1</option>";
    resultTable+="<option value=\"2\""; if(arJasco_Product.JASCO_Product_iSalesCount==2)resultTable+="selected";resultTable+=">2</option>";
    resultTable+="<option value=\"3\""; if(arJasco_Product.JASCO_Product_iSalesCount==3)resultTable+="selected";resultTable+=">3</option>";
    resultTable+="<option value=\"4\""; if(arJasco_Product.JASCO_Product_iSalesCount==4)resultTable+="selected";resultTable+=">4</option>";
    resultTable+="<option value=\"5\""; if(arJasco_Product.JASCO_Product_iSalesCount==5)resultTable+="selected";resultTable+=">5</option>";
    resultTable+="<option value=\"6\""; if(arJasco_Product.JASCO_Product_iSalesCount==6)resultTable+="selected";resultTable+=">6</option>";
    resultTable+="<option value=\"7\""; if(arJasco_Product.JASCO_Product_iSalesCount==7)resultTable+="selected";resultTable+=">7</option>";
    resultTable+="<option value=\"8\""; if(arJasco_Product.JASCO_Product_iSalesCount==8)resultTable+="selected";resultTable+=">8</option>";
    resultTable+="<option value=\"9\""; if(arJasco_Product.JASCO_Product_iSalesCount==9)resultTable+="selected";resultTable+=">9</option>";
    resultTable+="<option value=\"10\""; if(arJasco_Product.JASCO_Product_iSalesCount>9)resultTable+="selected";resultTable+=">10+</option>";
    resultTable+="</select>";
    if(arJasco_Product.JASCO_Product_iSalesCount <= 9){
      resultTable+="<input style=\"display:none\" name=\"textfield\" type=\"tel\" class=\"textfield\" id=\"tfield2\" onChange=\"getText("+i+")\" value=\""+arJasco_Product.JASCO_Product_iSalesCount+"\">"; 
    }else{
      resultTable+="<input style=\"display:block\" name=\"textfield\" type=\"tel\" class=\"textfield\" id=\"tfield2\" onChange=\"getText("+i+")\" value=\""+arJasco_Product.JASCO_Product_iSalesCount+"\">"; 
    }
    resultTable+="</form></td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_iSupplier +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_iInstrumentType +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_cCode +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_strNumber +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_strType +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_strName +"</td>";
    resultTable+="<td class=\"col_price\">"+ addCommma(arJasco_Product.JASCO_Product_dPriceP)+"</td>";//単価
    resultTable+="<td class=\"col_price\">"+ addCommma(parseInt(arJasco_Product.JASCO_Product_dPriceP)*parseInt(arJasco_Product.JASCO_Product_iSalesCount)) +"</td>";//販売価格
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_strDescription +"</td>";
    resultTable+="<td class=XXX>"+ arJasco_Product.JASCO_Product_strRemarks +"</td>";
    resultTable+="<td class=XXX>";
    resultTable+="<div class=\"fullscreen-image\" data-image=\""+arJasco_Product.JASCO_Product_linkPicture+"\" data-title=\""+arJasco_Product.JASCO_Product_strNumber+"\" data-caption=\""+arJasco_Product.JASCO_Product_strName+"\">";
    resultTable+="<img src=\""+arJasco_Product.JASCO_Product_linkPicture+"\" width=\"50\" alt=\" \">";
    resultTable+="</div></td></tr>";
    sum += parseInt(arJasco_Product.JASCO_Product_dPriceP)*parseInt(arJasco_Product.JASCO_Product_iSalesCount);
    //sum += parseInt(arJasco_Product.JASCO_Product_dPriceP);
  }
  resultTable+="</tbody>";
  resultTable+="<tfoot><tr>";
  resultTable+="<td colspan=\"12\"align=\"right\">"+"合計(税抜)：" + addCommma(sum) + "円です。</td>";
  resultTable+="</tfoot></tr></table>";
  //上のループで作成されたテキストを表示する
  document.getElementById("show_result_field").innerHTML = resultTable;
  //</body>の直前画像拡大用
  var elements = document.querySelectorAll( '.fullscreen-image' );
  Intense( elements );
}
//初期Load
function init(){
  //cle_olditem(86400);//86400 sec＝1day
  cle_olditem(60);//test
  show_result();
}