var storage = localStorage;
var arJasco_Product = {
  //properties
  JASCO_Product_lNumber:"null",//DB登録番号
  JASCO_Product_strNumber:"null",//品目番号
  JASCO_Product_strType:"null",//型式
  JASCO_Product_strName:"null",//商品名称
  JASCO_Product_dPriceP:"null",//定価
  JASCO_Product_dPriceM:"null",//工場卸
  JASCO_Product_dPriceD3:"null",//卸3
  JASCO_Product_dPriceD2:"null",//卸2
  JASCO_Product_dPriceD1:"null",//卸1
  JASCO_Product_cCode:"null",//商品番号
  JASCO_Product_iInstrumentType:"null",//機種分類
  JASCO_Product_strDescription:"null",//内容
  JASCO_Product_strRemarks:"null",//備考
  JASCO_Product_iSupplier:"null",//価格表分類　0：営業　1：エンジ
  JASCO_Product_linkDetails:"null",//詳細
  JASCO_Product_linkPicture:"null",//画像
  //sales infos
  JASCO_Product_iSalesCount:"null",
  //time stamp
  JASCO_Product_rec_timestamp:"0"//登録時刻
}

function sheetInit() {
    var numitems = localStorage.key("num_items");
    if(!numitems)numitems = 6;
    for (var i=0; i < numitems; i++) {
        var row = document.querySelector("table").insertRow(-1);
        for (var j=0; j<13; j++) {//12項目数＋1
            var letter = String.fromCharCode("A".charCodeAt(0)+j-1);
            row.insertCell(-1).innerHTML = i&&j ? "<input id='"+ letter+i +"'/>" : i||letter;
        }
    }
    var DATA={}, INPUTS=[].slice.call(document.querySelectorAll("input"));
    INPUTS.forEach(function(elm) {
        elm.onfocus = function(e) {
            e.target.value = localStorage[e.target.id] || "";
        };
        elm.onblur = function(e) {
            localStorage[e.target.id] = e.target.value;
            computeAll();
        };
        var getter = function() {
            var value = localStorage[elm.id] || "";
            if (value.charAt(0) == "=") {
                with (DATA) return eval(value.substring(1));
            } else { return isNaN(parseFloat(value)) ? value : parseFloat(value); }
        };
        Object.defineProperty(DATA, elm.id, {get:getter});
        Object.defineProperty(DATA, elm.id.toLowerCase(), {get:getter});
    });
    (window.computeAll = function() {
        INPUTS.forEach(function(elm) { try { elm.value = DATA[elm.id]; } catch(e) {} });
    })();
}
function setsheetvalue(tmp_row, tmp_column){

}
function loadjascocartinfo(){
    //title
    document.querySelector("table").rows[0].cells[0].innerHTML ="";
    document.querySelector("table").rows[0].cells[1].innerHTML ="品目番号";
    document.querySelector("table").rows[0].cells[2].innerHTML ="商品番号";
    document.querySelector("table").rows[0].cells[3].innerHTML ="商品名称";
    document.querySelector("table").rows[0].cells[4].innerHTML ="価格表分類";
    document.querySelector("table").rows[0].cells[5].innerHTML ="機種分類";
    document.querySelector("table").rows[0].cells[6].innerHTML ="型式";
    document.querySelector("table").rows[0].cells[7].innerHTML ="単価";
    document.querySelector("table").rows[0].cells[8].innerHTML ="個数";
    document.querySelector("table").rows[0].cells[9].innerHTML ="販売価格(税別)";
    document.querySelector("table").rows[0].cells[10].innerHTML ="内容";
    document.querySelector("table").rows[0].cells[11].innerHTML ="備考";
    document.querySelector("table").rows[0].cells[12].innerHTML ="画像";

    var numitems = localStorage.key("num_items");
    for(var i=0; i<numitems; i++){
    }
}
document.addEventListener("DOMContentLoaded", function() {
    // 実行したい処理
    sheetInit();
    loadjascocartinfo();
});