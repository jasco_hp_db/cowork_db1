<?php
  header("Content-type: text/html; charset=utf-8");
  require("./config/dbConnect.php");
  require("./component/list_all_header.php");

  $current_page = 0;
  if( isset( $_GET['current_page']))
  $current_page =$_GET['current_page'];
  if($current_page<0)
    $current_page = 0;//0スタート
  //echo $current_page ;
 
  $page_count = 0;
  //データベースへ接続
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！
    /**********************************************/
    //言語設定
    //echo "言語設定<br>";
    $start = microtime(true);
    //echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      $statement->execute();
      //if($statement->execute())
        //echo "言語設定成功です。<br>";
      //else
       // echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    //$end = microtime(true);
    //echo "終了:".$end."です。<br>";
    //$sec = ($end - $start);
    //echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    //更新日時の確認
    $buffer1 ="SELECT CREATE_TIME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=\"pricelist\" AND TABLE_NAME=\"h_list_result\"";
    $buffer2 = "-----";
    $create_table_date = null;
    $statement = $dbh->prepare($buffer1);
    if($statement){
      if($statement->execute()){
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          $create_table_date = $record["CREATE_TIME"];
        }
        $date = new DateTime($create_table_date);
        $buffer2 = $date->format('Y年m月d日');
        //echo "<div>最終更新日：".$buffer2."</div>";
      }
    }
    /**********************************************/
    //ページ割開始
    //echo "ページ割開始<br>";
    //echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("SELECT * FROM h_list_result");//ページ数カウント
    if($statement){
      if($statement->execute()){
        $list_count = $statement->rowCount();//リスト件数カウント
        //echo $list_count."件です<br>";
        if($list_count>0){
          $dbh->beginTransaction();//必須
          while($record = $statement->fetch(PDO::FETCH_ASSOC)){
            $rows0[] = $record;
          }
          $dbh->commit();//必須
        }
      }else{
        echo "ページ割開始 失敗<br>";
      }
    }else{
      echo "ページ割開始　失敗<br>";
    }
    
    $page_count = 0;
    $menu_count_num=0;
    $tmp_instrument_type_name="";
    $vec_page_rows = array('-1'=>'0');
    $vec_page_title = array('-1'=>'---');
    $vec_page_instrument_type = array('-1'=>'---');
    $loopindex = 0;
    foreach($rows0 as $row0){
      //echo $row0["h_list_title_or_code"]."<br>";
      if("0" ==$row0["h_list_row"]){
        if($page_count > 0)
          $vec_page_rows[$page_count-1] = $loopindex;//loopindex:1ページ分ずれている
        if($tmp_instrument_type_name!=$row0["h_list_instrument_type_name"]){
          $menu_count_num=0;
          $tmp_instrument_type_name = $row0["h_list_instrument_type_name"];
        }
        //$vec_page_title[$page_count] = $row0["h_list_instrument_type_name"]."-".$row0["h_list_page_index"];//IR-10などの表示
        $vec_page_title[$page_count] = $row0["h_list_title_or_code"]."(".($menu_count_num+1).")";//各ページタイトルの活用
        $vec_page_instrument_type[$page_count]  = $row0["h_list_instrument_type_name"];
        //echo $vec_page_title[$page_count].":".$page_count.":".$loopindex.":".$vec_page_rows[$page_count-1]."<br>";
        //echo $vec_page_instrument_type[$page_count]."<br>";
        $page_count++;
        $menu_count_num++;
        $loopindex = 0;
      }
      $loopindex++;
    }
    if($page_count > 0){//最後のページ分
      $vec_page_rows[$page_count-1]=$loopindex;
    }
    /**********************************************/
    //商品検索へのリンク
    echo"<div style=\"margin:0 auto;width:1240px;\">";
    echo"<a href=\"start_window.html\"><img class=\"img_jascologo\" src=\"./image/jasco_logo2.png\" alt=\"home\" title=\"商品検索へ戻る\" style=\"margin:0 auto;\"></a>";
    echo"</div>";
    /**********************************************/
    //表示用データの収集
    $current_page = 0;
    for($j = 0; $j < $page_count;$j++){
      $current_page = $j;
      $sum_rows = 0;
      $rows = [];//初期化しないと
      for($i = 0; $i < $j;$i++){
        $sum_rows += $vec_page_rows[$i];
      }
      //echo "sum_rows:".$sum_rows."です。<br>";
      //$sort_buffer = "SELECT * FROM h_list_result LIMIT ".$vec_page_rows[$current_page+1]." OFFSET ".$sum_rows;//h_list_resultに思っている順番で入らない
      $sort_buffer = "SELECT * FROM h_list_result LIMIT ".$vec_page_rows[$j]." OFFSET ".$sum_rows;//TEST
      //echo "sort_buffer:".$sort_buffer."です。<br>";
      $statement = $dbh->prepare($sort_buffer);//ページ数カウント
      //echo "buffer_string:".$sort_buffer."です。<br>";
      if($statement){
        if($statement->execute()){
          $list_count = $statement->rowCount();//リスト件数カウント
          if($list_count>0){
            //echo $list_count."件です";
            //echo "ページ割開始 成功<br>";
            $dbh->beginTransaction();//必須
            while($record = $statement->fetch(PDO::FETCH_ASSOC)){
              $rows[] = $record;
            }
            $dbh->commit();//必須
          }
        }else{
          //echo "ページ割開始 失敗<br>";
        }
      }else{
        //echo "ページ割開始　失敗<br>";
      }
      //ここからshow_result()***********************************************************************
      $page_title = "";
      foreach($rows as $row){
        $page_title = $row["h_list_title_or_code"];
        break;
      }
      //echo "<h1></h1><br><br><br><br>";
      //echo "<h1>価格表</h1><br>";
      
      /*if($j>0){
        $tmp =$j-1;
        echo "<br>";
        echo"<a href=\"#".$tmp."\">Back</a>";
      }*/
      echo "<div id=\"".$j."\" style=\"margin:20px 0px;\"></div><br><br>";
      echo "<div style=\"margin:0 auto;width:1240px;\">";
      if($j == 0){
        echo "<div style=\"font:28px bold;text-align:center;margin-top:-50px;margin-bottom:50px;\">価格表</div>";
        echo "<div style=\"text-align:right;margin-top:-50px;margin-bottom:0px;\">最終更新日：".$buffer2."</div>";//最終更新日：の追加
      }
      echo "<h2 style=\"margin:0px 0px;\">".$page_title."</h2><br>";
      echo "<table class=\"price\" style=\"margin:-15px 0px;width:1240px;\">";
      echo "<thead>";
      echo "<tr>";
      echo "<th class=xxx width=\"7%\" style=\"background:#d9fcdd;\">商品番号*</th>";//商品番号
      echo "<th class=xxx width=\"7%\" style=\"background:#d9fcdd;\">品目番号*</th>";//品目番号
      echo "<th class=xxx width=\"11%\" style=\"background:#d9fcdd;\">型　式*</th>";//型式
      echo "<th class=xxx width=\"19%\" style=\"background:#d9fcdd;\">商品名称*</th>";//商品名
      echo "<th class=xxx width=\"6%\" style=\"background:#d9fcdd;\">販売価格</th>";//販売価格
      echo "<th class=xxx width=\"15%\" style=\"background:#d9fcdd;\">規　格*</th>";//規格
      echo "<th class=xxx width=\"17%\" style=\"background:#d9fcdd;\">仕様 備考</th>";//仕様・備考
      echo "<th class=xxx width=\"6%\" style=\"background:#d9fcdd;\">卸　1</th>";//卸1
      echo "<th class=xxx width=\"6%\" style=\"background:#d9fcdd;\">卸　2</th>";//卸2
      //echo "<th class=xxx width=\"5%\" style=\"background:#d9fcdd;\">卸3</th>";//卸1
      echo "<th class=xxx width=\"6%\" style=\"background:#d9fcdd;\">工場卸</th>";//卸2
      echo "</tr>";
      echo "</thead>";
      echo "<tbody>";
      $loopindex = 0;
      foreach($rows as $row){
        if($loopindex<2){
          $loopindex++;
          continue;//タイトル列・列タイトル列を飛ばす
        }
        //if("200131-00"==$row["h_list_title_or_code"])
        if(preg_match('/^[0-9]{6}-[0-9]{2}$/', $row["h_list_title_or_code"]))
        break;//最終列に到着したら切るXXXXXX-XX(6桁数字ハイフン2桁数字_更新日時行)は非表示
        //<!--行の開始-->
        echo "<tr class=\"normal\">";
        if($row["h_list_title_or_code"] == "" && $row["h_list_number_string"] != ""){
          //品目番号だけ入っている→タイトル行
            //<!--商品番号-->
            echo "<td></td>";
            echo "<td colspan=9 rowspan=1>".$row["h_list_number_string"]."</td>";//連結
        }else if($row["h_list_title_or_code"] == "" && $row["h_list_number_string"] == ""){
          //<!--商品番号-->
          echo "<td>--------</td>";
          //<!--品目番号-->
          echo "<td></td>";
          //<!--型式-->
          echo "<td></td>";
          //<!--商品名-->
          echo "<td></td>";
          //<!--販売価格-->
          echo "<td></td>";
          //<!--規格-->
          echo "<td></td>";
          //<!--仕様・備考-->
          echo "<td></td>";
          //<!--卸1-->
          echo "<td></td>";
          //<!--卸2-->
          echo "<td></td>";
          //<!--卸3-->
          //echo "<td></td>";
          //<!--工場卸-->
          echo "<td></td>";
        //}else if($row["h_list_title_or_code"] == "特注" && ($row["h_list_number_string"] == "特注"||$row["h_list_number_string"] == "")){
        }else if(($row["h_list_title_or_code"] == "特注" && $row["h_list_number_string"] == "特注") ||
        ($row["h_list_title_or_code"] == "未登録" && $row["h_list_number_string"] == "未登録")){
          //<!--商品番号-->
          echo "<td>".$row["h_list_title_or_code"]."</td>";
          //<!--品目番号-->
          echo "<td>".$row["h_list_number_string"]."</td>";
          //<!--型式-->
          echo "<td></td>";
          //<!--商品名-->
          echo "<td>".$row["proc_name"]."</td>";
          //<!--販売価格-->
          echo "<td></td>";
          //<!--規格-->
          echo "<td></td>";
          //<!--仕様・備考-->
          echo "<td></td>";
          //<!--卸1-->
          echo "<td></td>";
          //<!--卸2-->
          echo "<td></td>";
          //<!--卸3-->
          //echo "<td></td>";
          //<!--工場卸-->
          echo "<td></td>";
        }else if(($row["h_list_title_or_code"] == "特注" && $row["h_list_number_string"] == "")||($row["h_list_title_or_code"] == "未登録" && $row["h_list_number_string"] == "") ){
          //<!--商品番号-->
          echo "<td>--------</td>";
          //<!--品目番号-->
          echo "<td></td>";
          //<!--型式-->
          echo "<td></td>";
          //<!--商品名-->
          echo "<td></td>";
          //<!--販売価格-->
          echo "<td></td>";
          //<!--規格-->
          echo "<td></td>";
          //<!--仕様・備考-->
          echo "<td></td>";
          //<!--卸1-->
          echo "<td></td>";
          //<!--卸2-->
          echo "<td></td>";
          //<!--卸3-->
          //echo "<td></td>";
          //<!--工場卸-->
          echo "<td></td>";
        }else if(preg_match('/^[0]{4}[E,J,K,R]{1}$/', $row["h_list_title_or_code"])||preg_match('/^[0]{4}H[0-9]{1}$/', $row["h_list_title_or_code"])){
          //0000E/0000J/0000K/0000R/0000H1-9 は必ず追加するが価格は非表示
          //<!--商品番号-->
          echo "<td>".$row["h_list_title_or_code"]."</td>";
          //<!--品目番号-->
          echo "<td>".$row["h_list_number_string"]."</td>";
          //<!--型式-->
          echo "<td>".$row["proc_type"]."</td>";
          //<!--商品名-->
          echo "<td>".$row["proc_name"]."</td>";
          //<!--販売価格-->
          echo "<td></td>";
          //<!--規格-->
          echo "<td></td>";
          //<!--仕様・備考-->
          echo "<td></td>";
          //<!--卸1-->
          echo "<td></td>";
          //<!--卸2-->
          echo "<td></td>";
          //<!--卸3-->
          //echo "<td></td>";
          //<!--工場卸-->
          echo "<td></td>";
        }else{
          //<!--商品番号-->
          if(mb_strlen($row["h_list_title_or_code"])==mb_strwidth($row["h_list_title_or_code"])){//半角英数のみ　「特注」「未登録」を除く
            echo "<td>"."<a href = \"detail_list_window_n3.php?List_CodeID=".$row["h_list_title_or_code"]."\">".$row["h_list_title_or_code"]."</td>";
          }else{
            echo "<td>".$row["h_list_title_or_code"]."</td>";
          }
          //<!--品目番号-->
          echo "<td>".$row["h_list_number_string"]."</td>";
          //<!--型式-->
          echo "<td>".$row["proc_type"]."</td>";
          //<!--商品名-->
          echo "<td>".$row["proc_name"]."</td>";
          //<!--販売価格-->
          echo "<td class=\"col_price\">".number_format($row["price_p"])."</td>";
          //<!--規格-->
          echo "<td>".$row["specification"]."</td>";
          //<!--仕様・備考-->
          echo "<td>".$row["notes"]."</td>";
          //<!--卸1-->
          echo "<td class=\"col_price\">".number_format($row["price_d1"])."</td>";
          //<!--卸2-->
          echo "<td class=\"col_price\">".number_format($row["price_d2"])."</td>";
          //<!--卸3-->
          //echo "<td class=\"col_price\">".number_format($row["x_price_d3"])."</td>";
          //<!--工場卸-->
          echo "<td class=\"col_price\">".number_format($row["price_m"])."</td>";
        }
        echo "</tr>";
        $loopindex++;
      }
      echo "</tbody>";
      echo "</table>";
      echo "<br>";
      echo ($j+1)." / ".$page_count;
      echo "</div>";

      /*if($j < $page_count-1){
        $tmp =$j+1;
        echo"<a href=\"#".$tmp."\"style=\"align:left;\">Next</a>";
      }*/
    }//$jループ

    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

?>

<!DOCTYPE html>
<html lang="ja">
<body>  
  <header>
  <div class="header-logo-menu">
    <div id="nav-drawer">
        <input id="nav-input" type="checkbox" class="nav-unshown">
        <label id="nav-open" for="nav-input"><span></span></label>
        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
        <div id="nav-content">
          
          <div class="guide_menu">
            <?PHP
            $menu_count=0;
            for($i = 0; $i < $page_count;$i++){
              if($i==0){
                echo"<label for=\"menu_bar".$menu_count."\">".$vec_page_instrument_type[$i]."</label>";
                echo"<input type=\"checkbox\" id=\"menu_bar".$menu_count."\" />";
                echo"<ul id=\"links".$menu_count."\">";
              }else if($vec_page_instrument_type[$i] != $vec_page_instrument_type[$i-1]){
                echo"</ul>";
                $menu_count++;
                
                echo"<label for=\"menu_bar".$menu_count."\">".$vec_page_instrument_type[$i]."</label>";
                echo"<input type=\"checkbox\" id=\"menu_bar".$menu_count."\" />";
                echo"<ul id=\"links".$menu_count."\">";
              }
              echo"<li><a href=\"#".$i."\">".$vec_page_title[$i]."</a></li>";
            }
            echo"</ul>";
            ?>
          </div>

        </div>
    </div>
  </div>
  </header>
  <div class="main">
  </div>
</body>
</html>