﻿<?php

function create_search_word($word_befor_split,$word_title,$word_bind,&$word_count_rslt,&$array_return_rslt){
  //例：(入力欄に入れた値,number_string→データテーブルの列タイトル,:strNumber→bindValue用の値,検索単語数,空白区切りで切られた配列)
  $func_result ="";

  $text1 = $word_befor_split;
  if (strlen($text1) >0 ){
    //受け取ったキーワードの全角スペースを半角スペースに変換する
    $text1 = str_replace("\t", "", $text1);//表を多めにコピーしたら\tが入ってしまうので削除
    $text1 = str_replace("\n", "", $text1);//改行も削除
    $text1 = str_replace("&amp;", " ", $text1);;//&を削除
    $text1 = str_replace("&quot;", " ", $text1);;//"を削除
    $text1 = str_replace("'", " ", $text1);;//'を削除
    $text1 = str_replace("&lt;", " ", $text1);;//<を削除
    $text1 = str_replace("&gt;", " ", $text1);;//>を削除
    
    $text2 = str_replace("　", " ", $text1);

    //キーワードを空白で分割する
    $array_return = explode(" ",$text2);
    $word_count =count($array_return);
    //分割された個々のキーワードをSQLの条件func_result句に反映する
    $word_count_rslt =0;
    $array_return_rslt=[];

    //$func_result = "(";
    for($i = 0; $i <$word_count;$i++){

      $operate_key1 = "LIKE";

      if($array_return[$i]=='@OR'){
        //echo "<script>alert('OR');</script>";
        continue;
      }
      
      if($array_return[$i]=='@NOT'){
        //echo "<script>alert('NOT');</script>";
        if ($i >= $word_count-1)
           break;//roop抜ける
        $i++;
        $operate_key1 = "NOT LIKE";
      }

      //$func_result .= "(";
      $func_result .= $word_title." ".$operate_key1." (:".$word_bind."_".$word_count_rslt.") collate utf8_unicode_ci";
      //$func_result .= ")";
      //$array_return_rslt[$word_count_rslt] = $array_return[$i];
      array_push($array_return_rslt,$array_return[$i]);
      $word_count_rslt++;

      if ($i <$word_count-1){
        if($array_return[$i+1]=='@OR'){
          $func_result .= " OR ";
        }else{
          $func_result .= " AND ";
        }
      }
    }
   // $func_result .= ")";
  }

  return $func_result;
}
