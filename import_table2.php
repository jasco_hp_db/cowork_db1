﻿<?php
 header("Content-type: text/html; charset=utf-8");
  //ファイルパスの設定
  require("./config/filePath.php");
  $filename ='';//PHP8.0 初期化必須
  $tempfile =''; 
  if( isset( $_FILES['fname2']['tmp_name']) )
    $tempfile = $_FILES['fname2']['tmp_name'];
  if( isset( $_FILES['fname2']['name']) )
    $filename = filePath::$importtext_path.$_FILES['fname2']['name'];
  if (is_uploaded_file($tempfile)) {
      if ( move_uploaded_file($tempfile , $filename )) {
      	chmod($filename, 0755);
    		echo $filename. "をアップロードしました。<br>";
    } else {
        echo "ファイルをアップロードできません。<br>";
    }
  } else {
    echo "ファイルが選択されていません。<br>";
  } 
  $filename1 = $filename;
  //$filename1 ="C:\\xampp\\_notes\\basefiles\\kouhan.txt";
  //$filename1 =$_REQUEST['name'];
  $dummy_code = '';
  //データベースへ接続設定
  require("./config/dbConnect.php");
  $start = microtime(true);
  try {
    $dbh = new PDO($dsn,$user,$password);//成功！

    /**********************************************/
    //言語設定
    echo "言語設定<br>";
    $start = microtime(true);
    echo "開始:".$start."です。filePath::".$filename1."<br>";
    $statement = $dbh->prepare("SET character_set_database=utf8");
    if($statement){
      if($statement->execute())
        echo "言語設定成功です。<br>";
      else
        echo "言語設定失敗しました。<br>";
    }else{
      echo "言語設定失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(後半_重複削除前)の削除前<br>";
    
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer_lh_proc'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(後半_重複削除前)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_lh_proc");
      if($statement){
        if($statement->execute())
          echo "データベース(後半_重複削除前)の削除成功です。<br>";
        else
          echo "データベース(後半_重複削除前)の削失敗しました。<br>";
      }else{
        echo "データベース(後半_重複削除前)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    echo "データベース(後半)の削除前<br>";
    
    $statement = $dbh->prepare("SHOW TABLES LIKE 'new_list_set_buffer_lh'");
    $testval = 0;
    if($statement){
      if($statement->execute()){    
       $testval =$statement->rowCount();
       echo "行数".$testval =$statement->rowCount();
      }
    }
    if($testval>0){
      //既存データベース(後半)の削除
      $start = microtime(true);
      echo "開始:".$start."です。<br>";
      $statement = $dbh->prepare("DROP TABLE new_list_set_buffer_lh");
      if($statement){
        if($statement->execute())
          echo "データベース(後半)の削除成功です。<br>";
        else
          echo "データベース(後半)の削失敗しました。<br>";
      }else{
        echo "データベース(後半)の削失敗しました。<br>";
      }
      $end = microtime(true);
      echo "終了:".$end."です。<br>";
      $sec = ($end - $start);
      echo "処理時間:".$sec."です。<br><br>";
    }
    /**********************************************/
    //データベース((後半_重複削除前)の作成（枠だけ）
    echo "データベース((後半_重複削除前)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_lh_proc(
    column_1 varchar(512),
    column_2 varchar(32),
    column_3 varchar(32),
    column_4 varchar(32),
    column_5 varchar(32),
    instrument_type varchar(128),
    code varchar(32),
    column_8 varchar(32),
    column_9 varchar(32),
    column_10 varchar(64),
    column_11 varchar(32),
    column_12 varchar(32),
    column_13 varchar(32),
    column_14 varchar(32),
    column_15 varchar(32),
    column_16 varchar(32),
    column_17 varchar(512),
    category varchar(256),
    column_19 varchar(512),
    column_20 varchar(32),
    column_21 varchar(32),
    column_22 varchar(32),
    column_23 varchar(32),
    column_24 varchar(32),
    column_25 varchar(32),
    column_26 varchar(32),
    column_27 varchar(32),
    column_28 varchar(32),
    column_29 varchar(32),
    column_30 varchar(32),
    compatible_models text,
    column_32 varchar(32),
    column_33 varchar(33)
    )");
    //PHP8.0厳格化の為、特定列のみLOADFileするように変更
    /*$statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_lh_proc(
      instrument_type varchar(128),
      code varchar(32),
      category varchar(256),
      compatible_models text
      )");*/
    if($statement){
      if($statement->execute())
        echo "データベース(後半_重複削除前)の作成成功です。<br>";
      else
        echo "データベース(後半_重複削除前)の作成失敗しました。<br>";
    }else{
      echo "データベース(後半_重複削除前)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /*********************************************/
    //データベース(後半)の作成（枠だけ）処理用
    echo "データベース(後半)の作成前<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    $statement = $dbh->prepare("CREATE TABLE pricelist.new_list_set_buffer_lh(
      reg_id varchar(32),
      code varchar(32),
      instrument_type varchar(128),
      compatible_models text,
      category varchar(256)
      )");
    if($statement){
      if($statement->execute())
        echo "データベース(後半)の作成成功です。<br>";
      else
        echo "データベース(後半)の作成失敗しました。<br>";
    }else{
      echo "データベース(後半)の作成失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    /**********************************************/
    echo "データベース(後半_重複削除前)の中身詰める<br>";
    //echo "ファイル名:".$filename1."です。<br>";
    //$filename_import = str_replace("\\", "\\\\", $filename1);
    $filename_import = $filename1;
    echo "ファイル名:".$filename_import."です。<br>";
    $start = microtime(true);
    echo "開始:".$start."です。<br>";
    //$buffer_string = "LOAD DATA INFILE '".$filename_import."' INTO TABLE new_list_set_buffer_lh_proc FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\r\\n'";
    //1行目にタイトルが入るようになったため飛ばす
    $buffer_string = "LOAD DATA INFILE :Load_filename INTO TABLE new_list_set_buffer_lh_proc FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES";
    //PHP8.0厳格化の為、特定列のみLOADFileするように変更
    //$buffer_string = "LOAD DATA INFILE :Load_filename INTO TABLE new_list_set_buffer_lh_proc FIELDS TERMINATED BY '\\t' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33) SET instrument_type=@6, code=@7, category=@18, compatible_models=@31;";
    echo "buffer_string:".$buffer_string."です。<br>";
    $statement = $dbh->prepare($buffer_string);
    if($statement){
      $statement->bindValue(':Load_filename', $filename_import, PDO::PARAM_STR);
      try{
        if($statement->execute()){
          echo "データベース(後半_重複削除前)の中身詰める 成功<br>";
        }else{
          echo "データベース(後半_重複削除前)の中身詰める 失敗<br>";
        }
      }catch(PDOException $Exception){

        echo 'エラー: ' . $Exception->getMessage();
    
      }
    }else{
      echo "データベース(後半_重複削除前)の中身詰める<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";
    
    /**********************************************/
     //インデックスの作成
     $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_lh_proc ADD INDEX `code_idx` (code)");
     if($statement){
       $statement->execute();
       echo "new_list_set_buffer_lh_procインデックス作成成功です。<br>";
     }else{
       echo "new_list_set_buffer_lh_procインデックス作成失敗しました。<br>";
     }    
    /**********************************************/
    echo "データベース(後半)の中身詰める<br>";  

    //海外商品番号挿入用
    $statement3 = $dbh->prepare("SELECT * FROM new_list_set_buffer_fh WHERE code LIKE (:ProcName) ");

    //海外商品で対応機種が異なる場合、Excel補足リストに記入あり→Codeを繋いでIFSデータから見て挿入ではなく、Excel補足リストの内容で登録
    $statement4 = $dbh->prepare("SELECT * FROM new_list_set_buffer_lh_proc WHERE code LIKE (:ProcName2) ");

    //新規テーブル準備
    $statement2 = $dbh -> prepare("INSERT INTO new_list_set_buffer_lh (reg_id, code, instrument_type, compatible_models, category) 
    VALUES (:reg_id, :code, :instrument_type, :compatible_models, :category)");
    $start = microtime(true);
    echo "後半詰める開始:".$start."です。<br>";
    //$statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_lh_proc GROUP BY code");//商品番号重複チェック→上手く働かなかったので機種分類・対象機種/用途が両方ブランクだったらはじく
    $statement = $dbh->prepare("SELECT * FROM new_list_set_buffer_lh_proc");
    if($statement){
      if($statement->execute()){
        $row_count = $statement->rowCount();
        $i = 0;
        $dbh->beginTransaction();//必須
        while($record = $statement->fetch(PDO::FETCH_ASSOC)){
          if(!$record["instrument_type"] && !$record["compatible_models"])//&& !$record["category"] 
          {//
           //echo $record["code"]."スキップ。<br>";
           continue;
          }
          //新規テーブルへ挿入
          if($statement2){
            //$statement2->bindValue(':reg_id', $row_count-$i, PDO::PARAM_STR);//エクセルとは逆順に詰まっていくが商品番号で連結ソートするので問題ない
            $statement2->bindValue(':reg_id', $i, PDO::PARAM_STR);//昇順で使用する
            $statement2->bindValue(':code', $record["code"], PDO::PARAM_STR);
            $statement2->bindValue(':instrument_type', $record["instrument_type"], PDO::PARAM_STR);
            $statement2->bindValue(':compatible_models', $record["compatible_models"], PDO::PARAM_STR);
            $statement2->bindValue(':category', $record["category"], PDO::PARAM_STR);
            //$statement2->bindValue(':emplacement_cost', "", PDO::PARAM_STR);
            //$statement2->bindValue(':memo', "", PDO::PARAM_STR);
            //echo "code:".$record["code"]."です。<br>";
            if($statement2->execute()){
              //echo "後半への挿入成功です。<br>";
              $i++;

              //海外での表示順用に挿入
              //number_stringはnew_list_set_buffer_lh_procにも持っている（column_8）が使わない(行削除可能)と決めたためnew_list_set_buffer_fhからピックアップ
              //例：$record["code"]=722E　→　$record3["number_string"]＝1102-8009A　→　$texttmpcode＝11028009A　→　new_list_set_buffer_fhに存在していれば優先順序として挿入
              $texttmpcode = "";
              $texttnumber_string = "";
              if($statement3){
                $statement3->bindValue(':ProcName', $record["code"], PDO::PARAM_STR);
                //echo "検索ワード:".$like_SearchWord."です。<br>";
                if($statement3->execute()){
                  while($record3 = $statement3->fetch(PDO::FETCH_ASSOC)){
                    $texttnumber_string = $record3["number_string"];
                  }
                }else{
                  $errors['error'] = "検索失敗しました。";
                }
              }
              //Excelにわざわざ海外用に補足登録してあった→飛ばす
              if($statement4 && strlen($texttnumber_string)>0){
                $texttmpcode = str_replace("-", "", $texttnumber_string);
                $statement4->bindValue(':ProcName2', $texttmpcode, PDO::PARAM_STR);
                if($statement4->execute()){
                  while($record4 = $statement4->fetch(PDO::FETCH_ASSOC)){
                    //$texttnumber_string = $record4["number_string"];
                    $texttnumber_string = "";//Excel補足に存在するため、飛ばす
                  }
                }else{
                  $errors['error'] = "検索失敗しました。";
                }
              }
              if($statement3){
                if(strlen($texttnumber_string)>0){
                  $texttmpcode = str_replace("-", "", $texttnumber_string);
                  //もともと9桁コードで商品番号の商品もあるのをとばす
                  if($record["code"] != $texttmpcode){
                    //echo "海外用に挿入1code:".$texttmpcode."です。<br>";
                    $statement3->bindValue(':ProcName', $texttmpcode, PDO::PARAM_STR);
                    if($statement3->execute()){
                      if($statement3->rowCount()>0){
                        //echo "海外用に挿入2code:".$texttmpcode."です。<br>";
                        $statement2->bindValue(':reg_id', $i, PDO::PARAM_STR);//昇順で使用する
                        $statement2->bindValue(':code', $texttmpcode, PDO::PARAM_STR);//品目番号1234-5678A　→　海外コード　12345678A　分登録
                        $statement2->bindValue(':instrument_type', $record["instrument_type"], PDO::PARAM_STR);
                        $statement2->bindValue(':compatible_models', $record["compatible_models"], PDO::PARAM_STR);
                        $statement2->bindValue(':category', $record["category"], PDO::PARAM_STR);
                        //$statement2->bindValue(':emplacement_cost', "", PDO::PARAM_STR);
                        //$statement2->bindValue(':memo', "", PDO::PARAM_STR);
                        if($statement2->execute()){
                          //echo "後半への挿入成功です。<br>";
                          $i++;
                        }
                      }
                    }
                  }//もともと9桁コードで商品番号の商品もある
                }
              }

            }else{
              //echo "処理2配列への挿入失敗です。<br>";
            }
          }
        }//while
        $dbh->commit();//必須
        echo $i."件後半への挿入成功です。<br>";
      }else{
        echo "後半への挿入失敗です。<br>";
      }
      echo "結果:".$row_count."件です。<br>";
    }else{
      $errors['error'] = "連結失敗しました。";
      echo "連結失敗しました。<br>";
    }
    $end = microtime(true);
    echo "終了:".$end."です。<br>";
    $sec = ($end - $start);
    echo "処理時間:".$sec."です。<br><br>";

    /**********************************************/
     //インデックスの作成
     $statement = $dbh->prepare("ALTER TABLE new_list_set_buffer_lh ADD INDEX `code_idx` (code)");
     if($statement){
       $statement->execute();
       echo "new_list_set_buffer_lhインデックス作成成功です。<br>";
     }else{
       echo "new_list_set_buffer_lhインデックス作成失敗しました。<br>";
     }    
    /**********************************************/
    print('データベース登録完了！<br>');
    //データベース接続切断
    $dbh = null;       
  } catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
  }

 //echo "<script type='text/javascript'>alert(\"結果を確認後、ウィンドウを閉じる\");</script>";
 //echo "<script type='text/javascript'>history.back();</script>";//力技でウィンドウを閉じる

?>
